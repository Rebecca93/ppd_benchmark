set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set load_g3_m3 1
load goods3 truck market3


set load_g3_m1 1
load goods3 truck market1


soft-AS_property load_g3_m3_not_m1
&& load_g3_m3 ! load_g3_m1


set load_g0_m2 1
load goods0 truck market2


set load_g0_m5 1
load goods0 truck market5


soft-AS_property load_g0_m2_not_m5
&& load_g0_m2 ! load_g0_m5


set load_g2_m5 1
load goods2 truck market5


set load_g2_m4 1
load goods2 truck market4


soft-AS_property load_g2_m5_not_m4
&& load_g2_m5 ! load_g2_m4


set drive_t0_market2_market3 2
drive truck0 market2 market3
drive truck0 market3 market2

soft-AS_property use_t0_market2_market3
drive_t0_market2_market3



set drive_t0_market5_market3 2
drive truck0 market5 market3
drive truck0 market3 market5

soft-AS_property use_not_t0_market5_market3
! drive_t0_market5_market3



set drive_t0_depot0_market5 2
drive truck0 depot0 market5
drive truck0 market5 depot0

soft-AS_property use_t0_depot0_market5
drive_t0_depot0_market5



set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-AS_property use_t0_market2_depot0
drive_t0_market2_depot0



set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-AS_property use_not_t0_market3_depot0
! drive_t0_market3_depot0



