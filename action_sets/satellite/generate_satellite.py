import sys
import random
from random import shuffle

if len(sys.argv) < 4:
    print "usage: python generate_satellite.py name_prefix size goals"
    print "This program generates the domain and problem for satellite."
    quit()

size = int(sys.argv[2])
goals = int(sys.argv[3])

problem_name = "problem_" + sys.argv[1] + "_" + str(size) + "dirs_" + str(goals) + "goals.pddl"

###########
# problem #
###########

pro = open(problem_name,"w+")

pro.write("(define (problem satellite-p" + sys.argv[1] + "_" + str(size) + "dirs_" + str(goals) + "goals)\n")
pro.write("(:domain satellite)\n")
pro.write("(:objects\n")

pro.write("	satellite0 - satellite\n")
pro.write("	instrument0 instrument1 instrument2 - instrument\n")
pro.write("	image spectograph thermograph - mode\n")

for i in range(0,size):
    pro.write(" dir"+str(i))
pro.write(" - direction\n")
pro.write(")\n")

pro.write("(:init\n")

pro.write("    (supports instrument0 image)\n")
pro.write("    (supports instrument1 thermograph)\n")
pro.write("    (supports instrument2 spectograph)\n")

pro.write("    (calibration_target instrument0 dir0)\n")
pro.write("    (calibration_target instrument1 dir0)\n")
pro.write("    (calibration_target instrument2 dir0)\n")

pro.write("	   (on_board instrument0 satellite0)\n")
pro.write("	   (on_board instrument1 satellite0)\n")
pro.write("	   (on_board instrument2 satellite0)\n")

pro.write("    (power_avail satellite0)\n")
pro.write("    (pointing satellite0 dir0)\n")
pro.write("\n")

# directions
angle = []
for i in range(0,size):
    angle.append(random.randint(0, 17))

for i in range(size):
    for j in range(i,size):
        if i!=j:
            pro.write("    (= (slew_time dir"+str(i)+" dir"+str(j)+") " + str(abs(angle[i] - angle[j])) + ")\n")
            pro.write("    (= (slew_time dir"+str(j)+" dir"+str(i)+") " + str(abs(angle[i] - angle[j])) + ")\n")
        else:
            pro.write("    (= (slew_time dir"+str(i)+" dir"+str(j)+") 0)\n")
            pro.write("    (= (slew_time dir"+str(j)+" dir"+str(i)+") 0)\n")
pro.write(")\n")

pro.write("(:goal (and\n")
for i in range(goals):
    pro.write("    (have_image dir"+str(i+1)+" image)\n")
pro.write("))\n")
pro.write("(:metric minimize (total-cost))\n")
pro.write(")\n")

pro.close()
