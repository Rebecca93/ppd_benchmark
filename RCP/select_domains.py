

def all_25():
    return tpp_25() + nomystery_25()

def all_50():
    return tpp_50() + nomystery_50()

def all_75():
    return tpp_75() + nomystery_75()

def all_100():
    return tpp_100() + nomystery_100()

def tpp_25():
    return [
        "tpp/tpp_10M_4G/tpp_10M_4G_25/",
        "tpp/tpp_10M_5G/tpp_10M_5G_25/",
        "tpp/tpp_10M_6G/tpp_10M_6G_25/",
        "tpp/tpp_6M_4G/tpp_6M_4G_25/",
        "tpp/tpp_6M_5G/tpp_6M_5G_25/",
        "tpp/tpp_6M_6G/tpp_6M_6G_25/",
        "tpp/tpp_6M_7G/tpp_6M_7G_25/",
        "tpp/tpp_8M_4G/tpp_8M_4G_25/",
        "tpp/tpp_8M_5G/tpp_8M_5G_25/",
        "tpp/tpp_8M_6G/tpp_8M_6G_25/",
    ]

def tpp_50():
    return [
        "tpp/tpp_10M_4G/tpp_10M_4G_50/",
        "tpp/tpp_10M_5G/tpp_10M_5G_50/",
        "tpp/tpp_10M_6G/tpp_10M_6G_50/",
        "tpp/tpp_6M_4G/tpp_6M_4G_50/",
        "tpp/tpp_6M_5G/tpp_6M_5G_50/",
        "tpp/tpp_6M_6G/tpp_6M_6G_50/",
        "tpp/tpp_6M_7G/tpp_6M_7G_50/",
        "tpp/tpp_8M_4G/tpp_8M_4G_50/",
        "tpp/tpp_8M_5G/tpp_8M_5G_50/",
        "tpp/tpp_8M_6G/tpp_8M_6G_50/",
    ]



def tpp_75():
    return [
        "tpp/tpp_10M_4G/tpp_10M_4G_75/",
        "tpp/tpp_10M_5G/tpp_10M_5G_75/",
        "tpp/tpp_10M_6G/tpp_10M_6G_75/",
        "tpp/tpp_6M_4G/tpp_6M_4G_75/",
        "tpp/tpp_6M_5G/tpp_6M_5G_75/",
        "tpp/tpp_6M_6G/tpp_6M_6G_75/",
        "tpp/tpp_6M_7G/tpp_6M_7G_75/",
        "tpp/tpp_8M_4G/tpp_8M_4G_75/",
        "tpp/tpp_8M_5G/tpp_8M_5G_75/",
        "tpp/tpp_8M_6G/tpp_8M_6G_75/",
    ]


def tpp_100():
    return [
        "tpp/tpp_10M_4G/tpp_10M_4G_100/",
        "tpp/tpp_10M_5G/tpp_10M_5G_100/",
        "tpp/tpp_10M_6G/tpp_10M_6G_100/",
        "tpp/tpp_6M_4G/tpp_6M_4G_100/",
        "tpp/tpp_6M_5G/tpp_6M_5G_100/",
        "tpp/tpp_6M_6G/tpp_6M_6G_100/",
        "tpp/tpp_6M_7G/tpp_6M_7G_100/",
        "tpp/tpp_8M_4G/tpp_8M_4G_100/",
        "tpp/tpp_8M_5G/tpp_8M_5G_100/",
        "tpp/tpp_8M_6G/tpp_8M_6G_100/",
    ]


def nomystery_25():
    return [
        #"nomystery/nomystery_10L_4P/nomystery_10L_4P_c25/",
        "nomystery/nomystery_6L_4P/nomystery_6L_4P_c25/",
        "nomystery/nomystery_6L_5P/nomystery_6L_5P_c25/",
        "nomystery/nomystery_6L_6P/nomystery_6L_6P_c25/",
        "nomystery/nomystery_8L_4P/nomystery_8L_4P_c25/",
        "nomystery/nomystery_8L_5P/nomystery_8L_5P_c25/",
        "nomystery/nomystery_8L_6P/nomystery_8L_6P_c25/",
    ]


def nomystery_50():
    return [
        #"nomystery/nomystery_10L_4P/nomystery_10L_4P_c50/",
        "nomystery/nomystery_6L_4P/nomystery_6L_4P_c50/",
        "nomystery/nomystery_6L_5P/nomystery_6L_5P_c50/",
        "nomystery/nomystery_6L_6P/nomystery_6L_6P_c50/",
        "nomystery/nomystery_8L_4P/nomystery_8L_4P_c50/",
        "nomystery/nomystery_8L_5P/nomystery_8L_5P_c50/",
        "nomystery/nomystery_8L_6P/nomystery_8L_6P_c50/",
    ]


def nomystery_75():
    return [
        #"nomystery/nomystery_10L_4P/nomystery_10L_4P_c75/",
        "nomystery/nomystery_6L_4P/nomystery_6L_4P_c75/",
        "nomystery/nomystery_6L_5P/nomystery_6L_5P_c75/",
        "nomystery/nomystery_6L_6P/nomystery_6L_6P_c75/",
        "nomystery/nomystery_8L_4P/nomystery_8L_4P_c75/",
        "nomystery/nomystery_8L_5P/nomystery_8L_5P_c75/",
        "nomystery/nomystery_8L_6P/nomystery_8L_6P_c75/",
    ]


def nomystery_100():
    return [
        #"nomystery/nomystery_10L_4P/nomystery_10L_4P_c100/",
        "nomystery/nomystery_6L_4P/nomystery_6L_4P_c100/",
        "nomystery/nomystery_6L_5P/nomystery_6L_5P_c100/",
        "nomystery/nomystery_6L_6P/nomystery_6L_6P_c100/",
        "nomystery/nomystery_8L_4P/nomystery_8L_4P_c100/",
        "nomystery/nomystery_8L_5P/nomystery_8L_5P_c100/",
        "nomystery/nomystery_8L_6P/nomystery_8L_6P_c100/",
    ]
