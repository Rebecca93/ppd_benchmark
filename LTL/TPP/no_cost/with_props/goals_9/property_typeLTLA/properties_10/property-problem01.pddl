state_set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-LTL_property use_not_t0_market1_market3
[] ! drive_t0_market1_market3



state_set load_g6_m3 1
load goods6 truck market3


state_set load_g6_m1 1
load goods6 truck market1


soft-LTL_property load_g6_m3_not_m1
&& <> load_g6_m3 [] ! load_g6_m1


state_set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-LTL_property use_not_t0_market3_depot0
[] ! drive_t0_market3_depot0



state_set drive_t0_market4_market2 2
drive truck0 market4 market2
drive truck0 market2 market4

soft-LTL_property use_t0_market4_market2
<> drive_t0_market4_market2



state_set drive_t0_depot0_market5 2
drive truck0 depot0 market5
drive truck0 market5 depot0

soft-LTL_property use_t0_depot0_market5
<> drive_t0_depot0_market5



state_set load_g8_m3 1
load goods8 truck market3


state_set load_g8_m2 1
load goods8 truck market2


soft-LTL_property load_g8_m3_not_m2
&& <> load_g8_m3 [] ! load_g8_m2


state_set drive_t0_market2_market1 2
drive truck0 market2 market1
drive truck0 market1 market2

soft-LTL_property use_t0_market2_market1
<> drive_t0_market2_market1



state_set drive_t0_market1_depot0 2
drive truck0 market1 depot0
drive truck0 depot0 market1

soft-LTL_property use_not_t0_market1_depot0
[] ! drive_t0_market1_depot0



state_set drive_t0_market3_market5 2
drive truck0 market3 market5
drive truck0 market5 market3

soft-LTL_property use_not_t0_market3_market5
[] ! drive_t0_market3_market5



