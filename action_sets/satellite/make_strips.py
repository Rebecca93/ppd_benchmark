import sys
import re
from itertools import product

if len(sys.argv) < 3:
    print "usage: python make_strips.py problem maxcost"
    print "This program converts a satellite problem instance to strips-style numerics."
    quit()

# read problem
maxcost = sys.argv[2]
problem = open(sys.argv[1], "r") 
for line in problem:
    if "(= (slew_time" in line:
        energy = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        if(int(energy[0]) > int(maxcost)):
            maxcost = energy[0]
problem.close()

# read problem
problem = open(sys.argv[1], "r") 
for line in problem:

    if  " - direction" in line:
        print line,
        for i in range(2,int(maxcost)+1):
            print "level"+str(i),
        print "- num"
    elif "(:init" in line:
        print line,
        print "(time_cost level" + str(maxcost) + ")"
        print
        for i,j in product(range(0,int(maxcost)+1), range(0,int(maxcost)+1)):
            if (i+j <= int(maxcost)):
                print "(sum level" + str(i) + " level" + str(j) + " level" + str(i+j) + ")"
    elif "(= (slew_time " in line:
        digits = [s for s in re.split('\)| |h',line) if s.isdigit()]
        strings = line.strip().split(" ")
        print "(slew_time level" + digits[0] + " " + strings[2] + " " + strings[3]
    elif "metric" in line:
        print
    else:
        print line,
problem.close()
