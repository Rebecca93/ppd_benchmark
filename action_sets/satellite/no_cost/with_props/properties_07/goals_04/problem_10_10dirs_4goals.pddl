(define (problem satellite-p10_10dirs_4goals)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 instrument1 instrument2 - instrument
	image spectograph thermograph - mode
 dir0 dir1 dir2 dir3 dir4 dir5 dir6 dir7 dir8 dir9 - direction
)
(:init
    (supports instrument0 image)
    (supports instrument1 thermograph)
    (supports instrument2 spectograph)
    (calibration_target instrument0 dir0)
    (calibration_target instrument1 dir0)
    (calibration_target instrument2 dir0)
    (= (calibration_time instrument0 dir0) 5)
    (= (calibration_time instrument1 dir0) 5)
    (= (calibration_time instrument2 dir0) 5)
	   (on_board instrument0 satellite0)
	   (on_board instrument1 satellite0)
	   (on_board instrument2 satellite0)
    (available instrument0)
    (available instrument1)
    (available instrument2)
    (not_calibrated instrument0)
    (not_calibrated instrument1)
    (not_calibrated instrument2)

    (power_avail satellite0)
    (pointing satellite0 dir0)

    (= (slew_time dir0 dir1) 228)
    (= (slew_time dir1 dir0) 228)
    (= (slew_time dir0 dir2) 203)
    (= (slew_time dir2 dir0) 203)
    (= (slew_time dir0 dir3) 280)
    (= (slew_time dir3 dir0) 280)
    (= (slew_time dir0 dir4) 33)
    (= (slew_time dir4 dir0) 33)
    (= (slew_time dir0 dir5) 47)
    (= (slew_time dir5 dir0) 47)
    (= (slew_time dir0 dir6) 2)
    (= (slew_time dir6 dir0) 2)
    (= (slew_time dir0 dir7) 190)
    (= (slew_time dir7 dir0) 190)
    (= (slew_time dir0 dir8) 278)
    (= (slew_time dir8 dir0) 278)
    (= (slew_time dir0 dir9) 321)
    (= (slew_time dir9 dir0) 321)
    (= (slew_time dir1 dir0) 228)
    (= (slew_time dir0 dir1) 228)
    (= (slew_time dir1 dir2) 25)
    (= (slew_time dir2 dir1) 25)
    (= (slew_time dir1 dir3) 52)
    (= (slew_time dir3 dir1) 52)
    (= (slew_time dir1 dir4) 195)
    (= (slew_time dir4 dir1) 195)
    (= (slew_time dir1 dir5) 181)
    (= (slew_time dir5 dir1) 181)
    (= (slew_time dir1 dir6) 226)
    (= (slew_time dir6 dir1) 226)
    (= (slew_time dir1 dir7) 38)
    (= (slew_time dir7 dir1) 38)
    (= (slew_time dir1 dir8) 50)
    (= (slew_time dir8 dir1) 50)
    (= (slew_time dir1 dir9) 93)
    (= (slew_time dir9 dir1) 93)
    (= (slew_time dir2 dir0) 203)
    (= (slew_time dir0 dir2) 203)
    (= (slew_time dir2 dir1) 25)
    (= (slew_time dir1 dir2) 25)
    (= (slew_time dir2 dir3) 77)
    (= (slew_time dir3 dir2) 77)
    (= (slew_time dir2 dir4) 170)
    (= (slew_time dir4 dir2) 170)
    (= (slew_time dir2 dir5) 156)
    (= (slew_time dir5 dir2) 156)
    (= (slew_time dir2 dir6) 201)
    (= (slew_time dir6 dir2) 201)
    (= (slew_time dir2 dir7) 13)
    (= (slew_time dir7 dir2) 13)
    (= (slew_time dir2 dir8) 75)
    (= (slew_time dir8 dir2) 75)
    (= (slew_time dir2 dir9) 118)
    (= (slew_time dir9 dir2) 118)
    (= (slew_time dir3 dir0) 280)
    (= (slew_time dir0 dir3) 280)
    (= (slew_time dir3 dir1) 52)
    (= (slew_time dir1 dir3) 52)
    (= (slew_time dir3 dir2) 77)
    (= (slew_time dir2 dir3) 77)
    (= (slew_time dir3 dir4) 247)
    (= (slew_time dir4 dir3) 247)
    (= (slew_time dir3 dir5) 233)
    (= (slew_time dir5 dir3) 233)
    (= (slew_time dir3 dir6) 278)
    (= (slew_time dir6 dir3) 278)
    (= (slew_time dir3 dir7) 90)
    (= (slew_time dir7 dir3) 90)
    (= (slew_time dir3 dir8) 2)
    (= (slew_time dir8 dir3) 2)
    (= (slew_time dir3 dir9) 41)
    (= (slew_time dir9 dir3) 41)
    (= (slew_time dir4 dir0) 33)
    (= (slew_time dir0 dir4) 33)
    (= (slew_time dir4 dir1) 195)
    (= (slew_time dir1 dir4) 195)
    (= (slew_time dir4 dir2) 170)
    (= (slew_time dir2 dir4) 170)
    (= (slew_time dir4 dir3) 247)
    (= (slew_time dir3 dir4) 247)
    (= (slew_time dir4 dir5) 14)
    (= (slew_time dir5 dir4) 14)
    (= (slew_time dir4 dir6) 31)
    (= (slew_time dir6 dir4) 31)
    (= (slew_time dir4 dir7) 157)
    (= (slew_time dir7 dir4) 157)
    (= (slew_time dir4 dir8) 245)
    (= (slew_time dir8 dir4) 245)
    (= (slew_time dir4 dir9) 288)
    (= (slew_time dir9 dir4) 288)
    (= (slew_time dir5 dir0) 47)
    (= (slew_time dir0 dir5) 47)
    (= (slew_time dir5 dir1) 181)
    (= (slew_time dir1 dir5) 181)
    (= (slew_time dir5 dir2) 156)
    (= (slew_time dir2 dir5) 156)
    (= (slew_time dir5 dir3) 233)
    (= (slew_time dir3 dir5) 233)
    (= (slew_time dir5 dir4) 14)
    (= (slew_time dir4 dir5) 14)
    (= (slew_time dir5 dir6) 45)
    (= (slew_time dir6 dir5) 45)
    (= (slew_time dir5 dir7) 143)
    (= (slew_time dir7 dir5) 143)
    (= (slew_time dir5 dir8) 231)
    (= (slew_time dir8 dir5) 231)
    (= (slew_time dir5 dir9) 274)
    (= (slew_time dir9 dir5) 274)
    (= (slew_time dir6 dir0) 2)
    (= (slew_time dir0 dir6) 2)
    (= (slew_time dir6 dir1) 226)
    (= (slew_time dir1 dir6) 226)
    (= (slew_time dir6 dir2) 201)
    (= (slew_time dir2 dir6) 201)
    (= (slew_time dir6 dir3) 278)
    (= (slew_time dir3 dir6) 278)
    (= (slew_time dir6 dir4) 31)
    (= (slew_time dir4 dir6) 31)
    (= (slew_time dir6 dir5) 45)
    (= (slew_time dir5 dir6) 45)
    (= (slew_time dir6 dir7) 188)
    (= (slew_time dir7 dir6) 188)
    (= (slew_time dir6 dir8) 276)
    (= (slew_time dir8 dir6) 276)
    (= (slew_time dir6 dir9) 319)
    (= (slew_time dir9 dir6) 319)
    (= (slew_time dir7 dir0) 190)
    (= (slew_time dir0 dir7) 190)
    (= (slew_time dir7 dir1) 38)
    (= (slew_time dir1 dir7) 38)
    (= (slew_time dir7 dir2) 13)
    (= (slew_time dir2 dir7) 13)
    (= (slew_time dir7 dir3) 90)
    (= (slew_time dir3 dir7) 90)
    (= (slew_time dir7 dir4) 157)
    (= (slew_time dir4 dir7) 157)
    (= (slew_time dir7 dir5) 143)
    (= (slew_time dir5 dir7) 143)
    (= (slew_time dir7 dir6) 188)
    (= (slew_time dir6 dir7) 188)
    (= (slew_time dir7 dir8) 88)
    (= (slew_time dir8 dir7) 88)
    (= (slew_time dir7 dir9) 131)
    (= (slew_time dir9 dir7) 131)
    (= (slew_time dir8 dir0) 278)
    (= (slew_time dir0 dir8) 278)
    (= (slew_time dir8 dir1) 50)
    (= (slew_time dir1 dir8) 50)
    (= (slew_time dir8 dir2) 75)
    (= (slew_time dir2 dir8) 75)
    (= (slew_time dir8 dir3) 2)
    (= (slew_time dir3 dir8) 2)
    (= (slew_time dir8 dir4) 245)
    (= (slew_time dir4 dir8) 245)
    (= (slew_time dir8 dir5) 231)
    (= (slew_time dir5 dir8) 231)
    (= (slew_time dir8 dir6) 276)
    (= (slew_time dir6 dir8) 276)
    (= (slew_time dir8 dir7) 88)
    (= (slew_time dir7 dir8) 88)
    (= (slew_time dir8 dir9) 43)
    (= (slew_time dir9 dir8) 43)
    (= (slew_time dir9 dir0) 321)
    (= (slew_time dir0 dir9) 321)
    (= (slew_time dir9 dir1) 93)
    (= (slew_time dir1 dir9) 93)
    (= (slew_time dir9 dir2) 118)
    (= (slew_time dir2 dir9) 118)
    (= (slew_time dir9 dir3) 41)
    (= (slew_time dir3 dir9) 41)
    (= (slew_time dir9 dir4) 288)
    (= (slew_time dir4 dir9) 288)
    (= (slew_time dir9 dir5) 274)
    (= (slew_time dir5 dir9) 274)
    (= (slew_time dir9 dir6) 319)
    (= (slew_time dir6 dir9) 319)
    (= (slew_time dir9 dir7) 131)
    (= (slew_time dir7 dir9) 131)
    (= (slew_time dir9 dir8) 43)
    (= (slew_time dir8 dir9) 43)
)
(:goal (and
    (have_image dir1 image)
    (have_image dir2 image)
    (have_image dir3 image)
    (have_image dir4 image)
))
(:metric minimize (total-time))
)
