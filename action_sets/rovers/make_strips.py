import sys
import re
from itertools import product

if len(sys.argv) < 2:
    print "usage: python make_strips.py problem"
    print "This program converts a rovers problem instance to strips-style numerics."
    quit()

# read problem
maxenergy = 17
foundobj = False
problem = open(sys.argv[1], "r") 
for line in problem:
    if  " - Objective" in line:
        foundobj = True
    if "    (= (total-energy) " in line:
        energy = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        if(int(energy[0]) > maxenergy):
            maxenergy = int(energy[0])
problem.close()

# read problem
problem = open(sys.argv[1], "r") 
for line in problem:

    if  " - Objective" in line or (not foundobj and " - Waypoint" in line):
        print line,
        print "   ",
        for i in range(0,maxenergy+1):
            print "level"+str(i),
        print "- energylevel"
    elif "    (= (total-energy) " in line:
        energy = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        print "    (energy level" + energy[0] + ")"
        print
        for i,j in product(range(0,maxenergy+1), range(0,maxenergy+1)):
            if (i+j <= maxenergy):
                print "    (sum level" + str(i) + " level" + str(j) + " level" + str(i+j) + ")"

    elif "    (= (energycost " in line:
        digits = [s for s in re.split('\)| |waypoint',line) if s.isdigit()]
        print "    (energycost level" + digits[2] + " waypoint" + digits[0] + " waypoint" + digits[1] + ")"
    else:
        print line,
problem.close()
