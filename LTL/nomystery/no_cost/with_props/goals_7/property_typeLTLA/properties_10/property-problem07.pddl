state_set drive_t1_l4_l2 2
drive t1 l4 l2
drive t1 l2 l4

soft-LTL_property use_t1_l4_l2
<> drive_t1_l4_l2


state_set delwithT0_p3p5 4
unload p3 t0 location
load p3 t0 location
unload p5 t0 location
load p5 t0 location

state_set delwithT1_p3p5 4
unload p3 t1 location
load p3 t1 location
unload p5 t1 location
load p5 t1 location

soft-LTL_property same_truck_p3p5
|| [] ! delwithT0_p3p5 [] ! delwithT1_p3p5


state_set delwithT0_p0p6 4
unload p0 t0 location
load p0 t0 location
unload p6 t0 location
load p6 t0 location

state_set delwithT1_p0p6 4
unload p0 t1 location
load p0 t1 location
unload p6 t1 location
load p6 t1 location

soft-LTL_property same_truck_p0p6
|| [] ! delwithT0_p0p6 [] ! delwithT1_p0p6


state_set delwithT0_p0p5 4
unload p0 t0 location
load p0 t0 location
unload p5 t0 location
load p5 t0 location

state_set delwithT1_p0p5 4
unload p0 t1 location
load p0 t1 location
unload p5 t1 location
load p5 t1 location

soft-LTL_property same_truck_p0p5
|| [] ! delwithT0_p0p5 [] ! delwithT1_p0p5


state_set drive_t1_l2_l0 2
drive t1 l2 l0
drive t1 l0 l2

soft-LTL_property use_t1_l2_l0
<> drive_t1_l2_l0


state_set drive_t0_l1_l5 2
drive t0 l1 l5
drive t0 l5 l1

soft-LTL_property use_t0_l1_l5
<> drive_t0_l1_l5


state_set drive_t0_l5_l3 2
drive t0 l5 l3
drive t0 l3 l5

soft-LTL_property use_t0_l5_l3
<> drive_t0_l5_l3


state_set delwithT0_p3p6 4
unload p3 t0 location
load p3 t0 location
unload p6 t0 location
load p6 t0 location

state_set delwithT1_p3p6 4
unload p3 t1 location
load p3 t1 location
unload p6 t1 location
load p6 t1 location

soft-LTL_property same_truck_p3p6
|| [] ! delwithT0_p3p6 [] ! delwithT1_p3p6


state_set drive_t1_l2_l1 2
drive t1 l2 l1
drive t1 l1 l2

soft-LTL_property use_t1_l2_l1
<> drive_t1_l2_l1


state_set drive_t0_l0_l2 2
drive t0 l0 l2
drive t0 l2 l0

soft-LTL_property use_t0_l0_l2
<> drive_t0_l0_l2


