soft-LTL_property use_t0_market4_market1
|| <> && at(truck0,market4) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market4)


soft-LTL_property use_not_t0_market2_depot0
&& [] || ! at(truck0,market2) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market2)


soft-LTL_property load_g1_m2_not_m1
<> && && available(goods1,market2) ! available(goods1,market1) X loaded(goods1,truck0)


soft-LTL_property use_not_t0_market3_market1
&& [] || ! at(truck0,market3) ! X at(truck0,market1) [] || ! at(truck0,market1) ! X at(truck0,market3)


soft-LTL_property use_t0_market3_market5
|| <> && at(truck0,market3) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market3)


soft-LTL_property use_t0_depot0_market3
|| <> && at(truck0,depot0) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,depot0)


soft-LTL_property load_g2_m5_not_m2
<> && && available(goods2,market5) ! available(goods2,market2) X loaded(goods2,truck0)


