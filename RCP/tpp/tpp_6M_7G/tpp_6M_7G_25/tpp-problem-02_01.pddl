(define (problem tpp-problem-m6-g7-c0.250000-s1000)
(:domain TPPLike-Metric)
(:objects
	market1 market2 market3 market4 market5 market6 - market
	depot0 - depot
	truck0 - truck
	goods0 goods1 goods2 goods3 goods4 goods5 goods6 - goods
    level3 level2 level1 level0 - moneylevel
)
(:init
(sum level0 level0 level0)
(sum level0 level1 level1)
(sum level0 level2 level2)
(sum level0 level3 level3)
(sum level1 level0 level1)
(sum level1 level1 level2)
(sum level1 level2 level3)
(sum level2 level0 level2)
(sum level2 level1 level3)
(sum level3 level0 level3)
	(price goods0 market2 level1)
	(on-sale goods0 market2)
	(price goods0 market3 level1)
	(on-sale goods0 market3)
	(price goods1 market4 level1)
	(on-sale goods1 market4)
	(price goods1 market6 level1)
	(on-sale goods1 market6)
	(price goods2 market1 level1)
	(on-sale goods2 market1)
	(price goods2 market6 level1)
	(on-sale goods2 market6)
	(price goods3 market1 level1)
	(on-sale goods3 market1)
	(price goods3 market2 level1)
	(on-sale goods3 market2)
	(price goods4 market3 level1)
	(on-sale goods4 market3)
	(price goods4 market5 level1)
	(on-sale goods4 market5)
	(price goods5 market1 level1)
	(on-sale goods5 market1)
	(price goods5 market2 level1)
	(on-sale goods5 market2)
	(price goods6 market1 level1)
	(on-sale goods6 market1)
	(price goods6 market3 level1)
	(on-sale goods6 market3)
	(drive-cost depot0 market2 level1)
	(connected depot0 market2)
	(drive-cost market1 market3 level1)
	(connected market1 market3)
	(drive-cost market2 depot0 level1)
	(connected market2 depot0)
	(drive-cost market2 market3 level1)
	(connected market2 market3)
	(drive-cost market2 market5 level1)
	(connected market2 market5)
	(drive-cost market3 market1 level1)
	(connected market3 market1)
	(drive-cost market3 market2 level1)
	(connected market3 market2)
	(drive-cost market3 market5 level1)
	(connected market3 market5)
	(drive-cost market3 market6 level1)
	(connected market3 market6)
	(drive-cost market4 market5 level1)
	(connected market4 market5)
	(drive-cost market5 market2 level1)
	(connected market5 market2)
	(drive-cost market5 market3 level1)
	(connected market5 market3)
	(drive-cost market5 market4 level1)
	(connected market5 market4)
	(drive-cost market6 market3 level1)
	(connected market6 market3)
	(at truck0 depot0)
(money level3)
)
(:goal (and
	(stored goods0)
	(stored goods1)
	(stored goods2)
	(stored goods3)
	(stored goods4)
	(stored goods5)
	(stored goods6)
))
)
