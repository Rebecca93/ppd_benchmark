set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set load_g1_m3 1
load goods1 truck market3


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m3_not_m2
&& load_g1_m3 ! load_g1_m2


set drive_t0_depot0_market1 2
drive truck0 depot0 market1 moneylevel moneylevel moneylevel
drive truck0 market1 depot0 moneylevel moneylevel moneylevel


soft-property use_not_t0_depot0_market1
! drive_t0_depot0_market1



set load_g2_m1 1
load goods2 truck market1


set load_g2_m5 1
load goods2 truck market5


soft-property load_g2_m1_not_m5
&& load_g2_m1 ! load_g2_m5


set drive_t0_market1_market2 2
drive truck0 market1 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market1 moneylevel moneylevel moneylevel


soft-property use_t0_market1_market2
drive_t0_market1_market2



