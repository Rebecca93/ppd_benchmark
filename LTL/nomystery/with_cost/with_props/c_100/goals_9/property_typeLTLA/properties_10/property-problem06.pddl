state_set drive_t1_l5_l0 2
drive t1 l5 l0 fuellevel fuellevel fuellevel
drive t1 l0 l5 fuellevel fuellevel fuellevel

soft-LTL_property use_t1_l5_l0
<> drive_t1_l5_l0


state_set delwithT0_p5p6 4
unload p5 t0 location
load p5 t0 location
unload p6 t0 location
load p6 t0 location

state_set delwithT1_p5p6 4
unload p5 t1 location
load p5 t1 location
unload p6 t1 location
load p6 t1 location

soft-LTL_property same_truck_p5p6
|| [] ! delwithT0_p5p6 [] ! delwithT1_p5p6


state_set drive_t0_l1_l0 2
drive t0 l1 l0 fuellevel fuellevel fuellevel
drive t0 l0 l1 fuellevel fuellevel fuellevel

soft-LTL_property use_t0_l1_l0
<> drive_t0_l1_l0


state_set drive_t1_l5_l2 2
drive t1 l5 l2 fuellevel fuellevel fuellevel
drive t1 l2 l5 fuellevel fuellevel fuellevel

soft-LTL_property use_t1_l5_l2
<> drive_t1_l5_l2


state_set delwithT0_p3p5 4
unload p3 t0 location
load p3 t0 location
unload p5 t0 location
load p5 t0 location

state_set delwithT1_p3p5 4
unload p3 t1 location
load p3 t1 location
unload p5 t1 location
load p5 t1 location

soft-LTL_property same_truck_p3p5
|| [] ! delwithT0_p3p5 [] ! delwithT1_p3p5


state_set drive_t0_l1_l5 2
drive t0 l1 l5 fuellevel fuellevel fuellevel
drive t0 l5 l1 fuellevel fuellevel fuellevel

soft-LTL_property use_t0_l1_l5
<> drive_t0_l1_l5


state_set drive_t0_l0_l5 2
drive t0 l0 l5 fuellevel fuellevel fuellevel
drive t0 l5 l0 fuellevel fuellevel fuellevel

soft-LTL_property use_t0_l0_l5
<> drive_t0_l0_l5


state_set drive_t0_l1_l3 2
drive t0 l1 l3 fuellevel fuellevel fuellevel
drive t0 l3 l1 fuellevel fuellevel fuellevel

soft-LTL_property use_t0_l1_l3
<> drive_t0_l1_l3


state_set delwithT0_p3p6 4
unload p3 t0 location
load p3 t0 location
unload p6 t0 location
load p6 t0 location

state_set delwithT1_p3p6 4
unload p3 t1 location
load p3 t1 location
unload p6 t1 location
load p6 t1 location

soft-LTL_property same_truck_p3p6
|| [] ! delwithT0_p3p6 [] ! delwithT1_p3p6


state_set delwithT0_p4p6 4
unload p4 t0 location
load p4 t0 location
unload p6 t0 location
load p6 t0 location

state_set delwithT1_p4p6 4
unload p4 t1 location
load p4 t1 location
unload p6 t1 location
load p6 t1 location

soft-LTL_property same_truck_p4p6
|| [] ! delwithT0_p4p6 [] ! delwithT1_p4p6


