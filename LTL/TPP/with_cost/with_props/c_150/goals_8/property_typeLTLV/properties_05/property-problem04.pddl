soft-LTL_property use_t0_market4_market5
|| <> && at(truck0,market4) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market4)


soft-LTL_property use_not_t0_market1_market5
&& [] || ! at(truck0,market1) ! X at(truck0,market5) [] || ! at(truck0,market5) ! X at(truck0,market1)


soft-LTL_property load_g6_m3_not_m1
<> && && available(goods6,market3) ! available(goods6,market1) X loaded(goods6,truck0)


soft-LTL_property use_not_t0_market2_market4
&& [] || ! at(truck0,market2) ! X at(truck0,market4) [] || ! at(truck0,market4) ! X at(truck0,market2)


soft-LTL_property use_t0_market2_market5
|| <> && at(truck0,market2) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market2)


