state_set drive_t0_market4_market1 2
drive truck0 market4 market1
drive truck0 market1 market4

soft-LTL_property use_t0_market4_market1
<> drive_t0_market4_market1



state_set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-LTL_property use_not_t0_market2_depot0
[] ! drive_t0_market2_depot0



state_set load_g3_m5 1
load goods3 truck market5


state_set load_g3_m3 1
load goods3 truck market3


soft-LTL_property load_g3_m5_not_m3
&& <> load_g3_m5 [] ! load_g3_m3


state_set drive_t0_market3_market1 2
drive truck0 market3 market1
drive truck0 market1 market3

soft-LTL_property use_not_t0_market3_market1
[] ! drive_t0_market3_market1



state_set drive_t0_market3_market5 2
drive truck0 market3 market5
drive truck0 market5 market3

soft-LTL_property use_t0_market3_market5
<> drive_t0_market3_market5



state_set drive_t0_depot0_market3 2
drive truck0 depot0 market3
drive truck0 market3 depot0

soft-LTL_property use_t0_depot0_market3
<> drive_t0_depot0_market3



state_set load_g1_m2 1
load goods1 truck market2


state_set load_g1_m1 1
load goods1 truck market1


soft-LTL_property load_g1_m2_not_m1
&& <> load_g1_m2 [] ! load_g1_m1


state_set drive_t0_market2_market4 2
drive truck0 market2 market4
drive truck0 market4 market2

soft-LTL_property use_t0_market2_market4
<> drive_t0_market2_market4



