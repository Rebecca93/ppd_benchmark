set drive_t0_market4_market1 2
drive truck0 market4 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market1
drive_t0_market4_market1



set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set load_g3_m5 1
load goods3 truck market5


set load_g3_m3 1
load goods3 truck market3


soft-AS_property load_g3_m5_not_m3
&& load_g3_m5 ! load_g3_m3


