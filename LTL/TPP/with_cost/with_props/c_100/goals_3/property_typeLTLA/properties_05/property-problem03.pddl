state_set drive_t0_market4_depot0 2
drive truck0 market4 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_depot0
<> drive_t0_market4_depot0



state_set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market2_depot0
[] ! drive_t0_market2_depot0



state_set load_g1_m5 1
load goods1 truck market5


state_set load_g1_m3 1
load goods1 truck market3


soft-LTL_property load_g1_m5_not_m3
&& <> load_g1_m5 [] ! load_g1_m3


state_set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market3_depot0
[] ! drive_t0_market3_depot0



state_set drive_t0_market3_market2 2
drive truck0 market3 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market3 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market3_market2
<> drive_t0_market3_market2



