(define (problem blocksworld-p08_7)
(:domain blocksworld)
(:objects
a b c d e f g h - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear h)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
(on h g)
)
(:goal (and
(on d c)
(on h d)
(on f h)
(on e f)
(on b e)
(on g b)
(on a g)
))
(:metric minimize (total-cost))
)
