set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set drive_t0_market5_market4 2
drive truck0 market5 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market5 moneylevel moneylevel moneylevel


soft-property use_not_t0_market5_market4
! drive_t0_market5_market4



set load_g2_m3 1
load goods2 truck market3


set load_g2_m4 1
load goods2 truck market4


soft-property load_g2_m3_not_m4
&& load_g2_m3 ! load_g2_m4


set load_g1_m5 1
load goods1 truck market5


set load_g1_m1 1
load goods1 truck market1


soft-property load_g1_m5_not_m1
&& load_g1_m5 ! load_g1_m1


set load_g0_m2 1
load goods0 truck market2


set load_g0_m5 1
load goods0 truck market5


soft-property load_g0_m2_not_m5
&& load_g0_m2 ! load_g0_m5


set load_g3_m1 1
load goods3 truck market1


set load_g3_m4 1
load goods3 truck market4


soft-property load_g3_m1_not_m4
&& load_g3_m1 ! load_g3_m4


set drive_t0_market3_market4 2
drive truck0 market3 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market3 moneylevel moneylevel moneylevel


soft-property use_not_t0_market3_market4
! drive_t0_market3_market4



set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-property use_t0_market3_depot0
drive_t0_market3_depot0



