set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-AS_property use_not_t0_market1_market3
! drive_t0_market1_market3



set load_g1_m4 1
load goods1 truck market4


set load_g1_m2 1
load goods1 truck market2


soft-AS_property load_g1_m4_not_m2
&& load_g1_m4 ! load_g1_m2


set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-AS_property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market4_market2 2
drive truck0 market4 market2
drive truck0 market2 market4

soft-AS_property use_t0_market4_market2
drive_t0_market4_market2



set drive_t0_depot0_market5 2
drive truck0 depot0 market5
drive truck0 market5 depot0

soft-AS_property use_t0_depot0_market5
drive_t0_depot0_market5



set load_g2_m3 1
load goods2 truck market3


set load_g2_m2 1
load goods2 truck market2


soft-AS_property load_g2_m3_not_m2
&& load_g2_m3 ! load_g2_m2


