set samplerock_rover0_waypoint4 1
sample_rock rover0 store waypoint4

set take_image_rover0_waypoint1_objective1 1
take_image rover0 waypoint1 objective1 camera mode

set take_image_camera12_objective3 1
take_image rover waypoint objective3 camera12 mode

set take_image_camera00_objective2 1
take_image rover waypoint objective2 camera00 mode

set take_image_rover0_waypoint0_objective2 1
take_image rover0 waypoint0 objective2 camera mode

set take_image_camera11_objective5 1
take_image rover waypoint objective5 camera11 mode

set take_image_rover0_waypoint4_objective2 1
take_image rover0 waypoint4 objective2 camera mode

set take_image_rover0_waypoint4_objective3 1
take_image rover0 waypoint4 objective3 camera mode

set take_image_rover0_waypoint4_objective0 1
take_image rover0 waypoint4 objective0 camera mode

soft-property preferred_sample_rover0_waypoint4
samplerock_rover0_waypoint4

soft-property preferred_take_image_rover0_waypoint1_objective1
take_image_rover0_waypoint1_objective1

soft-property preferred_take_image_camera12_objective3
take_image_camera12_objective3

soft-property preferred_take_image_camera00_objective2
take_image_camera00_objective2

soft-property preferred_take_image_rover0_waypoint0_objective2
take_image_rover0_waypoint0_objective2

soft-property preferred_take_image_camera11_objective5
take_image_camera11_objective5

soft-property preferred_take_image_rover0_waypoint4_objective2
take_image_rover0_waypoint4_objective2

soft-property preferred_take_image_rover0_waypoint4_objective3
take_image_rover0_waypoint4_objective3

soft-property preferred_take_image_rover0_waypoint4_objective0
take_image_rover0_waypoint4_objective0

