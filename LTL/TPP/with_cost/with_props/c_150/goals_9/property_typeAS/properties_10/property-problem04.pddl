set drive_t0_market4_market5 2
drive truck0 market4 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



set drive_t0_market1_market5 2
drive truck0 market1 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market1 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market1_market5
! drive_t0_market1_market5



set load_g6_m3 1
load goods6 truck market3


set load_g6_m1 1
load goods6 truck market1


soft-AS_property load_g6_m3_not_m1
&& load_g6_m3 ! load_g6_m1


set drive_t0_market2_market4 2
drive truck0 market2 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market2 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market2_market4
! drive_t0_market2_market4



set drive_t0_market2_market5 2
drive truck0 market2 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market2 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market2_market5
drive_t0_market2_market5



set drive_t0_depot0_market2 2
drive truck0 depot0 market2 moneylevel moneylevel moneylevel
drive truck0 market2 depot0 moneylevel moneylevel moneylevel


soft-AS_property use_t0_depot0_market2
drive_t0_depot0_market2



set load_g8_m2 1
load goods8 truck market2


set load_g8_m1 1
load goods8 truck market1


soft-AS_property load_g8_m2_not_m1
&& load_g8_m2 ! load_g8_m1


set drive_t0_market1_market2 2
drive truck0 market1 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market1 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market1_market2
drive_t0_market1_market2



set drive_t0_depot0_market4 2
drive truck0 depot0 market4 moneylevel moneylevel moneylevel
drive truck0 market4 depot0 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_depot0_market4
! drive_t0_depot0_market4



set drive_t0_market1_depot0 2
drive truck0 market1 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market1 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market1_depot0
! drive_t0_market1_depot0



