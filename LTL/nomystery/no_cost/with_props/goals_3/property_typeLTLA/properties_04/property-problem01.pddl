state_set drive_t1_l4_l0 2
drive t1 l4 l0
drive t1 l0 l4

soft-LTL_property use_t1_l4_l0
<> drive_t1_l4_l0


state_set delwithT0_p1p2 4
unload p1 t0 location
load p1 t0 location
unload p2 t0 location
load p2 t0 location

state_set delwithT1_p1p2 4
unload p1 t1 location
load p1 t1 location
unload p2 t1 location
load p2 t1 location

soft-LTL_property same_truck_p1p2
|| [] ! delwithT0_p1p2 [] ! delwithT1_p1p2


state_set delwithT0_p0p2 4
unload p0 t0 location
load p0 t0 location
unload p2 t0 location
load p2 t0 location

state_set delwithT1_p0p2 4
unload p0 t1 location
load p0 t1 location
unload p2 t1 location
load p2 t1 location

soft-LTL_property same_truck_p0p2
|| [] ! delwithT0_p0p2 [] ! delwithT1_p0p2


state_set drive_t1_l0_l3 2
drive t1 l0 l3
drive t1 l3 l0

soft-LTL_property use_t1_l0_l3
<> drive_t1_l0_l3


