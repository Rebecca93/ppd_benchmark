set load_g1_m4 1
load goods1 truck market4


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m4_not_m2
&& load_g1_m4 ! load_g1_m2


set drive_t0_depot0_market3 2
drive truck0 depot0 market3 moneylevel moneylevel moneylevel
drive truck0 market3 depot0 moneylevel moneylevel moneylevel


soft-property use_not_t0_depot0_market3
! drive_t0_depot0_market3



set load_g3_m1 1
load goods3 truck market1


set load_g3_m5 1
load goods3 truck market5


soft-property load_g3_m1_not_m5
&& load_g3_m1 ! load_g3_m5


set load_g0_m5 1
load goods0 truck market5


set load_g0_m2 1
load goods0 truck market2


soft-property load_g0_m5_not_m2
&& load_g0_m5 ! load_g0_m2


set drive_t0_market2_market4 2
drive truck0 market2 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_market4
! drive_t0_market2_market4



set drive_t0_market3_market1 2
drive truck0 market3 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market3 moneylevel moneylevel moneylevel


soft-property use_not_t0_market3_market1
! drive_t0_market3_market1



set drive_t0_market2_market1 2
drive truck0 market2 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market1
drive_t0_market2_market1



set drive_t0_market1_depot0 2
drive truck0 market1 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market1 moneylevel moneylevel moneylevel


soft-property use_t0_market1_depot0
drive_t0_market1_depot0



