set load_g1_m4 1
load goods1 truck market4


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m4_not_m2
&& load_g1_m4 ! load_g1_m2


set drive_t0_depot0_market3 2
drive truck0 depot0 market3 moneylevel moneylevel moneylevel
drive truck0 market3 depot0 moneylevel moneylevel moneylevel


soft-property use_not_t0_depot0_market3
! drive_t0_depot0_market3



set load_g3_m1 1
load goods3 truck market1


set load_g3_m5 1
load goods3 truck market5


soft-property load_g3_m1_not_m5
&& load_g3_m1 ! load_g3_m5


