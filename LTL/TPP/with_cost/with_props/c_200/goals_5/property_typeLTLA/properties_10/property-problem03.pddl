state_set drive_t0_market4_depot0 2
drive truck0 market4 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_depot0
<> drive_t0_market4_depot0



state_set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market2_depot0
[] ! drive_t0_market2_depot0



state_set load_g3_m3 1
load goods3 truck market3


state_set load_g3_m2 1
load goods3 truck market2


soft-LTL_property load_g3_m3_not_m2
&& <> load_g3_m3 [] ! load_g3_m2


state_set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market3_depot0
[] ! drive_t0_market3_depot0



state_set drive_t0_market3_market2 2
drive truck0 market3 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market3 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market3_market2
<> drive_t0_market3_market2



state_set drive_t0_market1_market3 2
drive truck0 market1 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market1 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market1_market3
<> drive_t0_market1_market3



state_set load_g4_m5 1
load goods4 truck market5


state_set load_g4_m2 1
load goods4 truck market2


soft-LTL_property load_g4_m5_not_m2
&& <> load_g4_m5 [] ! load_g4_m2


state_set drive_t0_market2_market4 2
drive truck0 market2 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market2_market4
<> drive_t0_market2_market4



state_set drive_t0_market1_market4 2
drive truck0 market1 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market1 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market1_market4
[] ! drive_t0_market1_market4



state_set drive_t0_market2_market1 2
drive truck0 market2 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market2_market1
[] ! drive_t0_market2_market1



