state_set drive_t1_l4_l0 2
drive t1 l4 l0
drive t1 l0 l4

soft-LTL_property use_t1_l4_l0
<> drive_t1_l4_l0


state_set delwithT0_p2p3 4
unload p2 t0 location
load p2 t0 location
unload p3 t0 location
load p3 t0 location

state_set delwithT1_p2p3 4
unload p2 t1 location
load p2 t1 location
unload p3 t1 location
load p3 t1 location

soft-LTL_property same_truck_p2p3
|| [] ! delwithT0_p2p3 [] ! delwithT1_p2p3


state_set drive_t0_l1_l2 2
drive t0 l1 l2
drive t0 l2 l1

soft-LTL_property use_t0_l1_l2
<> drive_t0_l1_l2


state_set drive_t1_l5_l1 2
drive t1 l5 l1
drive t1 l1 l5

soft-LTL_property use_t1_l5_l1
<> drive_t1_l5_l1


state_set drive_t1_l0_l1 2
drive t1 l0 l1
drive t1 l1 l0

soft-LTL_property dont_use_t1_l0_l1
[] ! drive_t1_l0_l1


state_set delwithT0_p1p2 4
unload p1 t0 location
load p1 t0 location
unload p2 t0 location
load p2 t0 location

state_set delwithT1_p1p2 4
unload p1 t1 location
load p1 t1 location
unload p2 t1 location
load p2 t1 location

soft-LTL_property same_truck_p1p2
|| [] ! delwithT0_p1p2 [] ! delwithT1_p1p2


state_set drive_t0_l2_l0 2
drive t0 l2 l0
drive t0 l0 l2

soft-LTL_property use_t0_l2_l0
<> drive_t0_l2_l0


state_set drive_t0_l1_l3 2
drive t0 l1 l3
drive t0 l3 l1

soft-LTL_property use_t0_l1_l3
<> drive_t0_l1_l3


state_set drive_t0_l1_l5 2
drive t0 l1 l5
drive t0 l5 l1

soft-LTL_property use_t0_l1_l5
<> drive_t0_l1_l5


state_set delwithT0_p1p3 4
unload p1 t0 location
load p1 t0 location
unload p3 t0 location
load p3 t0 location

state_set delwithT1_p1p3 4
unload p1 t1 location
load p1 t1 location
unload p3 t1 location
load p3 t1 location

soft-LTL_property same_truck_p1p3
|| [] ! delwithT0_p1p3 [] ! delwithT1_p1p3


