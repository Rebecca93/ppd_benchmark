(define (problem satellite-p10_10dirs_9goals)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 instrument1 instrument2 - instrument
	image spectograph thermograph - mode
 dir0 dir1 dir2 dir3 dir4 dir5 dir6 dir7 dir8 dir9 - direction
)
(:init
    (supports instrument0 image)
    (supports instrument1 thermograph)
    (supports instrument2 spectograph)
    (calibration_target instrument0 dir0)
    (calibration_target instrument1 dir0)
    (calibration_target instrument2 dir0)
    (= (calibration_time instrument0 dir0) 5)
    (= (calibration_time instrument1 dir0) 5)
    (= (calibration_time instrument2 dir0) 5)
	   (on_board instrument0 satellite0)
	   (on_board instrument1 satellite0)
	   (on_board instrument2 satellite0)
    (available instrument0)
    (available instrument1)
    (available instrument2)
    (not_calibrated instrument0)
    (not_calibrated instrument1)
    (not_calibrated instrument2)

    (power_avail satellite0)
    (pointing satellite0 dir0)

    (= (slew_time dir0 dir1) 6)
    (= (slew_time dir1 dir0) 6)
    (= (slew_time dir0 dir2) 86)
    (= (slew_time dir2 dir0) 86)
    (= (slew_time dir0 dir3) 191)
    (= (slew_time dir3 dir0) 191)
    (= (slew_time dir0 dir4) 57)
    (= (slew_time dir4 dir0) 57)
    (= (slew_time dir0 dir5) 226)
    (= (slew_time dir5 dir0) 226)
    (= (slew_time dir0 dir6) 18)
    (= (slew_time dir6 dir0) 18)
    (= (slew_time dir0 dir7) 18)
    (= (slew_time dir7 dir0) 18)
    (= (slew_time dir0 dir8) 169)
    (= (slew_time dir8 dir0) 169)
    (= (slew_time dir0 dir9) 186)
    (= (slew_time dir9 dir0) 186)
    (= (slew_time dir1 dir0) 6)
    (= (slew_time dir0 dir1) 6)
    (= (slew_time dir1 dir2) 92)
    (= (slew_time dir2 dir1) 92)
    (= (slew_time dir1 dir3) 197)
    (= (slew_time dir3 dir1) 197)
    (= (slew_time dir1 dir4) 51)
    (= (slew_time dir4 dir1) 51)
    (= (slew_time dir1 dir5) 232)
    (= (slew_time dir5 dir1) 232)
    (= (slew_time dir1 dir6) 24)
    (= (slew_time dir6 dir1) 24)
    (= (slew_time dir1 dir7) 24)
    (= (slew_time dir7 dir1) 24)
    (= (slew_time dir1 dir8) 175)
    (= (slew_time dir8 dir1) 175)
    (= (slew_time dir1 dir9) 192)
    (= (slew_time dir9 dir1) 192)
    (= (slew_time dir2 dir0) 86)
    (= (slew_time dir0 dir2) 86)
    (= (slew_time dir2 dir1) 92)
    (= (slew_time dir1 dir2) 92)
    (= (slew_time dir2 dir3) 105)
    (= (slew_time dir3 dir2) 105)
    (= (slew_time dir2 dir4) 143)
    (= (slew_time dir4 dir2) 143)
    (= (slew_time dir2 dir5) 140)
    (= (slew_time dir5 dir2) 140)
    (= (slew_time dir2 dir6) 68)
    (= (slew_time dir6 dir2) 68)
    (= (slew_time dir2 dir7) 68)
    (= (slew_time dir7 dir2) 68)
    (= (slew_time dir2 dir8) 83)
    (= (slew_time dir8 dir2) 83)
    (= (slew_time dir2 dir9) 100)
    (= (slew_time dir9 dir2) 100)
    (= (slew_time dir3 dir0) 191)
    (= (slew_time dir0 dir3) 191)
    (= (slew_time dir3 dir1) 197)
    (= (slew_time dir1 dir3) 197)
    (= (slew_time dir3 dir2) 105)
    (= (slew_time dir2 dir3) 105)
    (= (slew_time dir3 dir4) 248)
    (= (slew_time dir4 dir3) 248)
    (= (slew_time dir3 dir5) 35)
    (= (slew_time dir5 dir3) 35)
    (= (slew_time dir3 dir6) 173)
    (= (slew_time dir6 dir3) 173)
    (= (slew_time dir3 dir7) 173)
    (= (slew_time dir7 dir3) 173)
    (= (slew_time dir3 dir8) 22)
    (= (slew_time dir8 dir3) 22)
    (= (slew_time dir3 dir9) 5)
    (= (slew_time dir9 dir3) 5)
    (= (slew_time dir4 dir0) 57)
    (= (slew_time dir0 dir4) 57)
    (= (slew_time dir4 dir1) 51)
    (= (slew_time dir1 dir4) 51)
    (= (slew_time dir4 dir2) 143)
    (= (slew_time dir2 dir4) 143)
    (= (slew_time dir4 dir3) 248)
    (= (slew_time dir3 dir4) 248)
    (= (slew_time dir4 dir5) 283)
    (= (slew_time dir5 dir4) 283)
    (= (slew_time dir4 dir6) 75)
    (= (slew_time dir6 dir4) 75)
    (= (slew_time dir4 dir7) 75)
    (= (slew_time dir7 dir4) 75)
    (= (slew_time dir4 dir8) 226)
    (= (slew_time dir8 dir4) 226)
    (= (slew_time dir4 dir9) 243)
    (= (slew_time dir9 dir4) 243)
    (= (slew_time dir5 dir0) 226)
    (= (slew_time dir0 dir5) 226)
    (= (slew_time dir5 dir1) 232)
    (= (slew_time dir1 dir5) 232)
    (= (slew_time dir5 dir2) 140)
    (= (slew_time dir2 dir5) 140)
    (= (slew_time dir5 dir3) 35)
    (= (slew_time dir3 dir5) 35)
    (= (slew_time dir5 dir4) 283)
    (= (slew_time dir4 dir5) 283)
    (= (slew_time dir5 dir6) 208)
    (= (slew_time dir6 dir5) 208)
    (= (slew_time dir5 dir7) 208)
    (= (slew_time dir7 dir5) 208)
    (= (slew_time dir5 dir8) 57)
    (= (slew_time dir8 dir5) 57)
    (= (slew_time dir5 dir9) 40)
    (= (slew_time dir9 dir5) 40)
    (= (slew_time dir6 dir0) 18)
    (= (slew_time dir0 dir6) 18)
    (= (slew_time dir6 dir1) 24)
    (= (slew_time dir1 dir6) 24)
    (= (slew_time dir6 dir2) 68)
    (= (slew_time dir2 dir6) 68)
    (= (slew_time dir6 dir3) 173)
    (= (slew_time dir3 dir6) 173)
    (= (slew_time dir6 dir4) 75)
    (= (slew_time dir4 dir6) 75)
    (= (slew_time dir6 dir5) 208)
    (= (slew_time dir5 dir6) 208)
    (= (slew_time dir6 dir7) 0)
    (= (slew_time dir7 dir6) 0)
    (= (slew_time dir6 dir8) 151)
    (= (slew_time dir8 dir6) 151)
    (= (slew_time dir6 dir9) 168)
    (= (slew_time dir9 dir6) 168)
    (= (slew_time dir7 dir0) 18)
    (= (slew_time dir0 dir7) 18)
    (= (slew_time dir7 dir1) 24)
    (= (slew_time dir1 dir7) 24)
    (= (slew_time dir7 dir2) 68)
    (= (slew_time dir2 dir7) 68)
    (= (slew_time dir7 dir3) 173)
    (= (slew_time dir3 dir7) 173)
    (= (slew_time dir7 dir4) 75)
    (= (slew_time dir4 dir7) 75)
    (= (slew_time dir7 dir5) 208)
    (= (slew_time dir5 dir7) 208)
    (= (slew_time dir7 dir6) 0)
    (= (slew_time dir6 dir7) 0)
    (= (slew_time dir7 dir8) 151)
    (= (slew_time dir8 dir7) 151)
    (= (slew_time dir7 dir9) 168)
    (= (slew_time dir9 dir7) 168)
    (= (slew_time dir8 dir0) 169)
    (= (slew_time dir0 dir8) 169)
    (= (slew_time dir8 dir1) 175)
    (= (slew_time dir1 dir8) 175)
    (= (slew_time dir8 dir2) 83)
    (= (slew_time dir2 dir8) 83)
    (= (slew_time dir8 dir3) 22)
    (= (slew_time dir3 dir8) 22)
    (= (slew_time dir8 dir4) 226)
    (= (slew_time dir4 dir8) 226)
    (= (slew_time dir8 dir5) 57)
    (= (slew_time dir5 dir8) 57)
    (= (slew_time dir8 dir6) 151)
    (= (slew_time dir6 dir8) 151)
    (= (slew_time dir8 dir7) 151)
    (= (slew_time dir7 dir8) 151)
    (= (slew_time dir8 dir9) 17)
    (= (slew_time dir9 dir8) 17)
    (= (slew_time dir9 dir0) 186)
    (= (slew_time dir0 dir9) 186)
    (= (slew_time dir9 dir1) 192)
    (= (slew_time dir1 dir9) 192)
    (= (slew_time dir9 dir2) 100)
    (= (slew_time dir2 dir9) 100)
    (= (slew_time dir9 dir3) 5)
    (= (slew_time dir3 dir9) 5)
    (= (slew_time dir9 dir4) 243)
    (= (slew_time dir4 dir9) 243)
    (= (slew_time dir9 dir5) 40)
    (= (slew_time dir5 dir9) 40)
    (= (slew_time dir9 dir6) 168)
    (= (slew_time dir6 dir9) 168)
    (= (slew_time dir9 dir7) 168)
    (= (slew_time dir7 dir9) 168)
    (= (slew_time dir9 dir8) 17)
    (= (slew_time dir8 dir9) 17)
)
(:goal (and
    (have_image dir1 image)
    (have_image dir2 image)
    (have_image dir3 image)
    (have_image dir4 image)
    (have_image dir5 image)
    (have_image dir6 image)
    (have_image dir7 image)
    (have_image dir8 image)
    (have_image dir9 image)
))
(:metric minimize (total-time))
)
