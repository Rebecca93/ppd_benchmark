state_set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_market5 2
drive truck0 market1 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market1 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market1_market5
[] ! drive_t0_market1_market5



state_set load_g3_m5 1
load goods3 truck market5


state_set load_g3_m4 1
load goods3 truck market4


soft-LTL_property load_g3_m5_not_m4
&& <> load_g3_m5 [] ! load_g3_m4


state_set drive_t0_market2_market5 2
drive truck0 market2 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market2_market5
[] ! drive_t0_market2_market5



