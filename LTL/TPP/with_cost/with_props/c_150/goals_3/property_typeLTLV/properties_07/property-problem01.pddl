soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property use_not_t0_market1_market3
&& [] || ! at(truck0,market1) ! X at(truck0,market3) [] || ! at(truck0,market3) ! X at(truck0,market1)


soft-LTL_property load_g1_m4_not_m2
<> && && available(goods1,market4) ! available(goods1,market2) X loaded(goods1,truck0)


soft-LTL_property use_not_t0_market3_depot0
&& [] || ! at(truck0,market3) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market3)


soft-LTL_property use_t0_market4_market2
|| <> && at(truck0,market4) X at(truck0,market2) <> && at(truck0,market2)  X at(truck0,market4)


soft-LTL_property use_t0_depot0_market5
|| <> && at(truck0,depot0) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,depot0)


soft-LTL_property load_g2_m3_not_m2
<> && && available(goods2,market3) ! available(goods2,market2) X loaded(goods2,truck0)


