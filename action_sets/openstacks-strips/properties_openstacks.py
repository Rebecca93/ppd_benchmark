import sys
import random
from random import shuffle

if len(sys.argv) < 3:
    print "usage: python properties_openstacks.py problem max_properties"
    print "This program generates the property file for a openstacks-strips problem instances."
    quit()

max_properties = int(sys.argv[2])

orders = 0

# read pfile and count orders and stacks
file = open(sys.argv[1], "r") 
for line in file:
    if "waiting" in line:
        orders = orders + 1

# declare action sets
orderlist = list(range(orders))
stacklist = []
shuffle(orderlist)

for i in range(0,max_properties):
    randstack = random.randint(0,3)
    stacklist.append(randstack)
    print "set start_order"+str(orderlist[i])+"_n"+str(randstack)+" 1"
    print "start-order-"+str(randstack)+" o"+str(orderlist[i])+" num num"
    print ""

# declare properties
for i in range(0,max_properties):
    print "soft-property start_order"+str(orderlist[i])+"_with_extrastacks"+str(stacklist[i])
    print "start_order"+str(orderlist[i])+"_n"+str(stacklist[i])
    print ""
