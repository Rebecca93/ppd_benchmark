soft-LTL_property use_t0_market5_market1
|| <> && at(truck0,market5) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market5)


soft-LTL_property use_not_t0_market2_market1
&& [] || ! at(truck0,market2) ! X at(truck0,market1) [] || ! at(truck0,market1) ! X at(truck0,market2)


soft-LTL_property load_g6_m3_not_m1
<> && && available(goods6,market3) ! available(goods6,market1) X loaded(goods6,truck0)


soft-LTL_property use_not_t0_market3_market5
&& [] || ! at(truck0,market3) ! X at(truck0,market5) [] || ! at(truck0,market5) ! X at(truck0,market3)


