set load_g3_m1 1
load goods3 truck market1


set load_g3_m3 1
load goods3 truck market3


soft-property load_g3_m1_not_m3
&& load_g3_m1 ! load_g3_m3


set load_g5_m5 1
load goods5 truck market5


set load_g5_m3 1
load goods5 truck market3


soft-property load_g5_m5_not_m3
&& load_g5_m5 ! load_g5_m3


set drive_t0_market2_market5 2
drive truck0 market2 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market5
drive_t0_market2_market5



set drive_t0_market2_market3 2
drive truck0 market2 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market3
drive_t0_market2_market3



set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set load_g2_m4 1
load goods2 truck market4


set load_g2_m5 1
load goods2 truck market5


soft-property load_g2_m4_not_m5
&& load_g2_m4 ! load_g2_m5


set load_g6_m5 1
load goods6 truck market5


set load_g6_m3 1
load goods6 truck market3


soft-property load_g6_m5_not_m3
&& load_g6_m5 ! load_g6_m3


set load_g1_m1 1
load goods1 truck market1


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m1_not_m2
&& load_g1_m1 ! load_g1_m2


set drive_t0_market2_market1 2
drive truck0 market2 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market1
drive_t0_market2_market1



