set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set drive_t0_market1_market3 2
drive truck0 market1 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market1 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market1_market3
! drive_t0_market1_market3



set load_g6_m3 1
load goods6 truck market3


set load_g6_m1 1
load goods6 truck market1


soft-AS_property load_g6_m3_not_m1
&& load_g6_m3 ! load_g6_m1


set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market4_market2 2
drive truck0 market4 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market2
drive_t0_market4_market2



set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-AS_property use_t0_depot0_market5
drive_t0_depot0_market5



set load_g3_m5 1
load goods3 truck market5


set load_g3_m1 1
load goods3 truck market1


soft-AS_property load_g3_m5_not_m1
&& load_g3_m5 ! load_g3_m1


