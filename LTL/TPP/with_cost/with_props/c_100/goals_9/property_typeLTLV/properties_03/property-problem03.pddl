soft-LTL_property use_t0_market4_depot0
|| <> && at(truck0,market4) X at(truck0,depot0) <> && at(truck0,depot0)  X at(truck0,market4)


soft-LTL_property use_not_t0_market2_depot0
&& [] || ! at(truck0,market2) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market2)


soft-LTL_property load_g6_m4_not_m3
<> && && available(goods6,market4) ! available(goods6,market3) X loaded(goods6,truck0)


