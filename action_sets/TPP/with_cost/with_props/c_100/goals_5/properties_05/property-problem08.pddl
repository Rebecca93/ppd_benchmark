set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-property use_t0_depot0_market5
drive_t0_depot0_market5



set load_g3_m5 1
load goods3 truck market5


set load_g3_m4 1
load goods3 truck market4


soft-property load_g3_m5_not_m4
&& load_g3_m5 ! load_g3_m4


set drive_t0_market2_market1 2
drive truck0 market2 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_market1
! drive_t0_market2_market1



set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set drive_t0_market1_market5 2
drive truck0 market1 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market1 moneylevel moneylevel moneylevel


soft-property use_t0_market1_market5
drive_t0_market1_market5



