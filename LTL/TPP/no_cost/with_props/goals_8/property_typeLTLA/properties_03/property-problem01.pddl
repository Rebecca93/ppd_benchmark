state_set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-LTL_property use_not_t0_market1_market3
[] ! drive_t0_market1_market3



state_set load_g6_m3 1
load goods6 truck market3


state_set load_g6_m1 1
load goods6 truck market1


soft-LTL_property load_g6_m3_not_m1
&& <> load_g6_m3 [] ! load_g6_m1


