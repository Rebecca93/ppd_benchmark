set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set load_g6_m1 1
load goods6 truck market1


set load_g6_m3 1
load goods6 truck market3


soft-property load_g6_m1_not_m3
&& load_g6_m1 ! load_g6_m3


set drive_t0_market4_depot0 2
drive truck0 market4 depot0
drive truck0 depot0 market4

soft-property use_not_t0_market4_depot0
! drive_t0_market4_depot0



set drive_t0_market5_market1 2
drive truck0 market5 market1
drive truck0 market1 market5

soft-property use_t0_market5_market1
drive_t0_market5_market1



set drive_t0_depot0_market1 2
drive truck0 depot0 market1
drive truck0 market1 depot0

soft-property use_t0_depot0_market1
drive_t0_depot0_market1



set drive_t0_market2_market1 2
drive truck0 market2 market1
drive truck0 market1 market2

soft-property use_t0_market2_market1
drive_t0_market2_market1



