state_set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-LTL_property use_t0_market4_market5
<> drive_t0_market4_market5



state_set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-LTL_property use_not_t0_market1_market5
[] ! drive_t0_market1_market5



state_set load_g3_m5 1
load goods3 truck market5


state_set load_g3_m2 1
load goods3 truck market2


soft-LTL_property load_g3_m5_not_m2
&& <> load_g3_m5 [] ! load_g3_m2


state_set drive_t0_market2_market4 2
drive truck0 market2 market4
drive truck0 market4 market2

soft-LTL_property use_not_t0_market2_market4
[] ! drive_t0_market2_market4



state_set drive_t0_market2_market5 2
drive truck0 market2 market5
drive truck0 market5 market2

soft-LTL_property use_t0_market2_market5
<> drive_t0_market2_market5



state_set drive_t0_depot0_market2 2
drive truck0 depot0 market2
drive truck0 market2 depot0

soft-LTL_property use_t0_depot0_market2
<> drive_t0_depot0_market2



state_set load_g4_m3 1
load goods4 truck market3


state_set load_g4_m1 1
load goods4 truck market1


soft-LTL_property load_g4_m3_not_m1
&& <> load_g4_m3 [] ! load_g4_m1


state_set drive_t0_market1_market2 2
drive truck0 market1 market2
drive truck0 market2 market1

soft-LTL_property use_t0_market1_market2
<> drive_t0_market1_market2



state_set drive_t0_depot0_market4 2
drive truck0 depot0 market4
drive truck0 market4 depot0

soft-LTL_property use_not_t0_depot0_market4
[] ! drive_t0_depot0_market4



state_set drive_t0_market1_depot0 2
drive truck0 market1 depot0
drive truck0 depot0 market1

soft-LTL_property use_not_t0_market1_depot0
[] ! drive_t0_market1_depot0



