set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set load_g6_m5 1
load goods6 truck market5


set load_g6_m3 1
load goods6 truck market3


soft-AS_property load_g6_m5_not_m3
&& load_g6_m5 ! load_g6_m3


set load_g0_m2 1
load goods0 truck market2


set load_g0_m5 1
load goods0 truck market5


soft-AS_property load_g0_m2_not_m5
&& load_g0_m2 ! load_g0_m5


