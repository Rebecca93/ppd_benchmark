soft-LTL_property use_t0_market5_market1
|| <> && at(truck0,market5) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market5)


soft-LTL_property use_not_t0_market2_market1
&& [] || ! at(truck0,market2) ! X at(truck0,market1) [] || ! at(truck0,market1) ! X at(truck0,market2)


soft-LTL_property load_g3_m4_not_m1
<> && && available(goods3,market4) ! available(goods3,market1) X loaded(goods3,truck0)


