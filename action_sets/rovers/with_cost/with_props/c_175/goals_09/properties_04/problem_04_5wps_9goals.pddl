(define (problem rovers-p04_5wps_9goals)

(:domain Rover)

(:objects

    general - Lander

    colour high_res low_res - Mode

    rover0 rover1 - Rover

    rover0store rover1store - Store

    camera00 camera01 camera02 camera10 camera11 camera12 - Camera

    waypoint0 waypoint1 waypoint2 waypoint3 waypoint4 - Waypoint

    objective0 objective1 objective2 objective3 objective4 objective5 objective6 - Objective

level0 level1 level2 level3 level4 level5 level6 level7 level8 level9 level10 level11 level12 level13 level14 level15  - energylevel
)

(:init

    (energy level15)
    (sum level0 level0 level0)
    (sum level0 level1 level1)
    (sum level0 level2 level2)
    (sum level0 level3 level3)
    (sum level0 level4 level4)
    (sum level0 level5 level5)
    (sum level0 level6 level6)
    (sum level0 level7 level7)
    (sum level0 level8 level8)
    (sum level0 level9 level9)
    (sum level0 level10 level10)
    (sum level0 level11 level11)
    (sum level0 level12 level12)
    (sum level0 level13 level13)
    (sum level0 level14 level14)
    (sum level0 level15 level15)
    (sum level1 level0 level1)
    (sum level1 level1 level2)
    (sum level1 level2 level3)
    (sum level1 level3 level4)
    (sum level1 level4 level5)
    (sum level1 level5 level6)
    (sum level1 level6 level7)
    (sum level1 level7 level8)
    (sum level1 level8 level9)
    (sum level1 level9 level10)
    (sum level1 level10 level11)
    (sum level1 level11 level12)
    (sum level1 level12 level13)
    (sum level1 level13 level14)
    (sum level1 level14 level15)
    (sum level2 level0 level2)
    (sum level2 level1 level3)
    (sum level2 level2 level4)
    (sum level2 level3 level5)
    (sum level2 level4 level6)
    (sum level2 level5 level7)
    (sum level2 level6 level8)
    (sum level2 level7 level9)
    (sum level2 level8 level10)
    (sum level2 level9 level11)
    (sum level2 level10 level12)
    (sum level2 level11 level13)
    (sum level2 level12 level14)
    (sum level2 level13 level15)
    (sum level3 level0 level3)
    (sum level3 level1 level4)
    (sum level3 level2 level5)
    (sum level3 level3 level6)
    (sum level3 level4 level7)
    (sum level3 level5 level8)
    (sum level3 level6 level9)
    (sum level3 level7 level10)
    (sum level3 level8 level11)
    (sum level3 level9 level12)
    (sum level3 level10 level13)
    (sum level3 level11 level14)
    (sum level3 level12 level15)
    (sum level4 level0 level4)
    (sum level4 level1 level5)
    (sum level4 level2 level6)
    (sum level4 level3 level7)
    (sum level4 level4 level8)
    (sum level4 level5 level9)
    (sum level4 level6 level10)
    (sum level4 level7 level11)
    (sum level4 level8 level12)
    (sum level4 level9 level13)
    (sum level4 level10 level14)
    (sum level4 level11 level15)
    (sum level5 level0 level5)
    (sum level5 level1 level6)
    (sum level5 level2 level7)
    (sum level5 level3 level8)
    (sum level5 level4 level9)
    (sum level5 level5 level10)
    (sum level5 level6 level11)
    (sum level5 level7 level12)
    (sum level5 level8 level13)
    (sum level5 level9 level14)
    (sum level5 level10 level15)
    (sum level6 level0 level6)
    (sum level6 level1 level7)
    (sum level6 level2 level8)
    (sum level6 level3 level9)
    (sum level6 level4 level10)
    (sum level6 level5 level11)
    (sum level6 level6 level12)
    (sum level6 level7 level13)
    (sum level6 level8 level14)
    (sum level6 level9 level15)
    (sum level7 level0 level7)
    (sum level7 level1 level8)
    (sum level7 level2 level9)
    (sum level7 level3 level10)
    (sum level7 level4 level11)
    (sum level7 level5 level12)
    (sum level7 level6 level13)
    (sum level7 level7 level14)
    (sum level7 level8 level15)
    (sum level8 level0 level8)
    (sum level8 level1 level9)
    (sum level8 level2 level10)
    (sum level8 level3 level11)
    (sum level8 level4 level12)
    (sum level8 level5 level13)
    (sum level8 level6 level14)
    (sum level8 level7 level15)
    (sum level9 level0 level9)
    (sum level9 level1 level10)
    (sum level9 level2 level11)
    (sum level9 level3 level12)
    (sum level9 level4 level13)
    (sum level9 level5 level14)
    (sum level9 level6 level15)
    (sum level10 level0 level10)
    (sum level10 level1 level11)
    (sum level10 level2 level12)
    (sum level10 level3 level13)
    (sum level10 level4 level14)
    (sum level10 level5 level15)
    (sum level11 level0 level11)
    (sum level11 level1 level12)
    (sum level11 level2 level13)
    (sum level11 level3 level14)
    (sum level11 level4 level15)
    (sum level12 level0 level12)
    (sum level12 level1 level13)
    (sum level12 level2 level14)
    (sum level12 level3 level15)
    (sum level13 level0 level13)
    (sum level13 level1 level14)
    (sum level13 level2 level15)
    (sum level14 level0 level14)
    (sum level14 level1 level15)
    (sum level15 level0 level15)
    (at_lander general waypoint0)

    (channel_free general)

    (visible waypoint0 waypoint0)

    (visible waypoint0 waypoint1)

    (energycost level16 waypoint0 waypoint1)

    (can_traverse rover0 waypoint0 waypoint1)

    (can_traverse rover1 waypoint0 waypoint1)

    (visible waypoint0 waypoint2)

    (energycost level2 waypoint0 waypoint2)

    (can_traverse rover0 waypoint0 waypoint2)

    (can_traverse rover1 waypoint0 waypoint2)

    (visible waypoint0 waypoint3)

    (energycost level4 waypoint0 waypoint3)

    (can_traverse rover0 waypoint0 waypoint3)

    (can_traverse rover1 waypoint0 waypoint3)

    (visible waypoint0 waypoint4)

    (energycost level4 waypoint0 waypoint4)

    (can_traverse rover0 waypoint0 waypoint4)

    (can_traverse rover1 waypoint0 waypoint4)

    (visible waypoint1 waypoint0)

    (energycost level7 waypoint1 waypoint0)

    (can_traverse rover0 waypoint1 waypoint0)

    (can_traverse rover1 waypoint1 waypoint0)

    (visible waypoint1 waypoint1)

    (visible waypoint1 waypoint2)

    (energycost level11 waypoint1 waypoint2)

    (can_traverse rover0 waypoint1 waypoint2)

    (can_traverse rover1 waypoint1 waypoint2)

    (visible waypoint1 waypoint3)

    (energycost level2 waypoint1 waypoint3)

    (can_traverse rover0 waypoint1 waypoint3)

    (can_traverse rover1 waypoint1 waypoint3)

    (visible waypoint1 waypoint4)

    (energycost level7 waypoint1 waypoint4)

    (can_traverse rover0 waypoint1 waypoint4)

    (can_traverse rover1 waypoint1 waypoint4)

    (visible waypoint2 waypoint0)

    (energycost level12 waypoint2 waypoint0)

    (can_traverse rover0 waypoint2 waypoint0)

    (can_traverse rover1 waypoint2 waypoint0)

    (visible waypoint2 waypoint1)

    (energycost level7 waypoint2 waypoint1)

    (can_traverse rover0 waypoint2 waypoint1)

    (can_traverse rover1 waypoint2 waypoint1)

    (visible waypoint2 waypoint2)

    (visible waypoint2 waypoint3)

    (energycost level13 waypoint2 waypoint3)

    (can_traverse rover0 waypoint2 waypoint3)

    (can_traverse rover1 waypoint2 waypoint3)

    (visible waypoint2 waypoint4)

    (energycost level4 waypoint2 waypoint4)

    (can_traverse rover0 waypoint2 waypoint4)

    (can_traverse rover1 waypoint2 waypoint4)

    (visible waypoint3 waypoint0)

    (energycost level1 waypoint3 waypoint0)

    (can_traverse rover0 waypoint3 waypoint0)

    (can_traverse rover1 waypoint3 waypoint0)

    (visible waypoint3 waypoint1)

    (energycost level13 waypoint3 waypoint1)

    (can_traverse rover0 waypoint3 waypoint1)

    (can_traverse rover1 waypoint3 waypoint1)

    (visible waypoint3 waypoint2)

    (energycost level8 waypoint3 waypoint2)

    (can_traverse rover0 waypoint3 waypoint2)

    (can_traverse rover1 waypoint3 waypoint2)

    (visible waypoint3 waypoint3)

    (visible waypoint3 waypoint4)

    (energycost level8 waypoint3 waypoint4)

    (can_traverse rover0 waypoint3 waypoint4)

    (can_traverse rover1 waypoint3 waypoint4)

    (visible waypoint4 waypoint0)

    (energycost level1 waypoint4 waypoint0)

    (can_traverse rover0 waypoint4 waypoint0)

    (can_traverse rover1 waypoint4 waypoint0)

    (visible waypoint4 waypoint1)

    (energycost level14 waypoint4 waypoint1)

    (can_traverse rover0 waypoint4 waypoint1)

    (can_traverse rover1 waypoint4 waypoint1)

    (visible waypoint4 waypoint2)

    (energycost level8 waypoint4 waypoint2)

    (can_traverse rover0 waypoint4 waypoint2)

    (can_traverse rover1 waypoint4 waypoint2)

    (visible waypoint4 waypoint3)

    (energycost level9 waypoint4 waypoint3)

    (can_traverse rover0 waypoint4 waypoint3)

    (can_traverse rover1 waypoint4 waypoint3)

    (visible waypoint4 waypoint4)

    (at_rock_sample waypoint3)

    (at_rock_sample waypoint4)

    (visible_from objective0 waypoint3)

    (visible_from objective1 waypoint0)

    (visible_from objective2 waypoint2)

    (visible_from objective2 waypoint3)

    (visible_from objective2 waypoint4)

    (visible_from objective3 waypoint1)

    (visible_from objective3 waypoint2)

    (visible_from objective3 waypoint3)

    (visible_from objective3 waypoint4)

    (visible_from objective4 waypoint0)

    (visible_from objective4 waypoint1)

    (visible_from objective4 waypoint2)

    (visible_from objective5 waypoint0)

    (visible_from objective5 waypoint3)

    (visible_from objective6 waypoint0)

    (visible_from objective6 waypoint1)

    (visible_from objective6 waypoint2)

    (visible_from objective6 waypoint3)

    (visible_from objective6 waypoint4)

    (at rover0 waypoint0)

    (available rover0)

    (store_of rover0store rover0)

    (empty rover0store)

    (equipped_for_soil_analysis rover0)

    (equipped_for_rock_analysis rover0)

    (equipped_for_imaging rover0)

    (on_board camera00 rover0)

    (supports camera00 colour)

    (supports camera00 high_res)

    (supports camera00 low_res)

    (calibration_target camera00 objective0 )

    (calibration_target camera00 objective1 )

    (calibration_target camera00 objective2 )

    (calibration_target camera00 objective3 )

    (calibration_target camera00 objective4 )

    (calibration_target camera00 objective5 )

    (calibration_target camera00 objective6 )

    (on_board camera01 rover0)

    (supports camera01 colour)

    (supports camera01 high_res)

    (supports camera01 low_res)

    (calibration_target camera01 objective0 )

    (calibration_target camera01 objective1 )

    (calibration_target camera01 objective2 )

    (calibration_target camera01 objective3 )

    (calibration_target camera01 objective4 )

    (calibration_target camera01 objective5 )

    (calibration_target camera01 objective6 )

    (on_board camera02 rover0)

    (supports camera02 colour)

    (supports camera02 high_res)

    (supports camera02 low_res)

    (calibration_target camera02 objective0 )

    (calibration_target camera02 objective1 )

    (calibration_target camera02 objective2 )

    (calibration_target camera02 objective3 )

    (calibration_target camera02 objective4 )

    (calibration_target camera02 objective5 )

    (calibration_target camera02 objective6 )

    (at rover1 waypoint0)

    (available rover1)

    (store_of rover1store rover1)

    (empty rover1store)

    (equipped_for_soil_analysis rover1)

    (equipped_for_rock_analysis rover1)

    (equipped_for_imaging rover1)

    (on_board camera10 rover1)

    (supports camera10 colour)

    (supports camera10 high_res)

    (supports camera10 low_res)

    (calibration_target camera10 objective0 )

    (calibration_target camera10 objective1 )

    (calibration_target camera10 objective2 )

    (calibration_target camera10 objective3 )

    (calibration_target camera10 objective4 )

    (calibration_target camera10 objective5 )

    (calibration_target camera10 objective6 )

    (on_board camera11 rover1)

    (supports camera11 colour)

    (supports camera11 high_res)

    (supports camera11 low_res)

    (calibration_target camera11 objective0 )

    (calibration_target camera11 objective1 )

    (calibration_target camera11 objective2 )

    (calibration_target camera11 objective3 )

    (calibration_target camera11 objective4 )

    (calibration_target camera11 objective5 )

    (calibration_target camera11 objective6 )

    (on_board camera12 rover1)

    (supports camera12 colour)

    (supports camera12 high_res)

    (supports camera12 low_res)

    (calibration_target camera12 objective0 )

    (calibration_target camera12 objective1 )

    (calibration_target camera12 objective2 )

    (calibration_target camera12 objective3 )

    (calibration_target camera12 objective4 )

    (calibration_target camera12 objective5 )

    (calibration_target camera12 objective6 )

)

(:goal (and

    (communicated_rock_data waypoint3)

    (communicated_rock_data waypoint4)

    (communicated_image_data objective0 high_res)

    (communicated_image_data objective1 high_res)

    (communicated_image_data objective2 high_res)

    (communicated_image_data objective3 high_res)

    (communicated_image_data objective4 low_res)

    (communicated_image_data objective5 low_res)

    (communicated_image_data objective6 colour)

))

)

