set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



set drive_t0_market1_depot0 2
drive truck0 market1 depot0
drive truck0 depot0 market1

soft-AS_property use_not_t0_market1_depot0
! drive_t0_market1_depot0



set load_g6_m5 1
load goods6 truck market5


set load_g6_m1 1
load goods6 truck market1


soft-AS_property load_g6_m5_not_m1
&& load_g6_m5 ! load_g6_m1


