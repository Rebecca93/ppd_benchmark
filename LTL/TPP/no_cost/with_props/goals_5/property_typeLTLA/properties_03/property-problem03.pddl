state_set drive_t0_market4_depot0 2
drive truck0 market4 depot0
drive truck0 depot0 market4

soft-LTL_property use_t0_market4_depot0
<> drive_t0_market4_depot0



state_set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-LTL_property use_not_t0_market2_depot0
[] ! drive_t0_market2_depot0



state_set load_g3_m3 1
load goods3 truck market3


state_set load_g3_m2 1
load goods3 truck market2


soft-LTL_property load_g3_m3_not_m2
&& <> load_g3_m3 [] ! load_g3_m2


