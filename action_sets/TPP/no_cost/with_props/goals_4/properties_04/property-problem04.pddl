set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set load_g1_m3 1
load goods1 truck market3


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m3_not_m2
&& load_g1_m3 ! load_g1_m2


set drive_t0_depot0_market1 2
drive truck0 depot0 market1
drive truck0 market1 depot0

soft-property use_not_t0_depot0_market1
! drive_t0_depot0_market1



set load_g0_m1 1
load goods0 truck market1


set load_g0_m2 1
load goods0 truck market2


soft-property load_g0_m1_not_m2
&& load_g0_m1 ! load_g0_m2


