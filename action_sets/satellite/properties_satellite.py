import sys
import random
import string
from random import shuffle

if len(sys.argv) < 3:
    print "usage: python properties_satellite.py problem max_properties"
    print "This program generates the property file for a satellite problem instance."
    quit()

max_properties = int(sys.argv[2])

instruments = ["instrument1", "instrument2"]
dirs = []
for i in range(0,10):
    dirs.append("dir"+str(i))

# declare action sets
asets = [(x, y) for x in instruments for y in dirs]
shuffle(asets)

for i in range(0,max_properties):
    print "set take_image_with_"+str(asets[i][0])+"_"+str(asets[i][1])+" 1"
    print "take_image satellite "+str(asets[i][1])+" "+str(asets[i][0]) + " mode"
    print ""

# declare properties
for i in range(0,max_properties):
    print "soft-property tiw_"+str(asets[i][0])+"_"+str(asets[i][1])
    print "take_image_with_"+str(asets[i][0])+"_"+str(asets[i][1])
    print ""
