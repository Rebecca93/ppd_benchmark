state_set drive_t0_market4_market5 2
drive truck0 market4 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_market5
<> drive_t0_market4_market5



state_set drive_t0_market1_depot0 2
drive truck0 market1 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market1 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market1_depot0
[] ! drive_t0_market1_depot0



state_set load_g3_m3 1
load goods3 truck market3


state_set load_g3_m2 1
load goods3 truck market2


soft-LTL_property load_g3_m3_not_m2
&& <> load_g3_m3 [] ! load_g3_m2


state_set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market3_depot0
[] ! drive_t0_market3_depot0



state_set drive_t0_market4_depot0 2
drive truck0 market4 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_depot0
<> drive_t0_market4_depot0



state_set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_depot0_market5
<> drive_t0_depot0_market5



state_set load_g1_m4 1
load goods1 truck market4


state_set load_g1_m2 1
load goods1 truck market2


soft-LTL_property load_g1_m4_not_m2
&& <> load_g1_m4 [] ! load_g1_m2


