(define (problem blocksworld-p08_9)
(:domain blocksworld)
(:objects
a b c d e f g h i j - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear j)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
(on h g)
(on i h)
(on j i)
)
(:goal (and
(on i g)
(on e i)
(on b e)
(on f b)
(on j f)
(on d j)
(on a d)
(on c a)
(on h c)
))
(:metric minimize (total-cost))
)
