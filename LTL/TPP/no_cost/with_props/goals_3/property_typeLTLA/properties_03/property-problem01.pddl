state_set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-LTL_property use_not_t0_market1_market3
[] ! drive_t0_market1_market3



state_set load_g1_m4 1
load goods1 truck market4


state_set load_g1_m2 1
load goods1 truck market2


soft-LTL_property load_g1_m4_not_m2
&& <> load_g1_m4 [] ! load_g1_m2


