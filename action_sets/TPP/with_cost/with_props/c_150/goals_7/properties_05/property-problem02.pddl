set drive_t0_market3_market1 2
drive truck0 market3 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market3 moneylevel moneylevel moneylevel


soft-property use_t0_market3_market1
drive_t0_market3_market1



set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set drive_t0_market2_market3 2
drive truck0 market2 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_market3
! drive_t0_market2_market3



set load_g3_m3 1
load goods3 truck market3


set load_g3_m5 1
load goods3 truck market5


soft-property load_g3_m3_not_m5
&& load_g3_m3 ! load_g3_m5


set load_g5_m2 1
load goods5 truck market2


set load_g5_m4 1
load goods5 truck market4


soft-property load_g5_m2_not_m4
&& load_g5_m2 ! load_g5_m4


