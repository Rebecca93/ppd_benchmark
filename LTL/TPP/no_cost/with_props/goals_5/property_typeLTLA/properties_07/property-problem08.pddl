state_set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-LTL_property use_not_t0_market1_market5
[] ! drive_t0_market1_market5



state_set load_g3_m5 1
load goods3 truck market5


state_set load_g3_m4 1
load goods3 truck market4


soft-LTL_property load_g3_m5_not_m4
&& <> load_g3_m5 [] ! load_g3_m4


state_set drive_t0_market2_market5 2
drive truck0 market2 market5
drive truck0 market5 market2

soft-LTL_property use_not_t0_market2_market5
[] ! drive_t0_market2_market5



state_set drive_t0_market3_market1 2
drive truck0 market3 market1
drive truck0 market1 market3

soft-LTL_property use_t0_market3_market1
<> drive_t0_market3_market1



state_set drive_t0_depot0_market4 2
drive truck0 depot0 market4
drive truck0 market4 depot0

soft-LTL_property use_t0_depot0_market4
<> drive_t0_depot0_market4



state_set load_g4_m3 1
load goods4 truck market3


state_set load_g4_m2 1
load goods4 truck market2


soft-LTL_property load_g4_m3_not_m2
&& <> load_g4_m3 [] ! load_g4_m2


