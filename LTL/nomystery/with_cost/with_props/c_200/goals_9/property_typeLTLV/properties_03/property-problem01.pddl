soft-LTL_property use_t1_l4_l0
|| <> && at(t1,l4) X at(t1,l0) <> && at(t1,l0)  X at(t1,l4)


soft-LTL_property same_truck_p5p6
|| [] && ! in(p5,t1) ! in(p6,t1)  [] && ! in(p5,t0) ! in(p6,t0)


soft-LTL_property use_t0_l0_l5
|| <> && at(t0,l0) X at(t0,l5) <> && at(t0,l5)  X at(t0,l0)


