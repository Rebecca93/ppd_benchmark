set drive_t1_l4_l2 2
drive t1 l4 l2
drive t1 l2 l4

soft-AS_property use_t1_l4_l2
drive_t1_l4_l2


set delwithT0_p1p2 4
unload p1 t0 location
load p1 t0 location
unload p2 t0 location
load p2 t0 location

set delwithT1_p1p2 4
unload p1 t1 location
load p1 t1 location
unload p2 t1 location
load p2 t1 location

soft-AS_property same_truck_p1p2
|| && delwithT0_p1p2 ! delwithT1_p1p2 && ! delwithT0_p1p2 delwithT1_p1p2


