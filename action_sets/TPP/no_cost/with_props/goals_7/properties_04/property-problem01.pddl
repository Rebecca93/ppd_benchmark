set load_g6_m1 1
load goods6 truck market1


set load_g6_m3 1
load goods6 truck market3


soft-property load_g6_m1_not_m3
&& load_g6_m1 ! load_g6_m3


set load_g2_m3 1
load goods2 truck market3


set load_g2_m2 1
load goods2 truck market2


soft-property load_g2_m3_not_m2
&& load_g2_m3 ! load_g2_m2


set drive_t0_market5_market2 2
drive truck0 market5 market2
drive truck0 market2 market5

soft-property use_t0_market5_market2
drive_t0_market5_market2



set drive_t0_market2_market4 2
drive truck0 market2 market4
drive truck0 market4 market2

soft-property use_t0_market2_market4
drive_t0_market2_market4



