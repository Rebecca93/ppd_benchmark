state_set drive_t0_market5_market1 2
drive truck0 market5 market1
drive truck0 market1 market5

soft-LTL_property use_t0_market5_market1
<> drive_t0_market5_market1



state_set drive_t0_market3_market2 2
drive truck0 market3 market2
drive truck0 market2 market3

soft-LTL_property use_not_t0_market3_market2
[] ! drive_t0_market3_market2



state_set load_g6_m2 1
load goods6 truck market2


state_set load_g6_m1 1
load goods6 truck market1


soft-LTL_property load_g6_m2_not_m1
&& <> load_g6_m2 [] ! load_g6_m1


state_set drive_t0_market4_market2 2
drive truck0 market4 market2
drive truck0 market2 market4

soft-LTL_property use_not_t0_market4_market2
[] ! drive_t0_market4_market2



