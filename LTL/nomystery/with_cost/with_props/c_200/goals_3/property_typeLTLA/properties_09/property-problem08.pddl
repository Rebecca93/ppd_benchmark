state_set drive_t1_l4_l3 2
drive t1 l4 l3 fuellevel fuellevel fuellevel
drive t1 l3 l4 fuellevel fuellevel fuellevel

soft-LTL_property use_t1_l4_l3
<> drive_t1_l4_l3


state_set delwithT0_p1p2 4
unload p1 t0 location
load p1 t0 location
unload p2 t0 location
load p2 t0 location

state_set delwithT1_p1p2 4
unload p1 t1 location
load p1 t1 location
unload p2 t1 location
load p2 t1 location

soft-LTL_property same_truck_p1p2
|| [] ! delwithT0_p1p2 [] ! delwithT1_p1p2


state_set delwithT0_p0p2 4
unload p0 t0 location
load p0 t0 location
unload p2 t0 location
load p2 t0 location

state_set delwithT1_p0p2 4
unload p0 t1 location
load p0 t1 location
unload p2 t1 location
load p2 t1 location

soft-LTL_property same_truck_p0p2
|| [] ! delwithT0_p0p2 [] ! delwithT1_p0p2


state_set drive_t1_l1_l0 2
drive t1 l1 l0 fuellevel fuellevel fuellevel
drive t1 l0 l1 fuellevel fuellevel fuellevel

soft-LTL_property use_t1_l1_l0
<> drive_t1_l1_l0


state_set delwithT0_p0p1 4
unload p0 t0 location
load p0 t0 location
unload p1 t0 location
load p1 t0 location

state_set delwithT1_p0p1 4
unload p0 t1 location
load p0 t1 location
unload p1 t1 location
load p1 t1 location

soft-LTL_property same_truck_p0p1
|| [] ! delwithT0_p0p1 [] ! delwithT1_p0p1


state_set drive_t1_l1_l4 2
drive t1 l1 l4 fuellevel fuellevel fuellevel
drive t1 l4 l1 fuellevel fuellevel fuellevel

soft-LTL_property use_t1_l1_l4
<> drive_t1_l1_l4


state_set drive_t0_l5_l4 2
drive t0 l5 l4 fuellevel fuellevel fuellevel
drive t0 l4 l5 fuellevel fuellevel fuellevel

soft-LTL_property use_t0_l5_l4
<> drive_t0_l5_l4


state_set drive_t0_l0_l1 2
drive t0 l0 l1 fuellevel fuellevel fuellevel
drive t0 l1 l0 fuellevel fuellevel fuellevel

soft-LTL_property use_t0_l0_l1
<> drive_t0_l0_l1


state_set drive_t1_l3_l0 2
drive t1 l3 l0 fuellevel fuellevel fuellevel
drive t1 l0 l3 fuellevel fuellevel fuellevel

soft-LTL_property use_t1_l3_l0
<> drive_t1_l3_l0


