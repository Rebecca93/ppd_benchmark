set drive_t0_market5_market1 2
drive truck0 market5 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market5 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market5_market1
drive_t0_market5_market1



set drive_t0_market3_market2 2
drive truck0 market3 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market3 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market3_market2
! drive_t0_market3_market2



set load_g3_m4 1
load goods3 truck market4


set load_g3_m1 1
load goods3 truck market1


soft-AS_property load_g3_m4_not_m1
&& load_g3_m4 ! load_g3_m1


set drive_t0_market4_market2 2
drive truck0 market4 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market4 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market4_market2
! drive_t0_market4_market2



set drive_t0_market4_market5 2
drive truck0 market4 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



