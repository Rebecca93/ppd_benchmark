(define (problem os-time-p10_11)
(:domain openstacks-time-nonADL-nonNegated)
(:objects level6 level7 level8 level9 level10 level11 level12 level13 - num)
(:init

(sum level0 level0 level0)
(sum level0 level1 level1)
(sum level0 level2 level2)
(sum level0 level3 level3)
(sum level0 level4 level4)
(sum level0 level5 level5)
(sum level0 level6 level6)
(sum level0 level7 level7)
(sum level0 level8 level8)
(sum level0 level9 level9)
(sum level0 level10 level10)
(sum level0 level11 level11)
(sum level0 level12 level12)
(sum level0 level13 level13)
(sum level1 level0 level1)
(sum level1 level1 level2)
(sum level1 level2 level3)
(sum level1 level3 level4)
(sum level1 level4 level5)
(sum level1 level5 level6)
(sum level1 level6 level7)
(sum level1 level7 level8)
(sum level1 level8 level9)
(sum level1 level9 level10)
(sum level1 level10 level11)
(sum level1 level11 level12)
(sum level1 level12 level13)
(sum level2 level0 level2)
(sum level2 level1 level3)
(sum level2 level2 level4)
(sum level2 level3 level5)
(sum level2 level4 level6)
(sum level2 level5 level7)
(sum level2 level6 level8)
(sum level2 level7 level9)
(sum level2 level8 level10)
(sum level2 level9 level11)
(sum level2 level10 level12)
(sum level2 level11 level13)
(sum level3 level0 level3)
(sum level3 level1 level4)
(sum level3 level2 level5)
(sum level3 level3 level6)
(sum level3 level4 level7)
(sum level3 level5 level8)
(sum level3 level6 level9)
(sum level3 level7 level10)
(sum level3 level8 level11)
(sum level3 level9 level12)
(sum level3 level10 level13)
(sum level4 level0 level4)
(sum level4 level1 level5)
(sum level4 level2 level6)
(sum level4 level3 level7)
(sum level4 level4 level8)
(sum level4 level5 level9)
(sum level4 level6 level10)
(sum level4 level7 level11)
(sum level4 level8 level12)
(sum level4 level9 level13)
(sum level5 level0 level5)
(sum level5 level1 level6)
(sum level5 level2 level7)
(sum level5 level3 level8)
(sum level5 level4 level9)
(sum level5 level5 level10)
(sum level5 level6 level11)
(sum level5 level7 level12)
(sum level5 level8 level13)
(sum level6 level0 level6)
(sum level6 level1 level7)
(sum level6 level2 level8)
(sum level6 level3 level9)
(sum level6 level4 level10)
(sum level6 level5 level11)
(sum level6 level6 level12)
(sum level6 level7 level13)
(sum level7 level0 level7)
(sum level7 level1 level8)
(sum level7 level2 level9)
(sum level7 level3 level10)
(sum level7 level4 level11)
(sum level7 level5 level12)
(sum level7 level6 level13)
(sum level8 level0 level8)
(sum level8 level1 level9)
(sum level8 level2 level10)
(sum level8 level3 level11)
(sum level8 level4 level12)
(sum level8 level5 level13)
(sum level9 level0 level9)
(sum level9 level1 level10)
(sum level9 level2 level11)
(sum level9 level3 level12)
(sum level9 level4 level13)
(sum level10 level0 level10)
(sum level10 level1 level11)
(sum level10 level2 level12)
(sum level10 level3 level13)
(sum level11 level0 level11)
(sum level11 level1 level12)
(sum level11 level2 level13)
(sum level12 level0 level12)
(sum level12 level1 level13)
(sum level13 level0 level13)

(total-capacity level11)
(total-cost level13)
(waiting o0)
(waiting o1)
(waiting o2)
(waiting o3)
(waiting o4)
(waiting o5)
(waiting o6)
(waiting o7)
(waiting o8)
(waiting o9)
(waiting o10)

(not-made p0)
(not-made p1)
(not-made p2)
(not-made p3)
(not-made p4)
(not-made p5)
(not-made p6)
(not-made p7)
(not-made p8)
(not-made p9)
(not-made p10)

(includes o0 p8)
(includes o0 p4)
(includes o0 p9)
(includes o0 p2)
(includes o1 p0)
(includes o1 p7)
(includes o1 p6)
(includes o1 p8)
(includes o2 p2)
(includes o2 p8)
(includes o2 p10)
(includes o2 p7)
(includes o3 p3)
(includes o3 p8)
(includes o3 p7)
(includes o3 p10)
(includes o4 p7)
(includes o4 p8)
(includes o4 p3)
(includes o4 p1)
(includes o5 p1)
(includes o5 p8)
(includes o5 p6)
(includes o5 p3)
(includes o6 p0)
(includes o6 p6)
(includes o6 p8)
(includes o6 p3)
(includes o7 p4)
(includes o7 p3)
(includes o7 p0)
(includes o7 p7)
(includes o8 p10)
(includes o8 p7)
(includes o8 p1)
(includes o8 p9)
(includes o9 p7)
(includes o9 p9)
(includes o9 p8)
(includes o9 p2)
(includes o10 p2)
(includes o10 p3)
(includes o10 p1)
(includes o10 p7)

)

(:goal (and
(shipped o0)
(shipped o1)
(shipped o7)
))

)

