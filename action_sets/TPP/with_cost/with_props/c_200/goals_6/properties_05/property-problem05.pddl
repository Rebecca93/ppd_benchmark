set load_g2_m3 1
load goods2 truck market3


set load_g2_m5 1
load goods2 truck market5


soft-property load_g2_m3_not_m5
&& load_g2_m3 ! load_g2_m5


set drive_t0_market4_depot0 2
drive truck0 market4 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market4 moneylevel moneylevel moneylevel


soft-property use_t0_market4_depot0
drive_t0_market4_depot0



set load_g5_m5 1
load goods5 truck market5


set load_g5_m2 1
load goods5 truck market2


soft-property load_g5_m5_not_m2
&& load_g5_m5 ! load_g5_m2


set drive_t0_market3_market2 2
drive truck0 market3 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market3 moneylevel moneylevel moneylevel


soft-property use_not_t0_market3_market2
! drive_t0_market3_market2



set drive_t0_market2_market4 2
drive truck0 market2 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_market4
! drive_t0_market2_market4



