(define (problem blocksworld-p08_6)
(:domain blocksworld)
(:objects
a b c d e f g - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear g)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
)
(:goal (and
(on f d)
(on b f)
(on g b)
(on a g)
(on c a)
(on e c)
))
(:metric minimize (total-cost))
)
