soft-LTL_property use_t1_l5_l0
|| <> && at(t1,l5) X at(t1,l0) <> && at(t1,l0)  X at(t1,l5)


soft-LTL_property same_truck_p3p5
|| [] && ! in(p3,t1) ! in(p5,t1)  [] && ! in(p3,t0) ! in(p5,t0)


soft-LTL_property same_truck_p0p6
|| [] && ! in(p0,t1) ! in(p6,t1)  [] && ! in(p0,t0) ! in(p6,t0)


soft-LTL_property same_truck_p0p5
|| [] && ! in(p0,t1) ! in(p5,t1)  [] && ! in(p0,t0) ! in(p5,t0)


soft-LTL_property use_t0_l2_l4
|| <> && at(t0,l2) X at(t0,l4) <> && at(t0,l4)  X at(t0,l2)


