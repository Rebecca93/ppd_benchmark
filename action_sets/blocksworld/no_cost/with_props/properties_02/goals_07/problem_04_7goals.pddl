(define (problem blocksworld-p04_7)
(:domain blocksworld)
(:objects
a b c d e f g h - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear h)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
(on h g)
)
(:goal (and
(on a e)
(on f a)
(on d f)
(on h d)
(on c h)
(on g c)
(on b g)
))
(:metric minimize (total-cost))
)
