import sys
import math

# read pfile
file = open(sys.argv[1], "r") 
for line in file:
    if "metric" in line:
        print
    elif "total-cost" in line:
        energy = line[16:len(line)-2]
        print "(= (total-cost) "+str(int(math.ceil(float(energy)*float(sys.argv[2]))))+")"
    else:
        print line,
