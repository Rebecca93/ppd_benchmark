(define (domain satellite)
(:requirements :strips :equality :typing)
(:types satellite direction instrument mode num)
(:predicates 
    (on_board ?i - instrument ?s - satellite)
    (supports ?i - instrument ?m - mode)
    (pointing ?s - satellite ?d - direction)
    (power_avail ?s - satellite)
    (power_secondary ?s - satellite)
    (power_on ?i - instrument)
    (calibrated ?i - instrument)
    (have_image ?d - direction ?m - mode)
    (calibration_target ?i - instrument ?d - direction)

    (time_cost ?n - num)
    (slew_time ?n - num ?d1 ?d2 - direction)
    (sum ?a ?b ?c - num)
)

(:constants
    level0 level1 - num
)

(:action turn_to
    :parameters (?s - satellite ?d_new - direction ?d_prev - direction ?tpost ?tdelta ?tpre - num)
    :precondition (and
        (pointing ?s ?d_prev)
        (slew_time ?tdelta ?d_prev ?d_new)
        (time_cost ?tpre)
        (sum ?tpost ?tdelta ?tpre)
    )
    :effect (and
        (pointing ?s ?d_new)
        (not (pointing ?s ?d_prev))
        (not (time_cost ?tpre))
        (time_cost ?tpost)
    )
)

(:action switch_on
    :parameters (?i - instrument ?s - satellite)
    :precondition (and
        (on_board ?i ?s) 
        (power_avail ?s)
    )
    :effect (and
        (power_on ?i)
        (not (calibrated ?i))
        (not (power_avail ?s))
    )
)

(:action switch_on_secondary
    :parameters (?i - instrument ?s - satellite)
    :precondition (and
        (on_board ?i ?s) 
        (power_secondary ?s)
    )
    :effect (and
        (power_on ?i)
        (not (calibrated ?i))
        (not (power_secondary ?s))
    )
)

(:action switch_off
    :parameters (?i - instrument ?s - satellite)
    :precondition (and
        (on_board ?i ?s)
        (power_on ?i) 
    )
    :effect (and
        (not (power_on ?i))
        (power_avail ?s)
    )
)

(:action switch_off_secondary
    :parameters (?i - instrument ?s - satellite)
    :precondition (and
        (on_board ?i ?s)
        (power_on ?i) 
    )
    :effect (and
        (not (power_on ?i))
        (power_secondary ?s)
    )
)

(:action calibrate
    :parameters (?s - satellite ?i - instrument ?d - direction ?tpost ?tpre - num)
    :precondition (and
        (on_board ?i ?s)
        (calibration_target ?i ?d)
        (pointing ?s ?d)
        (power_on ?i)
        (time_cost ?tpre)
        (sum ?tpost level1 ?tpre)
    )
    :effect (and
        (calibrated ?i)
        (not (time_cost ?tpre))
        (time_cost ?tpost)
    )
)

(:action take_image
    :parameters (?s - satellite ?d - direction ?i - instrument ?m - mode)
    :precondition (and
        (calibrated ?i)
        (on_board ?i ?s)
        (supports ?i ?m)
        (power_on ?i)
        (pointing ?s ?d)
        (power_on ?i)
    )
    :effect (have_image ?d ?m)
)
)
