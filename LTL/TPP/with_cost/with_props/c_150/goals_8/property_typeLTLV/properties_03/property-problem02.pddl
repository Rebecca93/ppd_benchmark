soft-LTL_property use_t0_market4_market1
|| <> && at(truck0,market4) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market4)


soft-LTL_property use_not_t0_market2_depot0
&& [] || ! at(truck0,market2) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market2)


soft-LTL_property load_g6_m5_not_m1
<> && && available(goods6,market5) ! available(goods6,market1) X loaded(goods6,truck0)


