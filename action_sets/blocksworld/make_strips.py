import sys
import re
from itertools import product

if len(sys.argv) < 2:
    print "usage: python make_strips.py problem"
    print "This program converts a rovers problem instance to strips-style numerics."
    quit()

# read problem
maxenergy = 11
problem = open(sys.argv[1], "r") 
for line in problem:
    if "(= (total-cost) " in line:
        energy = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        if(int(energy[0]) > maxenergy):
            maxenergy = int(energy[0])
problem.close()

# read problem
problem = open(sys.argv[1], "r") 
for line in problem:

    if  " - hand" in line:
        print line,
        for i in range(11,maxenergy+1):
            print "level"+str(i),
        print "- energylevel"
    elif "(= (total-cost) " in line:
        energy = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        print "(energy level" + energy[0] + ")"
        print
        for i,j in product(range(0,maxenergy+1), range(0,maxenergy+1)):
            if (i+j <= maxenergy):
                print "(sum level" + str(i) + " level" + str(j) + " level" + str(i+j) + ")"
    elif "(= (ecost " in line:
        digits = [s for s in re.split('\)| |h',line) if s.isdigit()]
        print "(ecost level" + digits[1] + " h" + digits[0] + ")"
    elif "metric" in line:
        print
    else:
        print line,
problem.close()
