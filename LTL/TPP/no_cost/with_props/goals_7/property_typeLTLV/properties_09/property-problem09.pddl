soft-LTL_property use_t0_market5_market1
|| <> && at(truck0,market5) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market5)


soft-LTL_property use_not_t0_market2_market1
&& [] || ! at(truck0,market2) ! X at(truck0,market1) [] || ! at(truck0,market1) ! X at(truck0,market2)


soft-LTL_property load_g3_m4_not_m1
<> && && available(goods3,market4) ! available(goods3,market1) X loaded(goods3,truck0)


soft-LTL_property use_not_t0_market3_market5
&& [] || ! at(truck0,market3) ! X at(truck0,market5) [] || ! at(truck0,market5) ! X at(truck0,market3)


soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property use_t0_depot0_market2
|| <> && at(truck0,depot0) X at(truck0,market2) <> && at(truck0,market2)  X at(truck0,depot0)


soft-LTL_property load_g4_m3_not_m1
<> && && available(goods4,market3) ! available(goods4,market1) X loaded(goods4,truck0)


soft-LTL_property use_t0_market2_market5
|| <> && at(truck0,market2) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market2)


soft-LTL_property use_not_t0_market1_depot0
&& [] || ! at(truck0,market1) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market1)


