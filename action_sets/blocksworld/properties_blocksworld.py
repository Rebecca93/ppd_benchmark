import sys
import random
import string
from random import shuffle

if len(sys.argv) < 3:
    print "usage: python properties_blocksworld.py problem max_properties"
    print "This program generates the property file for a blocksworld problem instance."
    quit()

max_properties = int(sys.argv[2])

numblocks = 0
hands = []

# read pfile and count orders and stacks
file = open(sys.argv[1], "r") 
for line in file:
    if "(ecost" in line:
        hands.append("h"+str(len(hands)))
    if "(on " in line:
        numblocks = numblocks + 1
numblocks = numblocks/2 + 1
blocks = list(string.ascii_lowercase)[0:numblocks]
shuffle(blocks)

# declare action sets
asets = [(x, y) for x in hands for y in blocks]
bsets = []
for i in blocks:
    for j in blocks:
        if i!=j:
            bsets.append((i,j))
shuffle(asets)
shuffle(bsets)

for i in range(0,min(max_properties,numblocks*2)):
    print "set use_hand"+str(asets[i][0])+"_block_"+str(asets[i][1])+" 2"
    print "pick-up "+str(asets[i][1])+" "+str(asets[i][0]) + " energylevel energylevel energylevel"
    print "unstack "+str(asets[i][1])+" block "+str(asets[i][0]) + " energylevel energylevel energylevel"
    print ""

for i in range(0,min(max_properties-numblocks*2,numblocks*numblocks)):
    print "set puton_block"+str(bsets[i][0])+"_block"+str(bsets[i][1])+" 2"
    print "stack "+str(bsets[i][0])+" "+str(bsets[i][1]) + " hand energylevel energylevel energylevel"
    print ""    

# declare properties
for i in range(0,min(max_properties,numblocks*2)):
    print "soft-property use_hand"+str(asets[i][0])+"_block_"+str(asets[i][1])
    print "use_hand"+str(asets[i][0])+"_block_"+str(asets[i][1])
    print ""

for i in range(0,min(max_properties-numblocks*2,numblocks*numblocks)):
    print "soft-property wason_block"+str(bsets[i][0])+"_block"+str(bsets[i][1])
    print "puton_block"+str(bsets[i][0])+"_block"+str(bsets[i][1])
    print ""
