(define (problem os-time-p10_11)
(:domain openstacks-time-nonADL-nonNegated)
(:init

(= (total-capacity) 11)
(= (total-cost) 0)
(waiting o0)
(waiting o1)
(waiting o2)
(waiting o3)
(waiting o4)
(waiting o5)
(waiting o6)
(waiting o7)
(waiting o8)
(waiting o9)
(waiting o10)

(not-made p0)
(not-made p1)
(not-made p2)
(not-made p3)
(not-made p4)
(not-made p5)
(not-made p6)
(not-made p7)
(not-made p8)
(not-made p9)
(not-made p10)

(includes o0 p5)
(includes o0 p1)
(includes o0 p8)
(includes o0 p6)
(includes o1 p3)
(includes o1 p10)
(includes o1 p0)
(includes o1 p9)
(includes o2 p4)
(includes o2 p5)
(includes o2 p9)
(includes o2 p1)
(includes o3 p8)
(includes o3 p7)
(includes o3 p1)
(includes o3 p4)
(includes o4 p9)
(includes o4 p7)
(includes o4 p6)
(includes o4 p0)
(includes o5 p0)
(includes o5 p4)
(includes o5 p7)
(includes o5 p10)
(includes o6 p2)
(includes o6 p3)
(includes o6 p4)
(includes o6 p5)
(includes o7 p4)
(includes o7 p6)
(includes o7 p10)
(includes o7 p7)
(includes o8 p3)
(includes o8 p2)
(includes o8 p1)
(includes o8 p4)
(includes o9 p9)
(includes o9 p0)
(includes o9 p5)
(includes o9 p8)
(includes o10 p4)
(includes o10 p0)
(includes o10 p10)
(includes o10 p1)

)

(:goal (and
(shipped o3)
(shipped o4)
(shipped o2)
(shipped o0)
(shipped o8)
))
(:metric minimize (total-cost))
)

