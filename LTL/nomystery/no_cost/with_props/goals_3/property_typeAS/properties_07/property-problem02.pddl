set drive_t1_l4_l0 2
drive t1 l4 l0
drive t1 l0 l4

soft-AS_property use_t1_l4_l0
drive_t1_l4_l0


set delwithT0_p1p2 4
unload p1 t0 location
load p1 t0 location
unload p2 t0 location
load p2 t0 location

set delwithT1_p1p2 4
unload p1 t1 location
load p1 t1 location
unload p2 t1 location
load p2 t1 location

soft-AS_property same_truck_p1p2
|| && delwithT0_p1p2 ! delwithT1_p1p2 && ! delwithT0_p1p2 delwithT1_p1p2


set delwithT0_p0p2 4
unload p0 t0 location
load p0 t0 location
unload p2 t0 location
load p2 t0 location

set delwithT1_p0p2 4
unload p0 t1 location
load p0 t1 location
unload p2 t1 location
load p2 t1 location

soft-AS_property same_truck_p0p2
|| && delwithT0_p0p2 ! delwithT1_p0p2 && ! delwithT0_p0p2 delwithT1_p0p2


set drive_t0_l1_l0 2
drive t0 l1 l0
drive t0 l0 l1

soft-AS_property use_t0_l1_l0
drive_t0_l1_l0


set delwithT0_p0p1 4
unload p0 t0 location
load p0 t0 location
unload p1 t0 location
load p1 t0 location

set delwithT1_p0p1 4
unload p0 t1 location
load p0 t1 location
unload p1 t1 location
load p1 t1 location

soft-AS_property same_truck_p0p1
|| && delwithT0_p0p1 ! delwithT1_p0p1 && ! delwithT0_p0p1 delwithT1_p0p1


set drive_t0_l1_l5 2
drive t0 l1 l5
drive t0 l5 l1

soft-AS_property use_t0_l1_l5
drive_t0_l1_l5


set drive_t0_l5_l2 2
drive t0 l5 l2
drive t0 l2 l5

soft-AS_property use_t0_l5_l2
drive_t0_l5_l2


