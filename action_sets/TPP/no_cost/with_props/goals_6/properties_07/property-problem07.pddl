set drive_t0_market4_market1 2
drive truck0 market4 market1
drive truck0 market1 market4

soft-property use_t0_market4_market1
drive_t0_market4_market1



set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set load_g0_m5 1
load goods0 truck market5


set load_g0_m1 1
load goods0 truck market1


soft-property load_g0_m5_not_m1
&& load_g0_m5 ! load_g0_m1


set drive_t0_market5_depot0 2
drive truck0 market5 depot0
drive truck0 depot0 market5

soft-property use_not_t0_market5_depot0
! drive_t0_market5_depot0



set drive_t0_market2_market5 2
drive truck0 market2 market5
drive truck0 market5 market2

soft-property use_t0_market2_market5
drive_t0_market2_market5



set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-property use_t0_market1_market3
drive_t0_market1_market3



