soft-LTL_property use_t1_l4_l0
|| <> && at(t1,l4) X at(t1,l0) <> && at(t1,l0)  X at(t1,l4)


soft-LTL_property same_truck_p5p6
|| [] && ! in(p5,t1) ! in(p6,t1)  [] && ! in(p5,t0) ! in(p6,t0)


soft-LTL_property use_t0_l1_l2
|| <> && at(t0,l1) X at(t0,l2) <> && at(t0,l2)  X at(t0,l1)


soft-LTL_property use_t1_l5_l1
|| <> && at(t1,l5) X at(t1,l1) <> && at(t1,l1)  X at(t1,l5)


soft-LTL_property dont_use_t1_l0_l1
&& [] || ! at(t1,l0) ! X at(t1,l1) [] || ! at(t1,l1) ! X at(t1,l0)


soft-LTL_property same_truck_p3p5
|| [] && ! in(p3,t1) ! in(p5,t1)  [] && ! in(p3,t0) ! in(p5,t0)


soft-LTL_property use_t0_l2_l0
|| <> && at(t0,l2) X at(t0,l0) <> && at(t0,l0)  X at(t0,l2)


