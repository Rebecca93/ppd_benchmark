set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-AS_property use_not_t0_market1_market5
! drive_t0_market1_market5



set load_g1_m2 1
load goods1 truck market2


set load_g1_m1 1
load goods1 truck market1


soft-AS_property load_g1_m2_not_m1
&& load_g1_m2 ! load_g1_m1


