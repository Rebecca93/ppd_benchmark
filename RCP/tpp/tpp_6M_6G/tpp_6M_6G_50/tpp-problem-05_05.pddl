(define (problem tpp-problem-m6-g6-c0.500000-s1004)
(:domain TPPLike-Metric)
(:objects
	market1 market2 market3 market4 market5 market6 - market
	depot0 - depot
	truck0 - truck
	goods0 goods1 goods2 goods3 goods4 goods5 - goods
    level5 level4 level3 level2 level1 level0 - moneylevel
)
(:init
(sum level0 level0 level0)
(sum level0 level1 level1)
(sum level0 level2 level2)
(sum level0 level3 level3)
(sum level0 level4 level4)
(sum level0 level5 level5)
(sum level1 level0 level1)
(sum level1 level1 level2)
(sum level1 level2 level3)
(sum level1 level3 level4)
(sum level1 level4 level5)
(sum level2 level0 level2)
(sum level2 level1 level3)
(sum level2 level2 level4)
(sum level2 level3 level5)
(sum level3 level0 level3)
(sum level3 level1 level4)
(sum level3 level2 level5)
(sum level4 level0 level4)
(sum level4 level1 level5)
(sum level5 level0 level5)
	(price goods0 market3 level1)
	(on-sale goods0 market3)
	(price goods0 market4 level1)
	(on-sale goods0 market4)
	(price goods1 market2 level1)
	(on-sale goods1 market2)
	(price goods1 market6 level1)
	(on-sale goods1 market6)
	(price goods2 market1 level1)
	(on-sale goods2 market1)
	(price goods2 market2 level1)
	(on-sale goods2 market2)
	(price goods3 market4 level1)
	(on-sale goods3 market4)
	(price goods3 market5 level1)
	(on-sale goods3 market5)
	(price goods4 market3 level1)
	(on-sale goods4 market3)
	(price goods4 market6 level1)
	(on-sale goods4 market6)
	(price goods5 market2 level1)
	(on-sale goods5 market2)
	(price goods5 market5 level1)
	(on-sale goods5 market5)
	(drive-cost depot0 market5 level1)
	(connected depot0 market5)
	(drive-cost depot0 market6 level1)
	(connected depot0 market6)
	(drive-cost market1 market3 level1)
	(connected market1 market3)
	(drive-cost market1 market5 level1)
	(connected market1 market5)
	(drive-cost market2 market6 level1)
	(connected market2 market6)
	(drive-cost market3 market1 level1)
	(connected market3 market1)
	(drive-cost market3 market4 level1)
	(connected market3 market4)
	(drive-cost market3 market6 level1)
	(connected market3 market6)
	(drive-cost market4 market3 level1)
	(connected market4 market3)
	(drive-cost market5 depot0 level1)
	(connected market5 depot0)
	(drive-cost market5 market1 level1)
	(connected market5 market1)
	(drive-cost market6 depot0 level1)
	(connected market6 depot0)
	(drive-cost market6 market2 level1)
	(connected market6 market2)
	(drive-cost market6 market3 level1)
	(connected market6 market3)
	(at truck0 depot0)
(money level5)
)
(:goal (and
	(stored goods0)
	(stored goods1)
	(stored goods2)
	(stored goods3)
	(stored goods4)
	(stored goods5)
))
)
