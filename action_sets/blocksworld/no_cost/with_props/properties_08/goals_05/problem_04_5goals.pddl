(define (problem blocksworld-p04_5)
(:domain blocksworld)
(:objects
a b c d e f - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear f)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
)
(:goal (and
(on e c)
(on f e)
(on d f)
(on b d)
(on a b)
))
(:metric minimize (total-cost))
)
