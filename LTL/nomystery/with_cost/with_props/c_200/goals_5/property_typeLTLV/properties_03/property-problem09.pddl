soft-LTL_property use_t1_l4_l3
|| <> && at(t1,l4) X at(t1,l3) <> && at(t1,l3)  X at(t1,l4)


soft-LTL_property same_truck_p2p3
|| [] && ! in(p2,t1) ! in(p3,t1)  [] && ! in(p2,t0) ! in(p3,t0)


soft-LTL_property use_t1_l1_l2
|| <> && at(t1,l1) X at(t1,l2) <> && at(t1,l2)  X at(t1,l1)


