def twoDigit(i):
    assert(i < 100)
    if i > 0 and i < 10:
        return "0" + str(i)
    else:
        return str(i)

def cost_scaled_instances(base, scale, g_lower, g_upper, p_lower, p_upper):

    goals = []
    for g in range(g_lower, g_upper+1):
        goals.append(str(g))
    properties = []
    for p in range(p_lower, p_upper+1):
        properties.append(twoDigit(p))

    path = []
    for g in goals:
        for p in properties:
            path.append(base + "/c_" + scale + "/goals_" + g + "/properties_" + p + "/")

    return path


def property_instances(base, g_lower, g_upper, p_lower, p_upper):

    goals = []
    for g in range(g_lower, g_upper+1):
        if g >=10:
            goals.append(str(g))
        else:
            goals.append("0" + str(g))

    properties = []
    for p in range(p_lower, p_upper+1):
        properties.append(twoDigit(p))

    path = []
    for g in goals:
        for p in properties:
            path.append(base + "/goals_" + g + "/properties_" + p + "/")

    return path

def no_property_instances(base, g_lower, g_upper):
    print(base)
    print(g_lower)
    print(g_upper)
    goals = []
    for g in range(g_lower, g_upper+1):
        if g >=10:
            goals.append(str(g))
        else:
            goals.append("0" + str(g))

    path = []
    for g in goals:
            path.append(base + "/goals_" + g + "/")
   
    for p in path:
        print(p)
    return path




def cost_scaled_nomystery(scale):
    return cost_scaled_instances("nomystery_two_trucks/with_cost/with_props/", scale, 4, 6, 1, 10)

def nomystery_no_cost_with_props():
    return property_instances("nomystery_two_trucks/no_cost/with_props/", 4, 7, 1, 10)

def cost_scaled_tpp(scale):
    return cost_scaled_instances("TPP/with_cost/with_props/", scale, 4, 6, 1, 10)

def TPP_no_cost_with_props():
    return property_instances("TPP/no_cost/with_props/", 4, 7, 1, 10)

def rovers_no_cost_with_props():
    return property_instances("rovers/no_cost/with_props/", 5, 9, 1, 10)

def rovers_no_cost_no_props():
    return no_property_instances("rovers/no_cost/no_props/", 5, 9)

def openstacks_no_cost_with_props():
    return property_instances("openstacks-strips/no_cost/with_props/", 1, 10, 1, 10)

def openstacks_no_cost_no_props():
    return no_property_instances("openstacks-strips/no_cost/no_props/", 1, 10)

def blocksworld_no_cost_with_props():
    return property_instances("blocksworld/no_cost/with_props/", 1, 10, 1, 9)

def blocksworld_no_cost_no_props():
    return no_property_instances("blocksworld/no_cost/no_props/", 1, 9)

def satellite_no_cost_with_props():
    return property_instances("satellite/no_cost/with_props/", 1, 10, 1, 9)

def satellite_no_cost_no_props():
    return no_property_instances("blocksworld/no_cost/no_props/", 1, 9)
