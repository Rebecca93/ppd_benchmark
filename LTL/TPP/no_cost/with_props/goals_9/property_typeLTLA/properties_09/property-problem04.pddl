state_set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-LTL_property use_t0_market4_market5
<> drive_t0_market4_market5



state_set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-LTL_property use_not_t0_market1_market5
[] ! drive_t0_market1_market5



state_set load_g6_m3 1
load goods6 truck market3


state_set load_g6_m1 1
load goods6 truck market1


soft-LTL_property load_g6_m3_not_m1
&& <> load_g6_m3 [] ! load_g6_m1


state_set drive_t0_market2_market4 2
drive truck0 market2 market4
drive truck0 market4 market2

soft-LTL_property use_not_t0_market2_market4
[] ! drive_t0_market2_market4



state_set drive_t0_market2_market5 2
drive truck0 market2 market5
drive truck0 market5 market2

soft-LTL_property use_t0_market2_market5
<> drive_t0_market2_market5



state_set drive_t0_depot0_market2 2
drive truck0 depot0 market2
drive truck0 market2 depot0

soft-LTL_property use_t0_depot0_market2
<> drive_t0_depot0_market2



state_set load_g8_m2 1
load goods8 truck market2


state_set load_g8_m1 1
load goods8 truck market1


soft-LTL_property load_g8_m2_not_m1
&& <> load_g8_m2 [] ! load_g8_m1


state_set drive_t0_market1_market2 2
drive truck0 market1 market2
drive truck0 market2 market1

soft-LTL_property use_t0_market1_market2
<> drive_t0_market1_market2



state_set drive_t0_depot0_market4 2
drive truck0 depot0 market4
drive truck0 market4 depot0

soft-LTL_property use_not_t0_depot0_market4
[] ! drive_t0_depot0_market4



