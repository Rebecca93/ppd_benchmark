soft-LTL_property use_t1_l4_l2
|| <> && at(t1,l4) X at(t1,l2) <> && at(t1,l2)  X at(t1,l4)


soft-LTL_property same_truck_p3p5
|| [] && ! in(p3,t1) ! in(p5,t1)  [] && ! in(p3,t0) ! in(p5,t0)


soft-LTL_property same_truck_p0p6
|| [] && ! in(p0,t1) ! in(p6,t1)  [] && ! in(p0,t0) ! in(p6,t0)


soft-LTL_property same_truck_p0p5
|| [] && ! in(p0,t1) ! in(p5,t1)  [] && ! in(p0,t0) ! in(p5,t0)


soft-LTL_property use_t1_l2_l0
|| <> && at(t1,l2) X at(t1,l0) <> && at(t1,l0)  X at(t1,l2)


soft-LTL_property use_t0_l1_l5
|| <> && at(t0,l1) X at(t0,l5) <> && at(t0,l5)  X at(t0,l1)


soft-LTL_property use_t0_l5_l3
|| <> && at(t0,l5) X at(t0,l3) <> && at(t0,l3)  X at(t0,l5)


soft-LTL_property same_truck_p3p6
|| [] && ! in(p3,t1) ! in(p6,t1)  [] && ! in(p3,t0) ! in(p6,t0)


