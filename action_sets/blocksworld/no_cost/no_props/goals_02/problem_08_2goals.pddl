(define (problem blocksworld-p08_2)
(:domain blocksworld)
(:objects
a b c - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear c)
(on b a)
(on c b)
)
(:goal (and
(on c b)
(on a c)
))
(:metric minimize (total-cost))
)
