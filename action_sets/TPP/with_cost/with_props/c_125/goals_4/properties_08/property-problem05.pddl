set load_g2_m3 1
load goods2 truck market3


set load_g2_m5 1
load goods2 truck market5


soft-property load_g2_m3_not_m5
&& load_g2_m3 ! load_g2_m5


set drive_t0_market4_depot0 2
drive truck0 market4 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market4 moneylevel moneylevel moneylevel


soft-property use_t0_market4_depot0
drive_t0_market4_depot0



set load_g3_m4 1
load goods3 truck market4


set load_g3_m1 1
load goods3 truck market1


soft-property load_g3_m4_not_m1
&& load_g3_m4 ! load_g3_m1


set drive_t0_market3_market2 2
drive truck0 market3 market2 moneylevel moneylevel moneylevel
drive truck0 market2 market3 moneylevel moneylevel moneylevel


soft-property use_not_t0_market3_market2
! drive_t0_market3_market2



set drive_t0_market2_market4 2
drive truck0 market2 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_market4
! drive_t0_market2_market4



set drive_t0_market1_market5 2
drive truck0 market1 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market1 moneylevel moneylevel moneylevel


soft-property use_t0_market1_market5
drive_t0_market1_market5



set load_g1_m5 1
load goods1 truck market5


set load_g1_m4 1
load goods1 truck market4


soft-property load_g1_m5_not_m4
&& load_g1_m5 ! load_g1_m4


set drive_t0_market4_market5 2
drive truck0 market4 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market4 moneylevel moneylevel moneylevel


soft-property use_t0_market4_market5
drive_t0_market4_market5



