import sys
import random
from random import shuffle

if len(sys.argv) < 3:
    print "usage: python properties_rovers.py problem max_properties"
    print "This program generates the property file for a rovers problem instance."
    quit()

max_properties = int(sys.argv[2])

size = 0
objectives = {}
rocks = []

# read pfile
file = open(sys.argv[1], "r") 
for line in file:
    # count waypoints
    if "Waypoint" in line:
        size = len(line.split())-2
    # remember objectives
    if "visible_from" in line:
        obj = line.split()[1]
        if not obj in objectives:
            objectives[obj] = []
        wp = line.split()[2]
        wp = wp[0:len(wp)-1]
        objectives[obj].append(wp)
    # remember rock samples
    if "at_rock_sample" in line:
        wp = line.split()[1]
        wp = wp[0:len(wp)-1]
        rocks.append(wp)

property_name = "property-" + sys.argv[1].rsplit("/",1)[1]
pro = open(property_name,"w+")

# properties
props = []
cameras = ["camera00","camera01","camera02","camera10","camera11","camera12"]
rovers = ["rover0", "rover1"]

cam_obj = [(x, y) for x in cameras for y in objectives]
rov_rock = [(x, y) for x in rovers for y in rocks]
rov_obj = [(x,y,z) for x in rovers for y in objectives.keys() for z in objectives[y]]

allprops = cam_obj + rov_rock + rov_obj
shuffle(allprops)

if len(allprops) < 10:
  print "WARNING: THE PROBLEM DOES NOT SUPPORT 10 PROPERTIES (" + str(len(allprops)) + "), " + sys.argv[1]

for i in range(max_properties):

    prop_type = allprops[i]

    # preferred rover (rock sample)
    if prop_type[0].startswith("rover") and len(prop_type)==2:
        pro.write("set samplerock_"+prop_type[0]+"_"+prop_type[1]+" 1\n")
        pro.write("sample_rock "+prop_type[0]+" store "+prop_type[1]+"\n\n")
        props.append("soft-property preferred_sample_"+prop_type[0]+"_"+prop_type[1]+"\n")
        props.append("samplerock_"+prop_type[0]+"_"+prop_type[1]+"\n\n")

    # preferred rover (objective)
    elif prop_type[0].startswith("rover") and len(prop_type)==3:
        pro.write("set take_image_"+prop_type[0]+"_"+prop_type[2]+"_"+prop_type[1]+" 1\n")
        pro.write("take_image "+prop_type[0]+" "+prop_type[2]+" "+prop_type[1]+" camera mode\n\n")
        props.append("soft-property preferred_take_image_"+prop_type[0]+"_"+prop_type[2]+"_"+prop_type[1]+"\n")
        props.append("take_image_"+prop_type[0]+"_"+prop_type[2]+"_"+prop_type[1]+"\n\n")

    # preferred camera (objective)
    elif prop_type[0].startswith("camera"):
        pro.write("set take_image_"+prop_type[0]+"_"+prop_type[1]+" 1\n")
        pro.write("take_image rover waypoint "+prop_type[1]+" "+prop_type[0]+" mode\n\n")
        props.append("soft-property preferred_take_image_"+prop_type[0]+"_"+prop_type[1]+"\n")
        props.append("take_image_"+prop_type[0]+"_"+prop_type[1]+"\n\n")

for prop in props:
    pro.write(prop)

pro.close()
