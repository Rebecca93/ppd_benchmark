set load_g3_m2 1
load goods3 truck market2


set load_g3_m3 1
load goods3 truck market3


soft-property load_g3_m2_not_m3
&& load_g3_m2 ! load_g3_m3


set load_g0_m4 1
load goods0 truck market4


set load_g0_m5 1
load goods0 truck market5


soft-property load_g0_m4_not_m5
&& load_g0_m4 ! load_g0_m5


set load_g2_m4 1
load goods2 truck market4


set load_g2_m1 1
load goods2 truck market1


soft-property load_g2_m4_not_m1
&& load_g2_m4 ! load_g2_m1


set drive_t0_market1_market2 2
drive truck0 market1 market2
drive truck0 market2 market1

soft-property use_not_t0_market1_market2
! drive_t0_market1_market2



set drive_t0_depot0_market4 2
drive truck0 depot0 market4
drive truck0 market4 depot0

soft-property use_not_t0_depot0_market4
! drive_t0_depot0_market4



set drive_t0_market1_market4 2
drive truck0 market1 market4
drive truck0 market4 market1

soft-property use_t0_market1_market4
drive_t0_market1_market4



