(define (problem blocksworld-p06_9)
(:domain blocksworld)
(:objects
a b c d e f g h i j - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear j)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
(on h g)
(on i h)
(on j i)
)
(:goal (and
(on d j)
(on e d)
(on b e)
(on i b)
(on c i)
(on h c)
(on a h)
(on g a)
(on f g)
))
(:metric minimize (total-cost))
)
