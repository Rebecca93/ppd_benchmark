import sys
import random
from random import shuffle
import string

if len(sys.argv) < 4:
    print "usage: python generate_blocksworld.py name_prefix spaces blocks"
    print "This program generates the domain and problem for blocksworld."
    quit()

spaces = int(sys.argv[2])
blocks = int(sys.argv[3])

problem_name = "problem_" + sys.argv[1] + "_" + str(blocks) + "goals.pddl"

blocklist = list(string.ascii_lowercase)[0:blocks+1]

#################
# write problem #
#################

pro = open(problem_name,"w+")

pro.write("(define (problem blocksworld-p"+sys.argv[1]+"_"+str(blocks)+")\n")
pro.write("(:domain blocksworld)\n")

# blocks
pro.write("(:objects\n")
for i in blocklist:
    pro.write(i+" ")
pro.write("- block\n")
pro.write("h0 h1 - hand\n")
pro.write(")\n")

pro.write("(:init\n")
pro.write("\n")

pro.write("(handempty h0)\n")
pro.write("(handempty h1)\n")

pro.write("(= (ecost h0) 1)\n")
pro.write("(= (ecost h1) 2)\n")

pro.write("(= (total-cost) 0)\n")

# blocks
pro.write("(ontable "+blocklist[0]+")\n")
pro.write("(clear "+blocklist[len(blocklist)-1]+")\n")
for i in range(1,blocks+1):
    pro.write("(on "+blocklist[i]+" "+blocklist[i-1]+")\n")
pro.write(")\n")

shuffle(blocklist)

# goal
pro.write("(:goal (and\n")
for i in range(0,blocks):
    pro.write("(on "+blocklist[i+1]+" "+blocklist[i]+")\n")
pro.write("))\n")

pro.write("(:metric minimize (total-cost))\n")

pro.write(")\n")

pro.close()
