set drive_t0_market4_market1 2
drive truck0 market4 market1
drive truck0 market1 market4

soft-AS_property use_t0_market4_market1
drive_t0_market4_market1



set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-AS_property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set load_g6_m5 1
load goods6 truck market5


set load_g6_m1 1
load goods6 truck market1


soft-AS_property load_g6_m5_not_m1
&& load_g6_m5 ! load_g6_m1


set drive_t0_market3_market1 2
drive truck0 market3 market1
drive truck0 market1 market3

soft-AS_property use_not_t0_market3_market1
! drive_t0_market3_market1



set drive_t0_market3_market5 2
drive truck0 market3 market5
drive truck0 market5 market3

soft-AS_property use_t0_market3_market5
drive_t0_market3_market5



set drive_t0_depot0_market3 2
drive truck0 depot0 market3
drive truck0 market3 depot0

soft-AS_property use_t0_depot0_market3
drive_t0_depot0_market3



set load_g8_m5 1
load goods8 truck market5


set load_g8_m4 1
load goods8 truck market4


soft-AS_property load_g8_m5_not_m4
&& load_g8_m5 ! load_g8_m4


set drive_t0_market2_market4 2
drive truck0 market2 market4
drive truck0 market4 market2

soft-AS_property use_t0_market2_market4
drive_t0_market2_market4



