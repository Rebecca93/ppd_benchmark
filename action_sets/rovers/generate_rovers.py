import sys
import random
from random import shuffle

if len(sys.argv) < 4:
    print "usage: python generate_rovers.py name_prefix size goals"
    print "This program generates the domain and problem for rovers."
    quit()

size = int(sys.argv[2])
goals = int(sys.argv[3])

problem_name = "problem_" + sys.argv[1] + "_" + str(size) + "wps_" + str(goals) + "goals.pddl"

#########
# goals #
#########

num_goal_rock = random.randint(0,goals)
num_goal_image = goals - num_goal_rock

goal_image = []
goal_rock = list(range(size))
shuffle(goal_rock)
goal_rock = goal_rock[0:num_goal_rock]

for i in range(num_goal_image):
    view = []
    for waypoint in range(size):
        if random.randint(0,1)==0:
            view.append(waypoint)
    if len(view)==0:
        view.append(0)
    goal_image.append(view)

print goal_rock
print goal_image

###########
# problem #
###########

pro = open(problem_name,"w+")


pro.write("(define (problem rovers-p" + sys.argv[1] + "_" + str(size) + "wps_" + str(goals) + "goals)\n")
pro.write("(:domain Rover)\n")
pro.write("(:objects\n")

pro.write("    general - Lander\n")
pro.write("    colour high_res low_res - Mode\n")
pro.write("    rover0 rover1 - Rover\n")
pro.write("    rover0store rover1store - Store\n")
pro.write("    camera00 camera01 camera02 camera10 camera11 camera12 - Camera\n")

pro.write("    ")
for i in range(0,size):
    pro.write("waypoint"+str(i)+" ")
pro.write("- Waypoint\n")

if len(goal_image)>0:
    pro.write("    ")
    for i in range(0,len(goal_image)):
        pro.write("objective"+str(i)+" ")
    pro.write("- Objective\n")

pro.write(")\n")
pro.write("\n")


pro.write("(:init\n")
pro.write("\n")

pro.write("    (at_lander general waypoint0)\n")
pro.write("    (channel_free general)\n")
pro.write("\n")

# waypoint visibility and traversal
for i in range(size):
    for j in range(size):
        pro.write("    (visible waypoint"+str(i)+" waypoint"+str(j)+")\n")
        if i!=j:
            pro.write("    (= (energycost waypoint"+str(i)+" waypoint"+str(j)+") "+str(random.randint(1,16))+")\n")
            pro.write("    (can_traverse rover0 waypoint"+str(i)+" waypoint"+str(j)+")\n")
            pro.write("    (can_traverse rover1 waypoint"+str(i)+" waypoint"+str(j)+")\n")
pro.write("\n")

# rock sample goals
for i in range(len(goal_rock)):
    pro.write("    (at_rock_sample waypoint"+str(goal_rock[i])+")\n")

# objective image goals
for obj in range(len(goal_image)):
    for wps in goal_image[obj]:
        pro.write("    (visible_from objective"+str(obj)+" waypoint"+str(wps)+")\n")

# rovers
for r in range(2):
    pro.write("    (at rover"+str(r)+" waypoint0)\n")
    pro.write("    (available rover"+str(r)+")\n")
    pro.write("    (store_of rover"+str(r)+"store rover"+str(r)+")\n")
    pro.write("    (empty rover"+str(r)+"store)\n")
    pro.write("    (equipped_for_soil_analysis rover"+str(r)+")\n")
    pro.write("    (equipped_for_rock_analysis rover"+str(r)+")\n")
    pro.write("    (equipped_for_imaging rover"+str(r)+")\n")
    # cameras
    for c in range(3):
        pro.write("    (on_board camera"+str(r)+str(c)+" rover"+str(r)+")\n")
        pro.write("    (supports camera"+str(r)+str(c)+" colour)\n")
        pro.write("    (supports camera"+str(r)+str(c)+" high_res)\n")
        pro.write("    (supports camera"+str(r)+str(c)+" low_res)\n")
        for obj in range(len(goal_image)):
            pro.write("    (calibration_target camera"+str(r)+str(c)+" objective"+str(obj)+" )\n")

pro.write(")\n")
pro.write("\n")

pro.write("(:goal (and\n")
for i in range(len(goal_rock)):
    pro.write("    (communicated_rock_data waypoint"+str(goal_rock[i])+")\n")
for i in range(len(goal_image)):
    image_type = random.randint(0,2)
    if image_type == 0:
        pro.write("    (communicated_image_data objective"+str(i)+" colour)\n")
    if image_type == 1:
        pro.write("    (communicated_image_data objective"+str(i)+" high_res)\n")
    if image_type == 2:
        pro.write("    (communicated_image_data objective"+str(i)+" low_res)\n")
pro.write("))\n")

pro.write("(:metric minimize (total-cost))\n")

pro.write(")\n")

pro.close()
