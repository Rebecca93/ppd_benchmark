soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property load_g6_m5_not_m3
<> && && available(goods6,market5) ! available(goods6,market3) X loaded(goods6,truck0)


soft-LTL_property load_g0_m2_not_m5
<> && && available(goods0,market2) ! available(goods0,market5) X loaded(goods0,truck0)


