soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property load_g1_m2_not_m1
<> && && available(goods1,market2) ! available(goods1,market1) X loaded(goods1,truck0)


soft-LTL_property load_g0_m2_not_m5
<> && && available(goods0,market2) ! available(goods0,market5) X loaded(goods0,truck0)


soft-LTL_property load_g2_m5_not_m4
<> && && available(goods2,market5) ! available(goods2,market4) X loaded(goods2,truck0)


soft-LTL_property use_t0_market2_market3
|| <> && at(truck0,market2) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market2)


soft-LTL_property use_not_t0_market5_market3
&& [] || ! at(truck0,market5) ! X at(truck0,market3) [] || ! at(truck0,market3) ! X at(truck0,market5)


