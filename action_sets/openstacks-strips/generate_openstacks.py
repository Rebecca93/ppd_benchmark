import sys
import random
from random import shuffle

if len(sys.argv) < 4:
    print "usage: python generate_openstacks.py name_prefix size goals"
    print "This program generates the domain and problem for openstacks."
    quit()

size = int(sys.argv[2])
goals = int(sys.argv[3])

domain_name = "problem_" + sys.argv[1] + "_" + str(goals) + "orders-domain.pddl"
problem_name = "problem_" + sys.argv[1] + "_" + str(goals) + "orders.pddl"

##########################
# map products to orders #
##########################

order_to_product_map = {}
product_to_order_map = {}

for i in range(0,size):
    order_to_product_map[i] = []
    product_to_order_map[i] = []

for i in range(0,size):
    prods = list(range(size))
    shuffle(prods)
    for j in range(0,4):
        order_to_product_map[i].append(prods[j])
        product_to_order_map[prods[j]].append(i)

################
# write domain #
################

dom = open(domain_name,"w+")

# header
dom.write("(define (domain openstacks-time-nonADL-nonNegated)\n")
dom.write("(:requirements :typing)\n")
dom.write("(:types order product)\n")

# constants
dom.write("(:constants\n")

for i in range(0,size):
    dom.write("p"+str(i)+" ")
dom.write(" - product\n")

for i in range(0,size):
    dom.write("o"+str(i)+" ")
dom.write(" - order\n")

dom.write(")\n")
dom.write("\n")

# predicates
dom.write("(:predicates\n")
dom.write("    (includes ?o - order ?p - product)\n")
dom.write("    (waiting ?o - order)\n")
dom.write("    (started ?o - order)\n")
dom.write("    (shipped ?o - order)\n")
dom.write("    (made ?p - product)\n")
dom.write("    (not-made ?p - product)\n")
dom.write(")\n")
dom.write("\n")

# predicates
dom.write("(:functions\n")
dom.write("    (total-capacity)\n")
dom.write("    (stacks-used)\n")
dom.write(")\n")
dom.write("\n")

# start order operator
dom.write("(:action start-order-0\n")
dom.write(":parameters (?o - order)\n")
dom.write(":precondition (and\n")
dom.write("    (waiting ?o)\n")
dom.write("    )\n")
dom.write(":effect (and\n")
dom.write("    (not (waiting ?o))\n")
dom.write("    (started ?o)\n")
dom.write("    (increase (stacks-used) 1)\n")
dom.write("    )\n")
dom.write(")\n")
dom.write("\n")

# start order operator 2
dom.write("(:action start-order-1\n")
dom.write(":parameters (?o - order)\n")
dom.write(":precondition (and\n")
dom.write("    (waiting ?o)\n")
dom.write("    )\n")
dom.write(":effect (and\n")
dom.write("    (not (waiting ?o))\n")
dom.write("    (started ?o)\n")
dom.write("    (increase (stacks-used) 2)\n")
dom.write("    )\n")
dom.write(")\n")
dom.write("\n")

# start order operator 3
dom.write("(:action start-order-2\n")
dom.write(":parameters (?o - order)\n")
dom.write(":precondition (and\n")
dom.write("    (waiting ?o)\n")
dom.write("    )\n")
dom.write(":effect (and\n")
dom.write("    (not (waiting ?o))\n")
dom.write("    (started ?o)\n")
dom.write("    (increase (stacks-used) 3)\n")
dom.write("    )\n")
dom.write(")\n")
dom.write("\n")

# start order operator 4
dom.write("(:action start-order-3\n")
dom.write(":parameters (?o - order)\n")
dom.write(":precondition (and\n")
dom.write("    (waiting ?o)\n")
dom.write("    )\n")
dom.write(":effect (and\n")
dom.write("    (not (waiting ?o))\n")
dom.write("    (started ?o)\n")
dom.write("    (increase (stacks-used) 4)\n")
dom.write("    )\n")
dom.write(")\n")
dom.write("\n")

# make product operators
for p in range(0,size):
    dom.write("(:action make-product-p"+str(p)+"\n")
    dom.write(":parameters ()\n")
    dom.write(":precondition (and\n")
    dom.write("    (not-made p"+str(p)+")\n")
    for o in  product_to_order_map[p]:
        dom.write("    (started o"+str(o)+")\n")
    dom.write("    )\n")
    dom.write(":effect (and\n")
    dom.write("    (not (not-made p"+str(p)+"))\n")
    dom.write("    (made p"+str(p)+")\n")
    dom.write("    )\n")
    dom.write(")\n")
    dom.write("\n")

# ship order operators
for o in range(0,size):
    dom.write("(:action ship-order-o"+str(o)+"\n")
    dom.write(":parameters ()\n")
    dom.write(":precondition (and\n")
    dom.write("    (started o"+str(o)+")\n")
    for p in  order_to_product_map[o]:
        dom.write("    (made p"+str(p)+")\n")
    dom.write("    )\n")
    dom.write(":effect (and\n")
    dom.write("    (not (started o"+str(o)+"))\n")
    dom.write("    (decrease (stacks-used) 1)\n")
    dom.write("    (shipped o"+str(o)+")\n")
    dom.write("    )\n")
    dom.write(")\n")
    dom.write("\n")

dom.write(")\n")

dom.close()

#################
# write problem #
#################

pro = open(problem_name,"w+")

pro.write("(define (problem os-time-p"+sys.argv[1]+"_"+str(size)+")\n")
pro.write("(:domain openstacks-time-nonADL-nonNegated)\n")

pro.write("(:init\n")
pro.write("\n")

pro.write("(= (total-capacity) " + str(size) + ")\n")
pro.write("(= (stacks-used) 0)\n")

# orders
for i in range(0,size):
    pro.write("(waiting o"+str(i)+")\n")
pro.write("\n")

# products
for i in range(0,size):
    pro.write("(not-made p"+str(i)+")\n")
pro.write("\n")

# order to product map
for o in range(0,size):
    for p in order_to_product_map[o]:
        pro.write("(includes o"+str(o)+" p"+str(p)+")\n")
pro.write("\n")

pro.write(")\n")
pro.write("\n")

# goal
goallist = list(range(size))
shuffle(goallist)

pro.write("(:goal (and\n")
for g in range(0,goals):
    pro.write("(shipped o"+str(goallist[g])+")\n")
pro.write("))\n")
pro.write(")\n")
pro.write("\n")

pro.close()
