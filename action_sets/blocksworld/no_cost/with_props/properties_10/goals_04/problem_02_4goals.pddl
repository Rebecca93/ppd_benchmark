(define (problem blocksworld-p02_4)
(:domain blocksworld)
(:objects
a b c d e - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear e)
(on b a)
(on c b)
(on d c)
(on e d)
)
(:goal (and
(on a d)
(on e a)
(on c e)
(on b c)
))
(:metric minimize (total-cost))
)
