(define (problem blocksworld-p04_3)
(:domain blocksworld)
(:objects
a b c d - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear d)
(on b a)
(on c b)
(on d c)
)
(:goal (and
(on d b)
(on c d)
(on a c)
))
(:metric minimize (total-cost))
)
