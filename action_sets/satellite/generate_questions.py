import sys
import os
import random
import string
import itertools
from random import shuffle


if len(sys.argv) < 3:
    print "usage: python generate_questions.py problemfile propertiesfile"
    print "This program generates the questions file for a satellite problem instance."
    quit()

potential_questions = []

# read hard goals
prob_file = open(sys.argv[1], "r") 
readingGoals = False
for line in prob_file:
    if "(:goal" in line:
        readingGoals = True
    if readingGoals and "(have_image" in line:
        q = line.split()[0][1:]+"("
        for qs in line.split()[1:]:
            q = q+qs+","
        q = q[0:len(q)-1]
        potential_questions.append(q.rstrip())
prob_file.close()

# read soft goals
prop_file = open(sys.argv[2], "r") 
for line in prop_file:
    if "soft-property" in line:
        potential_questions.append("sat_"+line[14:].rstrip())
prob_file.close()

# write question files
random.shuffle(potential_questions)
filename_base = os.path.basename(os.path.normpath(sys.argv[2]))
for i in range(len(potential_questions)):
    if i == 0: continue
    count = 0
    for subset in itertools.combinations(potential_questions, i):
        filename = filename_base+"_s"+str(i)+"_"+str(count)+".q"
        count += 1
        f = open(filename, "a")
        for prop in subset:
            f.write(prop+"\n")
        f.close()
        if count > 9:
            break
