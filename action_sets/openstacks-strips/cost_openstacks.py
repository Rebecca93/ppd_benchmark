import sys
import re
from itertools import product

if len(sys.argv) < 3:
    print "usage: python cost_openstacks.py problem cost-scale"
    print "This program converts a openstacks problem instance and domain to strips-style numerics."
    quit()

# read pfile
maxcost = 5
maxstacks = 1
problem = open(sys.argv[1], "r") 
for line in problem:
    if "(= (total-cost) " in line:
        energy = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        if(int(energy[0]) > maxcost):
            maxcost = int(energy[0])
    if "(= (total-capacity) " in line:
        cap = [s for s in re.split('\)| |\.',line) if s.isdigit()]
        if(int(cap[0]) > maxstacks):
            maxstacks = int(cap[0])
problem.close()

maxcost = int(maxcost * float(sys.argv[2]))

# read pfile
prob = open(sys.argv[1], "r") 
for line in prob:
    if "metric" in line:
        print
    elif ":init" in line:
        print "(:objects",
        for i in range(6,max(maxcost,maxstacks)+1):
            print "level"+str(i),
        print "- num)"
        print line
        for i,j in product(range(0,max(maxcost,maxstacks)+1), range(0,max(maxcost,maxstacks)+1)):
            if (i+j <= max(maxcost,maxstacks)):
                print "(sum level" + str(i) + " level" + str(j) + " level" + str(i+j) + ")"
    elif "total-cost" in line:
        print "(total-cost level" + str(maxcost) + ")"
    elif "total-capacity" in line:
        print "(total-capacity level" + str(maxstacks) + ")"
    else:
        print line,
prob.close()
