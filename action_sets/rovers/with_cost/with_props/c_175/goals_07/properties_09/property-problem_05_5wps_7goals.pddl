set samplerock_rover1_waypoint3 1
sample_rock rover1 store waypoint3

set take_image_rover1_waypoint3_objective3 1
take_image rover1 waypoint3 objective3 camera mode

set take_image_rover1_waypoint1_objective0 1
take_image rover1 waypoint1 objective0 camera mode

set samplerock_rover0_waypoint4 1
sample_rock rover0 store waypoint4

set take_image_rover1_waypoint0_objective1 1
take_image rover1 waypoint0 objective1 camera mode

set take_image_camera01_objective2 1
take_image rover waypoint objective2 camera01 mode

set take_image_rover1_waypoint0_objective3 1
take_image rover1 waypoint0 objective3 camera mode

set take_image_camera10_objective1 1
take_image rover waypoint objective1 camera10 mode

set take_image_rover1_waypoint1_objective2 1
take_image rover1 waypoint1 objective2 camera mode

soft-property preferred_sample_rover1_waypoint3
samplerock_rover1_waypoint3

soft-property preferred_take_image_rover1_waypoint3_objective3
take_image_rover1_waypoint3_objective3

soft-property preferred_take_image_rover1_waypoint1_objective0
take_image_rover1_waypoint1_objective0

soft-property preferred_sample_rover0_waypoint4
samplerock_rover0_waypoint4

soft-property preferred_take_image_rover1_waypoint0_objective1
take_image_rover1_waypoint0_objective1

soft-property preferred_take_image_camera01_objective2
take_image_camera01_objective2

soft-property preferred_take_image_rover1_waypoint0_objective3
take_image_rover1_waypoint0_objective3

soft-property preferred_take_image_camera10_objective1
take_image_camera10_objective1

soft-property preferred_take_image_rover1_waypoint1_objective2
take_image_rover1_waypoint1_objective2

