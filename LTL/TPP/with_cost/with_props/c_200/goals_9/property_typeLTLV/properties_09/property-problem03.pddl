soft-LTL_property use_t0_market4_depot0
|| <> && at(truck0,market4) X at(truck0,depot0) <> && at(truck0,depot0)  X at(truck0,market4)


soft-LTL_property use_not_t0_market2_depot0
&& [] || ! at(truck0,market2) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market2)


soft-LTL_property load_g6_m4_not_m3
<> && && available(goods6,market4) ! available(goods6,market3) X loaded(goods6,truck0)


soft-LTL_property use_not_t0_market3_depot0
&& [] || ! at(truck0,market3) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market3)


soft-LTL_property use_t0_market3_market2
|| <> && at(truck0,market3) X at(truck0,market2) <> && at(truck0,market2)  X at(truck0,market3)


soft-LTL_property use_t0_market1_market3
|| <> && at(truck0,market1) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market1)


soft-LTL_property load_g8_m5_not_m3
<> && && available(goods8,market5) ! available(goods8,market3) X loaded(goods8,truck0)


soft-LTL_property use_t0_market2_market4
|| <> && at(truck0,market2) X at(truck0,market4) <> && at(truck0,market4)  X at(truck0,market2)


soft-LTL_property use_not_t0_market1_market4
&& [] || ! at(truck0,market1) ! X at(truck0,market4) [] || ! at(truck0,market4) ! X at(truck0,market1)


