set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set load_g6_m5 1
load goods6 truck market5


set load_g6_m3 1
load goods6 truck market3


soft-AS_property load_g6_m5_not_m3
&& load_g6_m5 ! load_g6_m3


set load_g1_m1 1
load goods1 truck market1


set load_g1_m2 1
load goods1 truck market2


soft-AS_property load_g1_m1_not_m2
&& load_g1_m1 ! load_g1_m2


set load_g4_m5 1
load goods4 truck market5


set load_g4_m2 1
load goods4 truck market2


soft-AS_property load_g4_m5_not_m2
&& load_g4_m5 ! load_g4_m2


set drive_t0_market2_market3 2
drive truck0 market2 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market2 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market2_market3
drive_t0_market2_market3



set drive_t0_market5_market3 2
drive truck0 market5 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market5 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market5_market3
! drive_t0_market5_market3



set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-AS_property use_t0_depot0_market5
drive_t0_depot0_market5



