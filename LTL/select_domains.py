
def twoDigit(i):
    assert(i < 100)
    if i > 0 and i < 10:
        return "0" + str(i)
    else:
        return str(i)

def cost_scaled_instances(base, scale, g_lower, g_upper, p_lower, p_upper, prop_type):

    goals = []
    for g in range(g_lower, g_upper+1):
        goals.append(str(g))
    properties = []
    for p in range(p_lower, p_upper+1):
        properties.append(twoDigit(p))

    path = []
    for g in goals:
        for p in properties:
            path.append(base + "/c_" + scale + "/goals_" + g + "/property_type/" + prop_type + "/properties_" + p + "/")

    return path


