set drive_t0_market5_market1 2
drive truck0 market5 market1
drive truck0 market1 market5

soft-AS_property use_t0_market5_market1
drive_t0_market5_market1



set drive_t0_market3_market2 2
drive truck0 market3 market2
drive truck0 market2 market3

soft-AS_property use_not_t0_market3_market2
! drive_t0_market3_market2



set load_g1_m5 1
load goods1 truck market5


set load_g1_m4 1
load goods1 truck market4


soft-AS_property load_g1_m5_not_m4
&& load_g1_m5 ! load_g1_m4


set drive_t0_market4_market2 2
drive truck0 market4 market2
drive truck0 market2 market4

soft-AS_property use_not_t0_market4_market2
! drive_t0_market4_market2



set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



set drive_t0_market1_market4 2
drive truck0 market1 market4
drive truck0 market4 market1

soft-AS_property use_t0_market1_market4
drive_t0_market1_market4



set load_g2_m5 1
load goods2 truck market5


set load_g2_m3 1
load goods2 truck market3


soft-AS_property load_g2_m5_not_m3
&& load_g2_m5 ! load_g2_m3


set drive_t0_market3_market5 2
drive truck0 market3 market5
drive truck0 market5 market3

soft-AS_property use_t0_market3_market5
drive_t0_market3_market5



set drive_t0_market2_market5 2
drive truck0 market2 market5
drive truck0 market5 market2

soft-AS_property use_t0_market2_market5
drive_t0_market2_market5



