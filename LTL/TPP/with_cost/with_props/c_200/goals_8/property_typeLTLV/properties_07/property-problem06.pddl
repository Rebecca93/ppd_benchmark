soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property load_g6_m5_not_m3
<> && && available(goods6,market5) ! available(goods6,market3) X loaded(goods6,truck0)


soft-LTL_property load_g0_m2_not_m5
<> && && available(goods0,market2) ! available(goods0,market5) X loaded(goods0,truck0)


soft-LTL_property load_g4_m5_not_m2
<> && && available(goods4,market5) ! available(goods4,market2) X loaded(goods4,truck0)


soft-LTL_property use_t0_market2_market3
|| <> && at(truck0,market2) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market2)


soft-LTL_property use_not_t0_market5_market3
&& [] || ! at(truck0,market5) ! X at(truck0,market3) [] || ! at(truck0,market3) ! X at(truck0,market5)


soft-LTL_property use_t0_depot0_market5
|| <> && at(truck0,depot0) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,depot0)


