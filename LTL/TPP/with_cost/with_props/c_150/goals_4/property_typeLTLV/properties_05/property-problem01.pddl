soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property use_not_t0_market1_market3
&& [] || ! at(truck0,market1) ! X at(truck0,market3) [] || ! at(truck0,market3) ! X at(truck0,market1)


soft-LTL_property load_g3_m5_not_m1
<> && && available(goods3,market5) ! available(goods3,market1) X loaded(goods3,truck0)


soft-LTL_property use_not_t0_market3_depot0
&& [] || ! at(truck0,market3) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market3)


soft-LTL_property use_t0_market4_market2
|| <> && at(truck0,market4) X at(truck0,market2) <> && at(truck0,market2)  X at(truck0,market4)


