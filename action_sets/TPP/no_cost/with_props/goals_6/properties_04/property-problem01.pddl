set load_g1_m4 1
load goods1 truck market4


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m4_not_m2
&& load_g1_m4 ! load_g1_m2


set drive_t0_depot0_market3 2
drive truck0 depot0 market3
drive truck0 market3 depot0

soft-property use_not_t0_depot0_market3
! drive_t0_depot0_market3



set load_g5_m1 1
load goods5 truck market1


set load_g5_m2 1
load goods5 truck market2


soft-property load_g5_m1_not_m2
&& load_g5_m1 ! load_g5_m2


set load_g2_m3 1
load goods2 truck market3


set load_g2_m2 1
load goods2 truck market2


soft-property load_g2_m3_not_m2
&& load_g2_m3 ! load_g2_m2


