set drive_t0_market4_depot0 2
drive truck0 market4 depot0
drive truck0 depot0 market4

soft-AS_property use_t0_market4_depot0
drive_t0_market4_depot0



set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-AS_property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set load_g6_m4 1
load goods6 truck market4


set load_g6_m3 1
load goods6 truck market3


soft-AS_property load_g6_m4_not_m3
&& load_g6_m4 ! load_g6_m3


set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-AS_property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market3_market2 2
drive truck0 market3 market2
drive truck0 market2 market3

soft-AS_property use_t0_market3_market2
drive_t0_market3_market2



set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-AS_property use_t0_market1_market3
drive_t0_market1_market3



set load_g8_m5 1
load goods8 truck market5


set load_g8_m3 1
load goods8 truck market3


soft-AS_property load_g8_m5_not_m3
&& load_g8_m5 ! load_g8_m3


