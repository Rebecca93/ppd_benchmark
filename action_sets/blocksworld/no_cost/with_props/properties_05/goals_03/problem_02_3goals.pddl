(define (problem blocksworld-p02_3)
(:domain blocksworld)
(:objects
a b c d - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear d)
(on b a)
(on c b)
(on d c)
)
(:goal (and
(on b c)
(on d b)
(on a d)
))
(:metric minimize (total-cost))
)
