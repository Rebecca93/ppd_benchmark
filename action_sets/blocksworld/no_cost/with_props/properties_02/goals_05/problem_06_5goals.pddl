(define (problem blocksworld-p06_5)
(:domain blocksworld)
(:objects
a b c d e f - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear f)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
)
(:goal (and
(on d e)
(on a d)
(on f a)
(on c f)
(on b c)
))
(:metric minimize (total-cost))
)
