import sys
import re
from itertools import product

if len(sys.argv) < 3:
    print "usage: python cost_satellite.py file scale"
    quit()

# read problem
scale = sys.argv[2]
problem = open(sys.argv[1], "r")
for line in problem:
    strings = line.strip().split(" ")
    print strings[0] + " " + str(int(float(strings[1])*float(scale)))
