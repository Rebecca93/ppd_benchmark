soft-LTL_property use_t1_l4_l3
|| <> && at(t1,l4) X at(t1,l3) <> && at(t1,l3)  X at(t1,l4)


soft-LTL_property same_truck_p1p2
|| [] && ! in(p1,t1) ! in(p2,t1)  [] && ! in(p1,t0) ! in(p2,t0)


soft-LTL_property same_truck_p0p2
|| [] && ! in(p0,t1) ! in(p2,t1)  [] && ! in(p0,t0) ! in(p2,t0)


soft-LTL_property use_t1_l0_l4
|| <> && at(t1,l0) X at(t1,l4) <> && at(t1,l4)  X at(t1,l0)


