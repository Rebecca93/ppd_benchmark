soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property load_g3_m3_not_m1
<> && && available(goods3,market3) ! available(goods3,market1) X loaded(goods3,truck0)


soft-LTL_property load_g0_m2_not_m5
<> && && available(goods0,market2) ! available(goods0,market5) X loaded(goods0,truck0)


soft-LTL_property load_g5_m5_not_m3
<> && && available(goods5,market5) ! available(goods5,market3) X loaded(goods5,truck0)


