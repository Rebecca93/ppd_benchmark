soft-LTL_property use_t0_market5_market1
|| <> && at(truck0,market5) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market5)


soft-LTL_property use_not_t0_market3_market2
&& [] || ! at(truck0,market3) ! X at(truck0,market2) [] || ! at(truck0,market2) ! X at(truck0,market3)


soft-LTL_property load_g6_m2_not_m1
<> && && available(goods6,market2) ! available(goods6,market1) X loaded(goods6,truck0)


soft-LTL_property use_not_t0_market4_market2
&& [] || ! at(truck0,market4) ! X at(truck0,market2) [] || ! at(truck0,market2) ! X at(truck0,market4)


soft-LTL_property use_t0_market4_market5
|| <> && at(truck0,market4) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market4)


soft-LTL_property use_t0_market1_market4
|| <> && at(truck0,market1) X at(truck0,market4) <> && at(truck0,market4)  X at(truck0,market1)


soft-LTL_property load_g8_m3_not_m2
<> && && available(goods8,market3) ! available(goods8,market2) X loaded(goods8,truck0)


soft-LTL_property use_t0_market3_market5
|| <> && at(truck0,market3) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market3)


soft-LTL_property use_t0_market2_market5
|| <> && at(truck0,market2) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market2)


soft-LTL_property use_t0_market4_depot0
|| <> && at(truck0,market4) X at(truck0,depot0) <> && at(truck0,depot0)  X at(truck0,market4)


