set drive_t1_l4_l2 2
drive t1 l4 l2
drive t1 l2 l4

soft-property use_t1_l4_l2
drive_t1_l4_l2


set delwithT0_p2p3 4
unload p2 t0 location
load p2 t0 location
unload p3 t0 location
load p3 t0 location

set delwithT1_p2p3 4
unload p2 t1 location
load p2 t1 location
unload p3 t1 location
load p3 t1 location

soft-property same_truck_p2p3
|| && delwithT0_p2p3 ! delwithT1_p2p3 && ! delwithT0_p2p3 delwithT1_p2p3


set drive_t1_l0_l4 2
drive t1 l0 l4
drive t1 l4 l0

soft-property use_t1_l0_l4
drive_t1_l0_l4


