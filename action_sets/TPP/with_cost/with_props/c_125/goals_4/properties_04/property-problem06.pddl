set load_g3_m1 1
load goods3 truck market1


set load_g3_m3 1
load goods3 truck market3


soft-property load_g3_m1_not_m3
&& load_g3_m1 ! load_g3_m3


set load_g2_m5 1
load goods2 truck market5


set load_g2_m4 1
load goods2 truck market4


soft-property load_g2_m5_not_m4
&& load_g2_m5 ! load_g2_m4


set drive_t0_market2_market5 2
drive truck0 market2 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market5
drive_t0_market2_market5



set drive_t0_market2_market3 2
drive truck0 market2 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market3
drive_t0_market2_market3



