(define (problem tpp-problem-m5-g7-c1-s49-enc2)
(:domain TPPLike-Metric)
(:objects
	market1 market2 market3 market4 market5 - market
	depot0 - depot
	truck0 - truck
	goods0 goods1 goods2 goods3 goods4 goods5 goods6 - goods
	)
(:init
	(= (price goods0 market1) 1)
	(on-sale goods0 market1)
	(= (price goods0 market2) 1)
	(on-sale goods0 market2)
	(= (price goods1 market1) 1)
	(on-sale goods1 market1)
	(= (price goods1 market2) 1)
	(on-sale goods1 market2)
	(= (price goods2 market1) 1)
	(on-sale goods2 market1)
	(= (price goods2 market4) 1)
	(on-sale goods2 market4)
	(= (price goods3 market4) 1)
	(on-sale goods3 market4)
	(= (price goods3 market5) 1)
	(on-sale goods3 market5)
	(= (price goods4 market2) 1)
	(on-sale goods4 market2)
	(= (price goods4 market3) 1)
	(on-sale goods4 market3)
	(= (price goods5 market2) 1)
	(on-sale goods5 market2)
	(= (price goods5 market4) 1)
	(on-sale goods5 market4)
	(= (price goods6 market2) 1)
	(on-sale goods6 market2)
	(= (price goods6 market5) 1)
	(on-sale goods6 market5)
	(= (drive-cost depot0 market2) 3)
	(connected depot0 market2)
	(= (drive-cost depot0 market4) 2)
	(connected depot0 market4)
	(= (drive-cost depot0 market5) 4)
	(connected depot0 market5)
	(= (drive-cost market1 market2) 4)
	(connected market1 market2)
	(= (drive-cost market1 market3) 3)
	(connected market1 market3)
	(= (drive-cost market1 market5) 4)
	(connected market1 market5)
	(= (drive-cost market2 depot0) 3)
	(connected market2 depot0)
	(= (drive-cost market2 market1) 4)
	(connected market2 market1)
	(= (drive-cost market2 market4) 3)
	(connected market2 market4)
	(= (drive-cost market2 market5) 1)
	(connected market2 market5)
	(= (drive-cost market3 market1) 3)
	(connected market3 market1)
	(= (drive-cost market3 market4) 1)
	(connected market3 market4)
	(= (drive-cost market4 depot0) 2)
	(connected market4 depot0)
	(= (drive-cost market4 market2) 3)
	(connected market4 market2)
	(= (drive-cost market4 market3) 1)
	(connected market4 market3)
	(= (drive-cost market5 depot0) 4)
	(connected market5 depot0)
	(= (drive-cost market5 market1) 4)
	(connected market5 market1)
	(= (drive-cost market5 market2) 1)
	(connected market5 market2)
	(at truck0 depot0)
)
(:goal (and
	(stored goods0)
	(stored goods1)
	(stored goods2)
	(stored goods3)
	(stored goods4)
	(stored goods5)
	(stored goods6)
))
(:metric minimize (total-cost))
)
