soft-LTL_property use_t1_l4_l3
|| <> && at(t1,l4) X at(t1,l3) <> && at(t1,l3)  X at(t1,l4)


soft-LTL_property same_truck_p3p5
|| [] && ! in(p3,t1) ! in(p5,t1)  [] && ! in(p3,t0) ! in(p5,t0)


soft-LTL_property same_truck_p0p6
|| [] && ! in(p0,t1) ! in(p6,t1)  [] && ! in(p0,t0) ! in(p6,t0)


soft-LTL_property same_truck_p0p5
|| [] && ! in(p0,t1) ! in(p5,t1)  [] && ! in(p0,t0) ! in(p5,t0)


soft-LTL_property use_t1_l1_l5
|| <> && at(t1,l1) X at(t1,l5) <> && at(t1,l5)  X at(t1,l1)


soft-LTL_property use_t1_l1_l3
|| <> && at(t1,l1) X at(t1,l3) <> && at(t1,l3)  X at(t1,l1)


