(define (problem blocksworld-p07_6)
(:domain blocksworld)
(:objects
a b c d e f g - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear g)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
)
(:goal (and
(on a c)
(on e a)
(on g e)
(on f g)
(on b f)
(on d b)
))
(:metric minimize (total-cost))
)
