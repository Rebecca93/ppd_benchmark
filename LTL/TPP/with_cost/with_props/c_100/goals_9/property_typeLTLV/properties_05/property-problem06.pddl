soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property load_g6_m5_not_m3
<> && && available(goods6,market5) ! available(goods6,market3) X loaded(goods6,truck0)


soft-LTL_property load_g1_m1_not_m2
<> && && available(goods1,market1) ! available(goods1,market2) X loaded(goods1,truck0)


soft-LTL_property load_g4_m5_not_m2
<> && && available(goods4,market5) ! available(goods4,market2) X loaded(goods4,truck0)


soft-LTL_property use_t0_market2_market3
|| <> && at(truck0,market2) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market2)


