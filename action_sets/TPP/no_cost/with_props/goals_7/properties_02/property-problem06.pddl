set load_g3_m1 1
load goods3 truck market1


set load_g3_m3 1
load goods3 truck market3


soft-property load_g3_m1_not_m3
&& load_g3_m1 ! load_g3_m3


set load_g5_m5 1
load goods5 truck market5


set load_g5_m3 1
load goods5 truck market3


soft-property load_g5_m5_not_m3
&& load_g5_m5 ! load_g5_m3


