set load_g3_m1 1
load goods3 truck market1


set load_g3_m3 1
load goods3 truck market3


soft-property load_g3_m1_not_m3
&& load_g3_m1 ! load_g3_m3


set load_g4_m2 1
load goods4 truck market2


set load_g4_m5 1
load goods4 truck market5


soft-property load_g4_m2_not_m5
&& load_g4_m2 ! load_g4_m5


set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-property use_not_t0_depot0_market5
! drive_t0_depot0_market5



