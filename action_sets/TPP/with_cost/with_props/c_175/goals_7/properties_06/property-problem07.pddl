set drive_t0_market4_market1 2
drive truck0 market4 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market4 moneylevel moneylevel moneylevel


soft-property use_t0_market4_market1
drive_t0_market4_market1



set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set load_g6_m1 1
load goods6 truck market1


set load_g6_m5 1
load goods6 truck market5


soft-property load_g6_m1_not_m5
&& load_g6_m1 ! load_g6_m5


set load_g0_m5 1
load goods0 truck market5


set load_g0_m1 1
load goods0 truck market1


soft-property load_g0_m5_not_m1
&& load_g0_m5 ! load_g0_m1


set drive_t0_market1_depot0 2
drive truck0 market1 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market1 moneylevel moneylevel moneylevel


soft-property use_not_t0_market1_depot0
! drive_t0_market1_depot0



