set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-AS_property use_t0_market4_market3
drive_t0_market4_market3



set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-AS_property use_not_t0_market1_market5
! drive_t0_market1_market5



set load_g6_m5 1
load goods6 truck market5


set load_g6_m2 1
load goods6 truck market2


soft-AS_property load_g6_m5_not_m2
&& load_g6_m5 ! load_g6_m2


set drive_t0_market2_market5 2
drive truck0 market2 market5
drive truck0 market5 market2

soft-AS_property use_not_t0_market2_market5
! drive_t0_market2_market5



set drive_t0_market3_market1 2
drive truck0 market3 market1
drive truck0 market1 market3

soft-AS_property use_t0_market3_market1
drive_t0_market3_market1



