state_set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_market3 2
drive truck0 market1 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market1 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market1_market3
[] ! drive_t0_market1_market3



state_set load_g3_m5 1
load goods3 truck market5


state_set load_g3_m1 1
load goods3 truck market1


soft-LTL_property load_g3_m5_not_m1
&& <> load_g3_m5 [] ! load_g3_m1


