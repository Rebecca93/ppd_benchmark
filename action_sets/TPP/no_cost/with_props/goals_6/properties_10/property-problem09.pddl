set drive_t0_market2_market3 2
drive truck0 market2 market3
drive truck0 market3 market2

soft-property use_not_t0_market2_market3
! drive_t0_market2_market3



set drive_t0_market5_market3 2
drive truck0 market5 market3
drive truck0 market3 market5

soft-property use_not_t0_market5_market3
! drive_t0_market5_market3



set load_g0_m1 1
load goods0 truck market1


set load_g0_m3 1
load goods0 truck market3


soft-property load_g0_m1_not_m3
&& load_g0_m1 ! load_g0_m3


set drive_t0_market3_market4 2
drive truck0 market3 market4
drive truck0 market4 market3

soft-property use_t0_market3_market4
drive_t0_market3_market4



set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-property use_not_t0_market1_market5
! drive_t0_market1_market5



set load_g5_m2 1
load goods5 truck market2


set load_g5_m3 1
load goods5 truck market3


soft-property load_g5_m2_not_m3
&& load_g5_m2 ! load_g5_m3


set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-property use_t0_market4_market5
drive_t0_market4_market5



set load_g3_m4 1
load goods3 truck market4


set load_g3_m1 1
load goods3 truck market1


soft-property load_g3_m4_not_m1
&& load_g3_m4 ! load_g3_m1


set load_g1_m4 1
load goods1 truck market4


set load_g1_m3 1
load goods1 truck market3


soft-property load_g1_m4_not_m3
&& load_g1_m4 ! load_g1_m3


set load_g4_m1 1
load goods4 truck market1


set load_g4_m3 1
load goods4 truck market3


soft-property load_g4_m1_not_m3
&& load_g4_m1 ! load_g4_m3


