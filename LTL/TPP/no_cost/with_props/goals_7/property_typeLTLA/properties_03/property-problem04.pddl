state_set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-LTL_property use_t0_market4_market5
<> drive_t0_market4_market5



state_set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-LTL_property use_not_t0_market1_market5
[] ! drive_t0_market1_market5



state_set load_g3_m5 1
load goods3 truck market5


state_set load_g3_m2 1
load goods3 truck market2


soft-LTL_property load_g3_m5_not_m2
&& <> load_g3_m5 [] ! load_g3_m2


