state_set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set load_g6_m5 1
load goods6 truck market5


state_set load_g6_m3 1
load goods6 truck market3


soft-LTL_property load_g6_m5_not_m3
&& <> load_g6_m5 [] ! load_g6_m3


state_set load_g0_m2 1
load goods0 truck market2


state_set load_g0_m5 1
load goods0 truck market5


soft-LTL_property load_g0_m2_not_m5
&& <> load_g0_m2 [] ! load_g0_m5


state_set load_g4_m5 1
load goods4 truck market5


state_set load_g4_m2 1
load goods4 truck market2


soft-LTL_property load_g4_m5_not_m2
&& <> load_g4_m5 [] ! load_g4_m2


state_set drive_t0_market2_market3 2
drive truck0 market2 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market2_market3
<> drive_t0_market2_market3



state_set drive_t0_market5_market3 2
drive truck0 market5 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market5 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market5_market3
[] ! drive_t0_market5_market3



state_set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_depot0_market5
<> drive_t0_depot0_market5



state_set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market2_depot0
<> drive_t0_market2_depot0



state_set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-LTL_property use_not_t0_market3_depot0
[] ! drive_t0_market3_depot0



