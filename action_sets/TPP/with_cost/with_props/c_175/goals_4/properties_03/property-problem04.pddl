set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set load_g1_m3 1
load goods1 truck market3


set load_g1_m2 1
load goods1 truck market2


soft-property load_g1_m3_not_m2
&& load_g1_m3 ! load_g1_m2


set drive_t0_depot0_market1 2
drive truck0 depot0 market1 moneylevel moneylevel moneylevel
drive truck0 market1 depot0 moneylevel moneylevel moneylevel


soft-property use_not_t0_depot0_market1
! drive_t0_depot0_market1



