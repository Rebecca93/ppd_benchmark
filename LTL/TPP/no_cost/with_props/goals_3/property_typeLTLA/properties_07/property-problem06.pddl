state_set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set load_g1_m2 1
load goods1 truck market2


state_set load_g1_m1 1
load goods1 truck market1


soft-LTL_property load_g1_m2_not_m1
&& <> load_g1_m2 [] ! load_g1_m1


state_set load_g0_m2 1
load goods0 truck market2


state_set load_g0_m5 1
load goods0 truck market5


soft-LTL_property load_g0_m2_not_m5
&& <> load_g0_m2 [] ! load_g0_m5


state_set load_g2_m5 1
load goods2 truck market5


state_set load_g2_m4 1
load goods2 truck market4


soft-LTL_property load_g2_m5_not_m4
&& <> load_g2_m5 [] ! load_g2_m4


state_set drive_t0_market2_market3 2
drive truck0 market2 market3
drive truck0 market3 market2

soft-LTL_property use_t0_market2_market3
<> drive_t0_market2_market3



state_set drive_t0_market5_market3 2
drive truck0 market5 market3
drive truck0 market3 market5

soft-LTL_property use_not_t0_market5_market3
[] ! drive_t0_market5_market3



state_set drive_t0_depot0_market5 2
drive truck0 depot0 market5
drive truck0 market5 depot0

soft-LTL_property use_t0_depot0_market5
<> drive_t0_depot0_market5



