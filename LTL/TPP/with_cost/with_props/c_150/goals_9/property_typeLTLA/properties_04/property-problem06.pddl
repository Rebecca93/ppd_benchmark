state_set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set load_g6_m5 1
load goods6 truck market5


state_set load_g6_m3 1
load goods6 truck market3


soft-LTL_property load_g6_m5_not_m3
&& <> load_g6_m5 [] ! load_g6_m3


state_set load_g1_m1 1
load goods1 truck market1


state_set load_g1_m2 1
load goods1 truck market2


soft-LTL_property load_g1_m1_not_m2
&& <> load_g1_m1 [] ! load_g1_m2


state_set load_g4_m5 1
load goods4 truck market5


state_set load_g4_m2 1
load goods4 truck market2


soft-LTL_property load_g4_m5_not_m2
&& <> load_g4_m5 [] ! load_g4_m2


