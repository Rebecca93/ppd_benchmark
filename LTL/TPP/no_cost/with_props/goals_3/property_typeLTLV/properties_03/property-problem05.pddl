soft-LTL_property use_t0_market5_market1
|| <> && at(truck0,market5) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market5)


soft-LTL_property use_not_t0_market3_market2
&& [] || ! at(truck0,market3) ! X at(truck0,market2) [] || ! at(truck0,market2) ! X at(truck0,market3)


soft-LTL_property load_g1_m5_not_m4
<> && && available(goods1,market5) ! available(goods1,market4) X loaded(goods1,truck0)


