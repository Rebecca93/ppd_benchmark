set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set drive_t0_market5_market4 2
drive truck0 market5 market4
drive truck0 market4 market5

soft-property use_not_t0_market5_market4
! drive_t0_market5_market4



set load_g2_m3 1
load goods2 truck market3


set load_g2_m4 1
load goods2 truck market4


soft-property load_g2_m3_not_m4
&& load_g2_m3 ! load_g2_m4


