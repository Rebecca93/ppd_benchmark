set drive_t0_market4_depot0 2
drive truck0 market4 depot0
drive truck0 depot0 market4

soft-AS_property use_t0_market4_depot0
drive_t0_market4_depot0



set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-AS_property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set load_g3_m3 1
load goods3 truck market3


set load_g3_m2 1
load goods3 truck market2


soft-AS_property load_g3_m3_not_m2
&& load_g3_m3 ! load_g3_m2


set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-AS_property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market3_market2 2
drive truck0 market3 market2
drive truck0 market2 market3

soft-AS_property use_t0_market3_market2
drive_t0_market3_market2



set drive_t0_market1_market3 2
drive truck0 market1 market3
drive truck0 market3 market1

soft-AS_property use_t0_market1_market3
drive_t0_market1_market3



