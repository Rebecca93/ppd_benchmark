state_set drive_t0_market4_market3 2
drive truck0 market4 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market4 moneylevel moneylevel moneylevel


soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set load_g3_m3 1
load goods3 truck market3


state_set load_g3_m1 1
load goods3 truck market1


soft-LTL_property load_g3_m3_not_m1
&& <> load_g3_m3 [] ! load_g3_m1


