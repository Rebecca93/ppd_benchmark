set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



set drive_t0_market1_market5 2
drive truck0 market1 market5
drive truck0 market5 market1

soft-AS_property use_not_t0_market1_market5
! drive_t0_market1_market5



set load_g6_m3 1
load goods6 truck market3


set load_g6_m1 1
load goods6 truck market1


soft-AS_property load_g6_m3_not_m1
&& load_g6_m3 ! load_g6_m1


set drive_t0_market2_market4 2
drive truck0 market2 market4
drive truck0 market4 market2

soft-AS_property use_not_t0_market2_market4
! drive_t0_market2_market4



