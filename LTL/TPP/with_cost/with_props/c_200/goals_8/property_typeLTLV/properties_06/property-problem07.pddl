soft-LTL_property use_t0_market4_market5
|| <> && at(truck0,market4) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,market4)


soft-LTL_property use_not_t0_market1_depot0
&& [] || ! at(truck0,market1) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market1)


soft-LTL_property load_g6_m5_not_m1
<> && && available(goods6,market5) ! available(goods6,market1) X loaded(goods6,truck0)


soft-LTL_property use_not_t0_market3_depot0
&& [] || ! at(truck0,market3) ! X at(truck0,depot0) [] || ! at(truck0,depot0) ! X at(truck0,market3)


soft-LTL_property use_t0_market4_depot0
|| <> && at(truck0,market4) X at(truck0,depot0) <> && at(truck0,depot0)  X at(truck0,market4)


soft-LTL_property use_t0_depot0_market5
|| <> && at(truck0,depot0) X at(truck0,market5) <> && at(truck0,market5)  X at(truck0,depot0)


