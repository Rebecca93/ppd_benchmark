(define (problem blocksworld-p06_7)
(:domain blocksworld)
(:objects
a b c d e f g h - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear h)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
(on h g)
)
(:goal (and
(on e a)
(on h e)
(on d h)
(on g d)
(on f g)
(on b f)
(on c b)
))
(:metric minimize (total-cost))
)
