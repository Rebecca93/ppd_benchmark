set drive_t0_market4_market5 2
drive truck0 market4 market5
drive truck0 market5 market4

soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



set drive_t0_market1_depot0 2
drive truck0 market1 depot0
drive truck0 depot0 market1

soft-AS_property use_not_t0_market1_depot0
! drive_t0_market1_depot0



set load_g1_m4 1
load goods1 truck market4


set load_g1_m2 1
load goods1 truck market2


soft-AS_property load_g1_m4_not_m2
&& load_g1_m4 ! load_g1_m2


