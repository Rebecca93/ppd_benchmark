state_set drive_t0_market4_market3 2
drive truck0 market4 market3
drive truck0 market3 market4

soft-LTL_property use_t0_market4_market3
<> drive_t0_market4_market3



state_set drive_t0_market1_depot0 2
drive truck0 market1 depot0
drive truck0 depot0 market1

soft-LTL_property use_not_t0_market1_depot0
[] ! drive_t0_market1_depot0



state_set load_g6_m3 1
load goods6 truck market3


state_set load_g6_m2 1
load goods6 truck market2


soft-LTL_property load_g6_m3_not_m2
&& <> load_g6_m3 [] ! load_g6_m2


state_set drive_t0_market3_depot0 2
drive truck0 market3 depot0
drive truck0 depot0 market3

soft-LTL_property use_not_t0_market3_depot0
[] ! drive_t0_market3_depot0



