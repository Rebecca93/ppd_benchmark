soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


soft-LTL_property use_not_t0_market1_market3
&& [] || ! at(truck0,market1) ! X at(truck0,market3) [] || ! at(truck0,market3) ! X at(truck0,market1)


soft-LTL_property load_g1_m4_not_m2
<> && && available(goods1,market4) ! available(goods1,market2) X loaded(goods1,truck0)


