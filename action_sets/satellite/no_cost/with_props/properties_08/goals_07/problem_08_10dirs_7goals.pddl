(define (problem satellite-p08_10dirs_7goals)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 instrument1 instrument2 - instrument
	image spectograph thermograph - mode
 dir0 dir1 dir2 dir3 dir4 dir5 dir6 dir7 dir8 dir9 - direction
)
(:init
    (supports instrument0 image)
    (supports instrument1 thermograph)
    (supports instrument2 spectograph)
    (calibration_target instrument0 dir0)
    (calibration_target instrument1 dir0)
    (calibration_target instrument2 dir0)
    (= (calibration_time instrument0 dir0) 5)
    (= (calibration_time instrument1 dir0) 5)
    (= (calibration_time instrument2 dir0) 5)
	   (on_board instrument0 satellite0)
	   (on_board instrument1 satellite0)
	   (on_board instrument2 satellite0)
    (available instrument0)
    (available instrument1)
    (available instrument2)
    (not_calibrated instrument0)
    (not_calibrated instrument1)
    (not_calibrated instrument2)

    (power_avail satellite0)
    (pointing satellite0 dir0)

    (= (slew_time dir0 dir1) 239)
    (= (slew_time dir1 dir0) 239)
    (= (slew_time dir0 dir2) 274)
    (= (slew_time dir2 dir0) 274)
    (= (slew_time dir0 dir3) 228)
    (= (slew_time dir3 dir0) 228)
    (= (slew_time dir0 dir4) 77)
    (= (slew_time dir4 dir0) 77)
    (= (slew_time dir0 dir5) 166)
    (= (slew_time dir5 dir0) 166)
    (= (slew_time dir0 dir6) 133)
    (= (slew_time dir6 dir0) 133)
    (= (slew_time dir0 dir7) 59)
    (= (slew_time dir7 dir0) 59)
    (= (slew_time dir0 dir8) 271)
    (= (slew_time dir8 dir0) 271)
    (= (slew_time dir0 dir9) 225)
    (= (slew_time dir9 dir0) 225)
    (= (slew_time dir1 dir0) 239)
    (= (slew_time dir0 dir1) 239)
    (= (slew_time dir1 dir2) 35)
    (= (slew_time dir2 dir1) 35)
    (= (slew_time dir1 dir3) 11)
    (= (slew_time dir3 dir1) 11)
    (= (slew_time dir1 dir4) 316)
    (= (slew_time dir4 dir1) 316)
    (= (slew_time dir1 dir5) 73)
    (= (slew_time dir5 dir1) 73)
    (= (slew_time dir1 dir6) 106)
    (= (slew_time dir6 dir1) 106)
    (= (slew_time dir1 dir7) 298)
    (= (slew_time dir7 dir1) 298)
    (= (slew_time dir1 dir8) 32)
    (= (slew_time dir8 dir1) 32)
    (= (slew_time dir1 dir9) 14)
    (= (slew_time dir9 dir1) 14)
    (= (slew_time dir2 dir0) 274)
    (= (slew_time dir0 dir2) 274)
    (= (slew_time dir2 dir1) 35)
    (= (slew_time dir1 dir2) 35)
    (= (slew_time dir2 dir3) 46)
    (= (slew_time dir3 dir2) 46)
    (= (slew_time dir2 dir4) 351)
    (= (slew_time dir4 dir2) 351)
    (= (slew_time dir2 dir5) 108)
    (= (slew_time dir5 dir2) 108)
    (= (slew_time dir2 dir6) 141)
    (= (slew_time dir6 dir2) 141)
    (= (slew_time dir2 dir7) 333)
    (= (slew_time dir7 dir2) 333)
    (= (slew_time dir2 dir8) 3)
    (= (slew_time dir8 dir2) 3)
    (= (slew_time dir2 dir9) 49)
    (= (slew_time dir9 dir2) 49)
    (= (slew_time dir3 dir0) 228)
    (= (slew_time dir0 dir3) 228)
    (= (slew_time dir3 dir1) 11)
    (= (slew_time dir1 dir3) 11)
    (= (slew_time dir3 dir2) 46)
    (= (slew_time dir2 dir3) 46)
    (= (slew_time dir3 dir4) 305)
    (= (slew_time dir4 dir3) 305)
    (= (slew_time dir3 dir5) 62)
    (= (slew_time dir5 dir3) 62)
    (= (slew_time dir3 dir6) 95)
    (= (slew_time dir6 dir3) 95)
    (= (slew_time dir3 dir7) 287)
    (= (slew_time dir7 dir3) 287)
    (= (slew_time dir3 dir8) 43)
    (= (slew_time dir8 dir3) 43)
    (= (slew_time dir3 dir9) 3)
    (= (slew_time dir9 dir3) 3)
    (= (slew_time dir4 dir0) 77)
    (= (slew_time dir0 dir4) 77)
    (= (slew_time dir4 dir1) 316)
    (= (slew_time dir1 dir4) 316)
    (= (slew_time dir4 dir2) 351)
    (= (slew_time dir2 dir4) 351)
    (= (slew_time dir4 dir3) 305)
    (= (slew_time dir3 dir4) 305)
    (= (slew_time dir4 dir5) 243)
    (= (slew_time dir5 dir4) 243)
    (= (slew_time dir4 dir6) 210)
    (= (slew_time dir6 dir4) 210)
    (= (slew_time dir4 dir7) 18)
    (= (slew_time dir7 dir4) 18)
    (= (slew_time dir4 dir8) 348)
    (= (slew_time dir8 dir4) 348)
    (= (slew_time dir4 dir9) 302)
    (= (slew_time dir9 dir4) 302)
    (= (slew_time dir5 dir0) 166)
    (= (slew_time dir0 dir5) 166)
    (= (slew_time dir5 dir1) 73)
    (= (slew_time dir1 dir5) 73)
    (= (slew_time dir5 dir2) 108)
    (= (slew_time dir2 dir5) 108)
    (= (slew_time dir5 dir3) 62)
    (= (slew_time dir3 dir5) 62)
    (= (slew_time dir5 dir4) 243)
    (= (slew_time dir4 dir5) 243)
    (= (slew_time dir5 dir6) 33)
    (= (slew_time dir6 dir5) 33)
    (= (slew_time dir5 dir7) 225)
    (= (slew_time dir7 dir5) 225)
    (= (slew_time dir5 dir8) 105)
    (= (slew_time dir8 dir5) 105)
    (= (slew_time dir5 dir9) 59)
    (= (slew_time dir9 dir5) 59)
    (= (slew_time dir6 dir0) 133)
    (= (slew_time dir0 dir6) 133)
    (= (slew_time dir6 dir1) 106)
    (= (slew_time dir1 dir6) 106)
    (= (slew_time dir6 dir2) 141)
    (= (slew_time dir2 dir6) 141)
    (= (slew_time dir6 dir3) 95)
    (= (slew_time dir3 dir6) 95)
    (= (slew_time dir6 dir4) 210)
    (= (slew_time dir4 dir6) 210)
    (= (slew_time dir6 dir5) 33)
    (= (slew_time dir5 dir6) 33)
    (= (slew_time dir6 dir7) 192)
    (= (slew_time dir7 dir6) 192)
    (= (slew_time dir6 dir8) 138)
    (= (slew_time dir8 dir6) 138)
    (= (slew_time dir6 dir9) 92)
    (= (slew_time dir9 dir6) 92)
    (= (slew_time dir7 dir0) 59)
    (= (slew_time dir0 dir7) 59)
    (= (slew_time dir7 dir1) 298)
    (= (slew_time dir1 dir7) 298)
    (= (slew_time dir7 dir2) 333)
    (= (slew_time dir2 dir7) 333)
    (= (slew_time dir7 dir3) 287)
    (= (slew_time dir3 dir7) 287)
    (= (slew_time dir7 dir4) 18)
    (= (slew_time dir4 dir7) 18)
    (= (slew_time dir7 dir5) 225)
    (= (slew_time dir5 dir7) 225)
    (= (slew_time dir7 dir6) 192)
    (= (slew_time dir6 dir7) 192)
    (= (slew_time dir7 dir8) 330)
    (= (slew_time dir8 dir7) 330)
    (= (slew_time dir7 dir9) 284)
    (= (slew_time dir9 dir7) 284)
    (= (slew_time dir8 dir0) 271)
    (= (slew_time dir0 dir8) 271)
    (= (slew_time dir8 dir1) 32)
    (= (slew_time dir1 dir8) 32)
    (= (slew_time dir8 dir2) 3)
    (= (slew_time dir2 dir8) 3)
    (= (slew_time dir8 dir3) 43)
    (= (slew_time dir3 dir8) 43)
    (= (slew_time dir8 dir4) 348)
    (= (slew_time dir4 dir8) 348)
    (= (slew_time dir8 dir5) 105)
    (= (slew_time dir5 dir8) 105)
    (= (slew_time dir8 dir6) 138)
    (= (slew_time dir6 dir8) 138)
    (= (slew_time dir8 dir7) 330)
    (= (slew_time dir7 dir8) 330)
    (= (slew_time dir8 dir9) 46)
    (= (slew_time dir9 dir8) 46)
    (= (slew_time dir9 dir0) 225)
    (= (slew_time dir0 dir9) 225)
    (= (slew_time dir9 dir1) 14)
    (= (slew_time dir1 dir9) 14)
    (= (slew_time dir9 dir2) 49)
    (= (slew_time dir2 dir9) 49)
    (= (slew_time dir9 dir3) 3)
    (= (slew_time dir3 dir9) 3)
    (= (slew_time dir9 dir4) 302)
    (= (slew_time dir4 dir9) 302)
    (= (slew_time dir9 dir5) 59)
    (= (slew_time dir5 dir9) 59)
    (= (slew_time dir9 dir6) 92)
    (= (slew_time dir6 dir9) 92)
    (= (slew_time dir9 dir7) 284)
    (= (slew_time dir7 dir9) 284)
    (= (slew_time dir9 dir8) 46)
    (= (slew_time dir8 dir9) 46)
)
(:goal (and
    (have_image dir1 image)
    (have_image dir2 image)
    (have_image dir3 image)
    (have_image dir4 image)
    (have_image dir5 image)
    (have_image dir6 image)
    (have_image dir7 image)
))
(:metric minimize (total-time))
)
