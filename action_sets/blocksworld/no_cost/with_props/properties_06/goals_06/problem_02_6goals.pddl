(define (problem blocksworld-p02_6)
(:domain blocksworld)
(:objects
a b c d e f g - block
h0 h1 - hand
)
(:init

(handempty h0)
(handempty h1)
(= (ecost h0) 1)
(= (ecost h1) 2)
(= (total-cost) 0)
(ontable a)
(clear g)
(on b a)
(on c b)
(on d c)
(on e d)
(on f e)
(on g f)
)
(:goal (and
(on g c)
(on b g)
(on a b)
(on e a)
(on f e)
(on d f)
))
(:metric minimize (total-cost))
)
