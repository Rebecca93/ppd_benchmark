(define (problem satellite-p04_10dirs_8goals)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 instrument1 instrument2 - instrument
	image spectograph thermograph - mode
 dir0 dir1 dir2 dir3 dir4 dir5 dir6 dir7 dir8 dir9 - direction
)
(:init
    (supports instrument0 image)
    (supports instrument1 thermograph)
    (supports instrument2 spectograph)
    (calibration_target instrument0 dir0)
    (calibration_target instrument1 dir0)
    (calibration_target instrument2 dir0)
    (= (calibration_time instrument0 dir0) 5)
    (= (calibration_time instrument1 dir0) 5)
    (= (calibration_time instrument2 dir0) 5)
	   (on_board instrument0 satellite0)
	   (on_board instrument1 satellite0)
	   (on_board instrument2 satellite0)
    (available instrument0)
    (available instrument1)
    (available instrument2)
    (not_calibrated instrument0)
    (not_calibrated instrument1)
    (not_calibrated instrument2)

    (power_avail satellite0)
    (pointing satellite0 dir0)

    (= (slew_time dir0 dir1) 28)
    (= (slew_time dir1 dir0) 28)
    (= (slew_time dir0 dir2) 180)
    (= (slew_time dir2 dir0) 180)
    (= (slew_time dir0 dir3) 140)
    (= (slew_time dir3 dir0) 140)
    (= (slew_time dir0 dir4) 93)
    (= (slew_time dir4 dir0) 93)
    (= (slew_time dir0 dir5) 45)
    (= (slew_time dir5 dir0) 45)
    (= (slew_time dir0 dir6) 148)
    (= (slew_time dir6 dir0) 148)
    (= (slew_time dir0 dir7) 10)
    (= (slew_time dir7 dir0) 10)
    (= (slew_time dir0 dir8) 76)
    (= (slew_time dir8 dir0) 76)
    (= (slew_time dir0 dir9) 161)
    (= (slew_time dir9 dir0) 161)
    (= (slew_time dir1 dir0) 28)
    (= (slew_time dir0 dir1) 28)
    (= (slew_time dir1 dir2) 208)
    (= (slew_time dir2 dir1) 208)
    (= (slew_time dir1 dir3) 168)
    (= (slew_time dir3 dir1) 168)
    (= (slew_time dir1 dir4) 121)
    (= (slew_time dir4 dir1) 121)
    (= (slew_time dir1 dir5) 17)
    (= (slew_time dir5 dir1) 17)
    (= (slew_time dir1 dir6) 176)
    (= (slew_time dir6 dir1) 176)
    (= (slew_time dir1 dir7) 38)
    (= (slew_time dir7 dir1) 38)
    (= (slew_time dir1 dir8) 104)
    (= (slew_time dir8 dir1) 104)
    (= (slew_time dir1 dir9) 189)
    (= (slew_time dir9 dir1) 189)
    (= (slew_time dir2 dir0) 180)
    (= (slew_time dir0 dir2) 180)
    (= (slew_time dir2 dir1) 208)
    (= (slew_time dir1 dir2) 208)
    (= (slew_time dir2 dir3) 40)
    (= (slew_time dir3 dir2) 40)
    (= (slew_time dir2 dir4) 87)
    (= (slew_time dir4 dir2) 87)
    (= (slew_time dir2 dir5) 225)
    (= (slew_time dir5 dir2) 225)
    (= (slew_time dir2 dir6) 32)
    (= (slew_time dir6 dir2) 32)
    (= (slew_time dir2 dir7) 170)
    (= (slew_time dir7 dir2) 170)
    (= (slew_time dir2 dir8) 104)
    (= (slew_time dir8 dir2) 104)
    (= (slew_time dir2 dir9) 19)
    (= (slew_time dir9 dir2) 19)
    (= (slew_time dir3 dir0) 140)
    (= (slew_time dir0 dir3) 140)
    (= (slew_time dir3 dir1) 168)
    (= (slew_time dir1 dir3) 168)
    (= (slew_time dir3 dir2) 40)
    (= (slew_time dir2 dir3) 40)
    (= (slew_time dir3 dir4) 47)
    (= (slew_time dir4 dir3) 47)
    (= (slew_time dir3 dir5) 185)
    (= (slew_time dir5 dir3) 185)
    (= (slew_time dir3 dir6) 8)
    (= (slew_time dir6 dir3) 8)
    (= (slew_time dir3 dir7) 130)
    (= (slew_time dir7 dir3) 130)
    (= (slew_time dir3 dir8) 64)
    (= (slew_time dir8 dir3) 64)
    (= (slew_time dir3 dir9) 21)
    (= (slew_time dir9 dir3) 21)
    (= (slew_time dir4 dir0) 93)
    (= (slew_time dir0 dir4) 93)
    (= (slew_time dir4 dir1) 121)
    (= (slew_time dir1 dir4) 121)
    (= (slew_time dir4 dir2) 87)
    (= (slew_time dir2 dir4) 87)
    (= (slew_time dir4 dir3) 47)
    (= (slew_time dir3 dir4) 47)
    (= (slew_time dir4 dir5) 138)
    (= (slew_time dir5 dir4) 138)
    (= (slew_time dir4 dir6) 55)
    (= (slew_time dir6 dir4) 55)
    (= (slew_time dir4 dir7) 83)
    (= (slew_time dir7 dir4) 83)
    (= (slew_time dir4 dir8) 17)
    (= (slew_time dir8 dir4) 17)
    (= (slew_time dir4 dir9) 68)
    (= (slew_time dir9 dir4) 68)
    (= (slew_time dir5 dir0) 45)
    (= (slew_time dir0 dir5) 45)
    (= (slew_time dir5 dir1) 17)
    (= (slew_time dir1 dir5) 17)
    (= (slew_time dir5 dir2) 225)
    (= (slew_time dir2 dir5) 225)
    (= (slew_time dir5 dir3) 185)
    (= (slew_time dir3 dir5) 185)
    (= (slew_time dir5 dir4) 138)
    (= (slew_time dir4 dir5) 138)
    (= (slew_time dir5 dir6) 193)
    (= (slew_time dir6 dir5) 193)
    (= (slew_time dir5 dir7) 55)
    (= (slew_time dir7 dir5) 55)
    (= (slew_time dir5 dir8) 121)
    (= (slew_time dir8 dir5) 121)
    (= (slew_time dir5 dir9) 206)
    (= (slew_time dir9 dir5) 206)
    (= (slew_time dir6 dir0) 148)
    (= (slew_time dir0 dir6) 148)
    (= (slew_time dir6 dir1) 176)
    (= (slew_time dir1 dir6) 176)
    (= (slew_time dir6 dir2) 32)
    (= (slew_time dir2 dir6) 32)
    (= (slew_time dir6 dir3) 8)
    (= (slew_time dir3 dir6) 8)
    (= (slew_time dir6 dir4) 55)
    (= (slew_time dir4 dir6) 55)
    (= (slew_time dir6 dir5) 193)
    (= (slew_time dir5 dir6) 193)
    (= (slew_time dir6 dir7) 138)
    (= (slew_time dir7 dir6) 138)
    (= (slew_time dir6 dir8) 72)
    (= (slew_time dir8 dir6) 72)
    (= (slew_time dir6 dir9) 13)
    (= (slew_time dir9 dir6) 13)
    (= (slew_time dir7 dir0) 10)
    (= (slew_time dir0 dir7) 10)
    (= (slew_time dir7 dir1) 38)
    (= (slew_time dir1 dir7) 38)
    (= (slew_time dir7 dir2) 170)
    (= (slew_time dir2 dir7) 170)
    (= (slew_time dir7 dir3) 130)
    (= (slew_time dir3 dir7) 130)
    (= (slew_time dir7 dir4) 83)
    (= (slew_time dir4 dir7) 83)
    (= (slew_time dir7 dir5) 55)
    (= (slew_time dir5 dir7) 55)
    (= (slew_time dir7 dir6) 138)
    (= (slew_time dir6 dir7) 138)
    (= (slew_time dir7 dir8) 66)
    (= (slew_time dir8 dir7) 66)
    (= (slew_time dir7 dir9) 151)
    (= (slew_time dir9 dir7) 151)
    (= (slew_time dir8 dir0) 76)
    (= (slew_time dir0 dir8) 76)
    (= (slew_time dir8 dir1) 104)
    (= (slew_time dir1 dir8) 104)
    (= (slew_time dir8 dir2) 104)
    (= (slew_time dir2 dir8) 104)
    (= (slew_time dir8 dir3) 64)
    (= (slew_time dir3 dir8) 64)
    (= (slew_time dir8 dir4) 17)
    (= (slew_time dir4 dir8) 17)
    (= (slew_time dir8 dir5) 121)
    (= (slew_time dir5 dir8) 121)
    (= (slew_time dir8 dir6) 72)
    (= (slew_time dir6 dir8) 72)
    (= (slew_time dir8 dir7) 66)
    (= (slew_time dir7 dir8) 66)
    (= (slew_time dir8 dir9) 85)
    (= (slew_time dir9 dir8) 85)
    (= (slew_time dir9 dir0) 161)
    (= (slew_time dir0 dir9) 161)
    (= (slew_time dir9 dir1) 189)
    (= (slew_time dir1 dir9) 189)
    (= (slew_time dir9 dir2) 19)
    (= (slew_time dir2 dir9) 19)
    (= (slew_time dir9 dir3) 21)
    (= (slew_time dir3 dir9) 21)
    (= (slew_time dir9 dir4) 68)
    (= (slew_time dir4 dir9) 68)
    (= (slew_time dir9 dir5) 206)
    (= (slew_time dir5 dir9) 206)
    (= (slew_time dir9 dir6) 13)
    (= (slew_time dir6 dir9) 13)
    (= (slew_time dir9 dir7) 151)
    (= (slew_time dir7 dir9) 151)
    (= (slew_time dir9 dir8) 85)
    (= (slew_time dir8 dir9) 85)
)
(:goal (and
    (have_image dir1 image)
    (have_image dir2 image)
    (have_image dir3 image)
    (have_image dir4 image)
    (have_image dir5 image)
    (have_image dir6 image)
    (have_image dir7 image)
    (have_image dir8 image)
))
(:metric minimize (total-time))
)
