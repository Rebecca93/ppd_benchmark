set drive_t0_market3_market1 2
drive truck0 market3 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market3 moneylevel moneylevel moneylevel


soft-property use_t0_market3_market1
drive_t0_market3_market1



set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_depot0
drive_t0_market2_depot0



set drive_t0_market2_market3 2
drive truck0 market2 market3 moneylevel moneylevel moneylevel
drive truck0 market3 market2 moneylevel moneylevel moneylevel


soft-property use_not_t0_market2_market3
! drive_t0_market2_market3



set load_g3_m3 1
load goods3 truck market3


set load_g3_m5 1
load goods3 truck market5


soft-property load_g3_m3_not_m5
&& load_g3_m3 ! load_g3_m5


set load_g5_m2 1
load goods5 truck market2


set load_g5_m4 1
load goods5 truck market4


soft-property load_g5_m2_not_m4
&& load_g5_m2 ! load_g5_m4


set load_g4_m5 1
load goods4 truck market5


set load_g4_m3 1
load goods4 truck market3


soft-property load_g4_m5_not_m3
&& load_g4_m5 ! load_g4_m3


set drive_t0_market4_market1 2
drive truck0 market4 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market4 moneylevel moneylevel moneylevel


soft-property use_t0_market4_market1
drive_t0_market4_market1



set drive_t0_depot0_market3 2
drive truck0 depot0 market3 moneylevel moneylevel moneylevel
drive truck0 market3 depot0 moneylevel moneylevel moneylevel


soft-property use_t0_depot0_market3
drive_t0_depot0_market3



set drive_t0_market2_market4 2
drive truck0 market2 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market2 moneylevel moneylevel moneylevel


soft-property use_t0_market2_market4
drive_t0_market2_market4



set load_g6_m1 1
load goods6 truck market1


set load_g6_m5 1
load goods6 truck market5


soft-property load_g6_m1_not_m5
&& load_g6_m1 ! load_g6_m5


