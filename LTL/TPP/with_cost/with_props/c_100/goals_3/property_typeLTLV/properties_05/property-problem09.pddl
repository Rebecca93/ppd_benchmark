soft-LTL_property use_t0_market5_market1
|| <> && at(truck0,market5) X at(truck0,market1) <> && at(truck0,market1)  X at(truck0,market5)


soft-LTL_property use_not_t0_market2_market1
&& [] || ! at(truck0,market2) ! X at(truck0,market1) [] || ! at(truck0,market1) ! X at(truck0,market2)


soft-LTL_property load_g1_m4_not_m3
<> && && available(goods1,market4) ! available(goods1,market3) X loaded(goods1,truck0)


soft-LTL_property use_not_t0_market3_market5
&& [] || ! at(truck0,market3) ! X at(truck0,market5) [] || ! at(truck0,market5) ! X at(truck0,market3)


soft-LTL_property use_t0_market4_market3
|| <> && at(truck0,market4) X at(truck0,market3) <> && at(truck0,market3)  X at(truck0,market4)


