(define (problem satellite-p06_10dirs_9goals)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 instrument1 instrument2 - instrument
	image spectograph thermograph - mode
 dir0 dir1 dir2 dir3 dir4 dir5 dir6 dir7 dir8 dir9 - direction
)
(:init
    (supports instrument0 image)
    (supports instrument1 thermograph)
    (supports instrument2 spectograph)
    (calibration_target instrument0 dir0)
    (calibration_target instrument1 dir0)
    (calibration_target instrument2 dir0)
    (= (calibration_time instrument0 dir0) 5)
    (= (calibration_time instrument1 dir0) 5)
    (= (calibration_time instrument2 dir0) 5)
	   (on_board instrument0 satellite0)
	   (on_board instrument1 satellite0)
	   (on_board instrument2 satellite0)
    (available instrument0)
    (available instrument1)
    (available instrument2)
    (not_calibrated instrument0)
    (not_calibrated instrument1)
    (not_calibrated instrument2)

    (power_avail satellite0)
    (pointing satellite0 dir0)

    (= (slew_time dir0 dir1) 282)
    (= (slew_time dir1 dir0) 282)
    (= (slew_time dir0 dir2) 33)
    (= (slew_time dir2 dir0) 33)
    (= (slew_time dir0 dir3) 166)
    (= (slew_time dir3 dir0) 166)
    (= (slew_time dir0 dir4) 10)
    (= (slew_time dir4 dir0) 10)
    (= (slew_time dir0 dir5) 230)
    (= (slew_time dir5 dir0) 230)
    (= (slew_time dir0 dir6) 112)
    (= (slew_time dir6 dir0) 112)
    (= (slew_time dir0 dir7) 54)
    (= (slew_time dir7 dir0) 54)
    (= (slew_time dir0 dir8) 285)
    (= (slew_time dir8 dir0) 285)
    (= (slew_time dir0 dir9) 32)
    (= (slew_time dir9 dir0) 32)
    (= (slew_time dir1 dir0) 282)
    (= (slew_time dir0 dir1) 282)
    (= (slew_time dir1 dir2) 249)
    (= (slew_time dir2 dir1) 249)
    (= (slew_time dir1 dir3) 116)
    (= (slew_time dir3 dir1) 116)
    (= (slew_time dir1 dir4) 272)
    (= (slew_time dir4 dir1) 272)
    (= (slew_time dir1 dir5) 52)
    (= (slew_time dir5 dir1) 52)
    (= (slew_time dir1 dir6) 170)
    (= (slew_time dir6 dir1) 170)
    (= (slew_time dir1 dir7) 336)
    (= (slew_time dir7 dir1) 336)
    (= (slew_time dir1 dir8) 3)
    (= (slew_time dir8 dir1) 3)
    (= (slew_time dir1 dir9) 250)
    (= (slew_time dir9 dir1) 250)
    (= (slew_time dir2 dir0) 33)
    (= (slew_time dir0 dir2) 33)
    (= (slew_time dir2 dir1) 249)
    (= (slew_time dir1 dir2) 249)
    (= (slew_time dir2 dir3) 133)
    (= (slew_time dir3 dir2) 133)
    (= (slew_time dir2 dir4) 23)
    (= (slew_time dir4 dir2) 23)
    (= (slew_time dir2 dir5) 197)
    (= (slew_time dir5 dir2) 197)
    (= (slew_time dir2 dir6) 79)
    (= (slew_time dir6 dir2) 79)
    (= (slew_time dir2 dir7) 87)
    (= (slew_time dir7 dir2) 87)
    (= (slew_time dir2 dir8) 252)
    (= (slew_time dir8 dir2) 252)
    (= (slew_time dir2 dir9) 1)
    (= (slew_time dir9 dir2) 1)
    (= (slew_time dir3 dir0) 166)
    (= (slew_time dir0 dir3) 166)
    (= (slew_time dir3 dir1) 116)
    (= (slew_time dir1 dir3) 116)
    (= (slew_time dir3 dir2) 133)
    (= (slew_time dir2 dir3) 133)
    (= (slew_time dir3 dir4) 156)
    (= (slew_time dir4 dir3) 156)
    (= (slew_time dir3 dir5) 64)
    (= (slew_time dir5 dir3) 64)
    (= (slew_time dir3 dir6) 54)
    (= (slew_time dir6 dir3) 54)
    (= (slew_time dir3 dir7) 220)
    (= (slew_time dir7 dir3) 220)
    (= (slew_time dir3 dir8) 119)
    (= (slew_time dir8 dir3) 119)
    (= (slew_time dir3 dir9) 134)
    (= (slew_time dir9 dir3) 134)
    (= (slew_time dir4 dir0) 10)
    (= (slew_time dir0 dir4) 10)
    (= (slew_time dir4 dir1) 272)
    (= (slew_time dir1 dir4) 272)
    (= (slew_time dir4 dir2) 23)
    (= (slew_time dir2 dir4) 23)
    (= (slew_time dir4 dir3) 156)
    (= (slew_time dir3 dir4) 156)
    (= (slew_time dir4 dir5) 220)
    (= (slew_time dir5 dir4) 220)
    (= (slew_time dir4 dir6) 102)
    (= (slew_time dir6 dir4) 102)
    (= (slew_time dir4 dir7) 64)
    (= (slew_time dir7 dir4) 64)
    (= (slew_time dir4 dir8) 275)
    (= (slew_time dir8 dir4) 275)
    (= (slew_time dir4 dir9) 22)
    (= (slew_time dir9 dir4) 22)
    (= (slew_time dir5 dir0) 230)
    (= (slew_time dir0 dir5) 230)
    (= (slew_time dir5 dir1) 52)
    (= (slew_time dir1 dir5) 52)
    (= (slew_time dir5 dir2) 197)
    (= (slew_time dir2 dir5) 197)
    (= (slew_time dir5 dir3) 64)
    (= (slew_time dir3 dir5) 64)
    (= (slew_time dir5 dir4) 220)
    (= (slew_time dir4 dir5) 220)
    (= (slew_time dir5 dir6) 118)
    (= (slew_time dir6 dir5) 118)
    (= (slew_time dir5 dir7) 284)
    (= (slew_time dir7 dir5) 284)
    (= (slew_time dir5 dir8) 55)
    (= (slew_time dir8 dir5) 55)
    (= (slew_time dir5 dir9) 198)
    (= (slew_time dir9 dir5) 198)
    (= (slew_time dir6 dir0) 112)
    (= (slew_time dir0 dir6) 112)
    (= (slew_time dir6 dir1) 170)
    (= (slew_time dir1 dir6) 170)
    (= (slew_time dir6 dir2) 79)
    (= (slew_time dir2 dir6) 79)
    (= (slew_time dir6 dir3) 54)
    (= (slew_time dir3 dir6) 54)
    (= (slew_time dir6 dir4) 102)
    (= (slew_time dir4 dir6) 102)
    (= (slew_time dir6 dir5) 118)
    (= (slew_time dir5 dir6) 118)
    (= (slew_time dir6 dir7) 166)
    (= (slew_time dir7 dir6) 166)
    (= (slew_time dir6 dir8) 173)
    (= (slew_time dir8 dir6) 173)
    (= (slew_time dir6 dir9) 80)
    (= (slew_time dir9 dir6) 80)
    (= (slew_time dir7 dir0) 54)
    (= (slew_time dir0 dir7) 54)
    (= (slew_time dir7 dir1) 336)
    (= (slew_time dir1 dir7) 336)
    (= (slew_time dir7 dir2) 87)
    (= (slew_time dir2 dir7) 87)
    (= (slew_time dir7 dir3) 220)
    (= (slew_time dir3 dir7) 220)
    (= (slew_time dir7 dir4) 64)
    (= (slew_time dir4 dir7) 64)
    (= (slew_time dir7 dir5) 284)
    (= (slew_time dir5 dir7) 284)
    (= (slew_time dir7 dir6) 166)
    (= (slew_time dir6 dir7) 166)
    (= (slew_time dir7 dir8) 339)
    (= (slew_time dir8 dir7) 339)
    (= (slew_time dir7 dir9) 86)
    (= (slew_time dir9 dir7) 86)
    (= (slew_time dir8 dir0) 285)
    (= (slew_time dir0 dir8) 285)
    (= (slew_time dir8 dir1) 3)
    (= (slew_time dir1 dir8) 3)
    (= (slew_time dir8 dir2) 252)
    (= (slew_time dir2 dir8) 252)
    (= (slew_time dir8 dir3) 119)
    (= (slew_time dir3 dir8) 119)
    (= (slew_time dir8 dir4) 275)
    (= (slew_time dir4 dir8) 275)
    (= (slew_time dir8 dir5) 55)
    (= (slew_time dir5 dir8) 55)
    (= (slew_time dir8 dir6) 173)
    (= (slew_time dir6 dir8) 173)
    (= (slew_time dir8 dir7) 339)
    (= (slew_time dir7 dir8) 339)
    (= (slew_time dir8 dir9) 253)
    (= (slew_time dir9 dir8) 253)
    (= (slew_time dir9 dir0) 32)
    (= (slew_time dir0 dir9) 32)
    (= (slew_time dir9 dir1) 250)
    (= (slew_time dir1 dir9) 250)
    (= (slew_time dir9 dir2) 1)
    (= (slew_time dir2 dir9) 1)
    (= (slew_time dir9 dir3) 134)
    (= (slew_time dir3 dir9) 134)
    (= (slew_time dir9 dir4) 22)
    (= (slew_time dir4 dir9) 22)
    (= (slew_time dir9 dir5) 198)
    (= (slew_time dir5 dir9) 198)
    (= (slew_time dir9 dir6) 80)
    (= (slew_time dir6 dir9) 80)
    (= (slew_time dir9 dir7) 86)
    (= (slew_time dir7 dir9) 86)
    (= (slew_time dir9 dir8) 253)
    (= (slew_time dir8 dir9) 253)
)
(:goal (and
    (have_image dir1 image)
    (have_image dir2 image)
    (have_image dir3 image)
    (have_image dir4 image)
    (have_image dir5 image)
    (have_image dir6 image)
    (have_image dir7 image)
    (have_image dir8 image)
    (have_image dir9 image)
))
(:metric minimize (total-time))
)
