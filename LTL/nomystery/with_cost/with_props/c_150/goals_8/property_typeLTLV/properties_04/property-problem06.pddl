soft-LTL_property use_t1_l5_l0
|| <> && at(t1,l5) X at(t1,l0) <> && at(t1,l0)  X at(t1,l5)


soft-LTL_property same_truck_p5p6
|| [] && ! in(p5,t1) ! in(p6,t1)  [] && ! in(p5,t0) ! in(p6,t0)


soft-LTL_property use_t0_l1_l0
|| <> && at(t0,l1) X at(t0,l0) <> && at(t0,l0)  X at(t0,l1)


soft-LTL_property use_t1_l5_l2
|| <> && at(t1,l5) X at(t1,l2) <> && at(t1,l2)  X at(t1,l5)


