soft-LTL_property use_t1_l4_l0
|| <> && at(t1,l4) X at(t1,l0) <> && at(t1,l0)  X at(t1,l4)


soft-LTL_property same_truck_p3p5
|| [] && ! in(p3,t1) ! in(p5,t1)  [] && ! in(p3,t0) ! in(p5,t0)


soft-LTL_property same_truck_p0p4
|| [] && ! in(p0,t1) ! in(p4,t1)  [] && ! in(p0,t0) ! in(p4,t0)


soft-LTL_property use_t1_l0_l3
|| <> && at(t1,l0) X at(t1,l3) <> && at(t1,l3)  X at(t1,l0)


