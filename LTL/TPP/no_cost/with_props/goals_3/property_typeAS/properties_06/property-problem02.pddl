set drive_t0_market4_market1 2
drive truck0 market4 market1
drive truck0 market1 market4

soft-AS_property use_t0_market4_market1
drive_t0_market4_market1



set drive_t0_market2_depot0 2
drive truck0 market2 depot0
drive truck0 depot0 market2

soft-AS_property use_not_t0_market2_depot0
! drive_t0_market2_depot0



set load_g1_m2 1
load goods1 truck market2


set load_g1_m1 1
load goods1 truck market1


soft-AS_property load_g1_m2_not_m1
&& load_g1_m2 ! load_g1_m1


set drive_t0_market3_market1 2
drive truck0 market3 market1
drive truck0 market1 market3

soft-AS_property use_not_t0_market3_market1
! drive_t0_market3_market1



set drive_t0_market3_market5 2
drive truck0 market3 market5
drive truck0 market5 market3

soft-AS_property use_t0_market3_market5
drive_t0_market3_market5



set drive_t0_depot0_market3 2
drive truck0 depot0 market3
drive truck0 market3 depot0

soft-AS_property use_t0_depot0_market3
drive_t0_depot0_market3



