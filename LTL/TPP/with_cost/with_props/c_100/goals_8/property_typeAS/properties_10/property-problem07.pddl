set drive_t0_market4_market5 2
drive truck0 market4 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_market5
drive_t0_market4_market5



set drive_t0_market1_depot0 2
drive truck0 market1 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market1 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market1_depot0
! drive_t0_market1_depot0



set load_g6_m5 1
load goods6 truck market5


set load_g6_m1 1
load goods6 truck market1


soft-AS_property load_g6_m5_not_m1
&& load_g6_m5 ! load_g6_m1


set drive_t0_market3_depot0 2
drive truck0 market3 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market3 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market3_depot0
! drive_t0_market3_depot0



set drive_t0_market4_depot0 2
drive truck0 market4 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market4 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market4_depot0
drive_t0_market4_depot0



set drive_t0_depot0_market5 2
drive truck0 depot0 market5 moneylevel moneylevel moneylevel
drive truck0 market5 depot0 moneylevel moneylevel moneylevel


soft-AS_property use_t0_depot0_market5
drive_t0_depot0_market5



set load_g3_m3 1
load goods3 truck market3


set load_g3_m2 1
load goods3 truck market2


soft-AS_property load_g3_m3_not_m2
&& load_g3_m3 ! load_g3_m2


set drive_t0_market2_depot0 2
drive truck0 market2 depot0 moneylevel moneylevel moneylevel
drive truck0 depot0 market2 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market2_depot0
drive_t0_market2_depot0



set drive_t0_market1_market4 2
drive truck0 market1 market4 moneylevel moneylevel moneylevel
drive truck0 market4 market1 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market1_market4
drive_t0_market1_market4



set drive_t0_market3_market1 2
drive truck0 market3 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market3 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market3_market1
drive_t0_market3_market1



