soft-LTL_property use_t1_l5_l0
|| <> && at(t1,l5) X at(t1,l0) <> && at(t1,l0)  X at(t1,l5)


soft-LTL_property same_truck_p1p2
|| [] && ! in(p1,t1) ! in(p2,t1)  [] && ! in(p1,t0) ! in(p2,t0)


soft-LTL_property same_truck_p0p2
|| [] && ! in(p0,t1) ! in(p2,t1)  [] && ! in(p0,t0) ! in(p2,t0)


soft-LTL_property use_t1_l0_l4
|| <> && at(t1,l0) X at(t1,l4) <> && at(t1,l4)  X at(t1,l0)


soft-LTL_property same_truck_p0p1
|| [] && ! in(p0,t1) ! in(p1,t1)  [] && ! in(p0,t0) ! in(p1,t0)


soft-LTL_property use_t1_l1_l5
|| <> && at(t1,l1) X at(t1,l5) <> && at(t1,l5)  X at(t1,l1)


soft-LTL_property use_t0_l5_l4
|| <> && at(t0,l5) X at(t0,l4) <> && at(t0,l4)  X at(t0,l5)


soft-LTL_property use_t0_l0_l1
|| <> && at(t0,l0) X at(t0,l1) <> && at(t0,l1)  X at(t0,l0)


soft-LTL_property use_t1_l3_l2
|| <> && at(t1,l3) X at(t1,l2) <> && at(t1,l2)  X at(t1,l3)


