set drive_t0_market5_market1 2
drive truck0 market5 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market5 moneylevel moneylevel moneylevel


soft-AS_property use_t0_market5_market1
drive_t0_market5_market1



set drive_t0_market2_market1 2
drive truck0 market2 market1 moneylevel moneylevel moneylevel
drive truck0 market1 market2 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market2_market1
! drive_t0_market2_market1



set load_g1_m4 1
load goods1 truck market4


set load_g1_m3 1
load goods1 truck market3


soft-AS_property load_g1_m4_not_m3
&& load_g1_m4 ! load_g1_m3


set drive_t0_market3_market5 2
drive truck0 market3 market5 moneylevel moneylevel moneylevel
drive truck0 market5 market3 moneylevel moneylevel moneylevel


soft-AS_property use_not_t0_market3_market5
! drive_t0_market3_market5



